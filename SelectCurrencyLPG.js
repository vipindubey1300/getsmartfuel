import React from 'react';
import { StyleSheet, Text, View ,Button, AsyncStorage,TouchableOpacity,Platform,ToastAndroid,ActivityIndicator,StatusBar,NetInfo,Alert} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
//import { Dropdown } from 'react-native-material-dropdown';
import FormData from 'FormData';
import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { colors,urls } from './Constants';

export default class SelectCurrencyLPG extends React.Component{

    constructor(){
        super()
        this.state = {
            text: '',
            fuel:[],

            loading_status:false,
            fetch_status:false,
            currency:'',
            currency_id:'key0',
            money:[]
        }

    }





    onSelectCurrency(index, value){
      this.setState({currency:value})

    }

next(){

  if(this.state.currency_id == 'key0' ){
    Platform.OS === 'android'
    ? ToastAndroid.show("Select Currency !", ToastAndroid.SHORT)
    : Alert.alert("Select Currency !")
    return
  }
  else{
    var result  = this.props.navigation.getParam('result')
    result['currency'] = this.state.currency
    this.props.navigation.navigate('LPGSelection',{result : result});
  }

}


fetch(){
  NetInfo.isConnected.fetch().then(isConnected => {
    if(!isConnected)
    {
      this.props.navigation.navigate("NoNetwork")
      return;
    }
    else{
      this.setState({loading_status:true})
      let url = urls.base_url + 'api_get_currencey'
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },


      }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({loading_status:false})
              if(!responseJson.error){
                var length = responseJson.result.length

                var temp_arr = []
              //	ToastAndroid.show("fetching",ToastAndroid.LONG)
                for(var i = 0 ; i < length ; i++){
                  var name = responseJson.result[i].Currency.name
                  var id = responseJson.result[i].Currency.id
                  var symbol = responseJson.result[i].Currency.symbol

                  const array = [...temp_arr];
                  array[i] = { ...array[i], key:id };
                  array[i] = { ...array[i], name: name };
                  array[i] = { ...array[i], symbol: symbol };
                  temp_arr = array


                }
                this.setState({ money: temp_arr });
              }

              //ToastAndroid.show(JSON.stringify(this.state.data),ToastAndroid.LONG)
            }).catch((error) => {
              console.error(error);

              Platform.OS === 'android'
    ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
    : Alert.alert("Error !")



              //ToastAndroid.show(JSON.stringify("Error !"),ToastAndroid.LONG)
            });

    }

   })

}



    continue(){


      if(this.state.subfuelid.length === 0 || this.state.subfuelid ==="key0"){
        this.setState({text: "Please select fuel type !"})
       // ToastAndroid.show("Please select fuel type !", ToastAndroid.SHORT);
        Platform.OS === 'android'
        ? ToastAndroid.show("Please select fuel type !", ToastAndroid.SHORT)
        : Alert.alert("Please select fuel type !")
        return
      }


	   Alert.alert(
      'Save Vehicle',
      'Do you want to add this vehicle?',
      [
      {
        text: 'Cancel',
        onPress: () => {
          this.next();
        },
        style: 'cancel'
      },
      {
        text: 'OK',
        onPress: () => {
          this.add()
          this.next()
        }
      }
      ],
      {
      cancelable: false
      }
    );

    }



    abc(){
      var result  = this.props.navigation.getParam('result')
      //ToastAndroid.show(result["user_id"] +'....'+ result["service_id"]+'--------'+result["location_name"]+'...'+result["make_id"]+'...'+result["vehicle_reg_num"], ToastAndroid.LONG);
    }






    componentDidMount(){
      StatusBar.setBackgroundColor('#32CD32')
        this.fetch()


    }

    render(){

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }



            let makeCurrency =this.state.money.map((money) => {
              return <Picker.Item label={money.name} value={money.key} key={money.key}/>
      })



        return(
            <View style={{ flexGrow: 1 ,marginLeft:15,marginRight :15}}>

            <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginTop:20,marginLeft:10}}>Choose your desired currency</Text>






                           <Form>
                           <Picker style={{ width :'90%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}

                             mode="dropdown"
                             selectedValue={this.state.currency_id}
                             onValueChange={(itemValue, itemIndex) =>
                               {
                                 //this.setState({selected_model: value})
                            //   ToastAndroid.show(itemValue, ToastAndroid.LONG);
                                 this.setState({currency_id: itemValue,currency:itemValue == '1' ? "USD" : "R"})
                            //   Alert.alert(itemValue)
                                   //this.pickerSelect(itemValue)
                                 }
                               }
                             >
                             <Picker.Item label="Select Currency" value="key0" />
                             {makeCurrency}

                           </Picker>
                           </Form>





                  {/* <Text style={{ color: 'red' }}>{this.state.text }</Text> */}


                <TouchableOpacity
                          style={styles.locationScreenButton}
                          onPress={this.next.bind(this)}
                          underlayColor='#fff'>
                          <Text style={styles.locationText}>CONTINUE</Text>
                 </TouchableOpacity>


            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        marginTop: 20,
        padding: 20
    },
    text: {
        padding: 10,
        fontSize: 14,
    },

    input: {
      width: "100%",
      height: 40,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
    locationScreenButton:{
      width: "100%",
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#FFC300',
      borderRadius:2,
      borderWidth: 1,
      borderColor: '#fff'
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    }
})


// {
//  this.state.isHidden ?  <View>
//  <Dropdown
//       label="Select Fuel Type"
//       data={this.state.data}
//       onChangeText={(name , price )=>{
//         //this.setState({subfuelid:value})
//         ToastAndroid.show("subfuelid" + name + "price "+ price  , ToastAndroid.LONG);
//
//       }}
//     />
//     </View> : null
// }
