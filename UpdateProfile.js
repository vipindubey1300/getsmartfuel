import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text, TouchableOpacity,Platform, Image, ScrollView,Alert,ToastAndroid, AsyncStorage ,NetInfo,ActivityIndicator} from 'react-native';
import FormData from 'FormData';
import { colors,urls } from './Constants';




export default class UpdateProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error:null,
      name: '',
	    email: '',
      mobile:'',
      id:0

    };
  }

  static navigationOptions = {
    header: null ,
  };



  isValid() {

    let valid = false;


    if (this.state.name.length > 0 && this.state.email.length > 0 && this.state.mobile.length > 0 ) {
      valid = true;
    }

    if(!this.state.email.includes("@") && !this.state.email.includes(".") && this.state.email.length > 0 ){
    //  this.setState({error:"Please Enter Valid Email Address"})
     // ToastAndroid.show("Please Enter Valid Email Address",ToastAndroid.SHORT)


      Platform.OS === 'android' 
      ? ToastAndroid.show("Please Enter Valid Email Address", ToastAndroid.SHORT)
      : Alert.alert("Please Enter Valid Email Address")


      return false;
    }



    if (this.state.name.length === 0) {
    //  this.setState({ error: 'You must enter your name' });
     // ToastAndroid.show('You must enter your name' ,ToastAndroid.SHORT)


      Platform.OS === 'android' 
      ? ToastAndroid.show('You must enter your name' , ToastAndroid.SHORT)
      : Alert.alert('You must enter your name' )


    }
    else if (this.state.email.length === 0) {
      //this.setState({ error: 'You must enter a email' });
       // ToastAndroid.show('You must enter a email' ,ToastAndroid.SHORT)


        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a email', ToastAndroid.SHORT)
        : Alert.alert('You must enter a email')


    }
    else if (this.state.mobile.length === 0) {
      //this.setState({ error: 'You must enter a contact' });
       // ToastAndroid.show('You must enter a contact' ,ToastAndroid.SHORT)


        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a contact', ToastAndroid.SHORT)
        : Alert.alert('You must enter a contact')


    }


    return valid;
  }
  checkInternet(){


                  NetInfo.isConnected.fetch().then(isConnected => {
                  if(isConnected)
                  {
                  return true;
                  }
                  else{
                    //this.props.navigation.navigate("NoNetwork")
                    return false;
                  }
                 })



  }

  updateProfile = async () =>{

    if (this.isValid()) {

      this.setState({loading_status:true})
          var formData = new FormData();

          await AsyncStorage.getItem('uname')
          .then((item) => {
             if (item) {
              // formData.append('id', item);
                this.setState({id:parseInt(item)})

           }
           else {  }
          });


          formData.append('id', this.state.id);
          formData.append('name', this.state.name);
          formData.append('email', this.state.email);
          formData.append('mobile', this.state.mobile);
        //  ToastAndroid.show("ID;;"+this.state.id,ToastAndroid.SHORT)


        NetInfo.isConnected.fetch().then(isConnected => {
     if(!isConnected)
     {
       this.props.navigation.navigate("NoNetwork")
       return false;
     }
else{

 //fetching all fata from server
 let url = urls.base_url + 'api_updateprofile'
 fetch(url, {
 method: 'POST',
 headers: {
   'Accept': 'application/json',
   'Content-Type':  'multipart/form-data',
 },
 body: formData

}).then((response) => response.json())
     .then((responseJson) => {
     this.setState({loading_status:false})
     if(!responseJson.error){

           
                               Platform.OS === 'android' 
                               ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                               : Alert.alert(JSON.stringify(responseJson.message))

   }else{

   
    Platform.OS === 'android' 
    ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
    : Alert.alert(JSON.stringify(responseJson.message))


     //  this.setState({ error: responseJson.message.toString() });
     }


     }).catch((error) => {
      Platform.OS === 'android'
      ? ToastAndroid.show("Error", ToastAndroid.SHORT)
      : Alert.alert("Error")
     });


}

    })


          }
  }


  render() {


    if (this.state.loading_status) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }



    return (
      <ScrollView>
      <View style={styles.container}>

      <Image source={require('./logo.png')} style={{height:220, width:220}} />



        <TextInput
          value={this.state.name}
          onChangeText={(name) => this.setState({ name })}
          placeholder={'Name'}
          style={styles.input}
          placeholderTextColor={'black'}
        />

        <TextInput
          value={this.state.email}
          onChangeText={(email) => this.setState({ email })}
          placeholder={'Email'}
          style={styles.input}
          placeholderTextColor={'black'}
        />

        <TextInput
          value={this.state.mobile}
          onChangeText={(mobile) => this.setState({ mobile })}
          placeholder={'Contact'}
          style={styles.input}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
          placeholderTextColor={'black'}
        />



        <TouchableOpacity
                  style={styles.loginScreenButton}
                  onPress={this.updateProfile.bind(this)}
                  underlayColor='#000000'>
                  <Text style={styles.loginText}>UPDATE PROFILE</Text>
         </TouchableOpacity>



      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  input: {
    width: "85%",
    height: 44,
    textAlign: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 15,
  },
  loginScreenButton:{
      width: "85%",
      marginTop:10,
      marginBottom : 18,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#050404',
    borderRadius:20,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#F0ED2E',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});
