import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text, TouchableOpacity,Platform, Image, ScrollView,Alert,ToastAndroid, AsyncStorage,ActivityIndicator,NetInfo } from 'react-native';
//import { CheckBox } from 'react-native-elements';
import { colors,urls } from './Constants';
import FormData from 'FormData';

export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {

	    email: '',
      error:null,
      loading_status:false

    };
  }

  static navigationOptions = {
    header: null ,
  };


  checkInternet(){


                  NetInfo.isConnected.fetch().then(isConnected => {
                  if(isConnected)
                  {
                  return true;
                  }
                  else{
                    //this.props.navigation.navigate("NoNetwork")
                    return false;
                  }
                 })



  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

  back(){
    this.props.navigation.goBack()
  }

  isValid() {

    let valid = false;
  //  this.setState({username_error:null,error:null})

    if (this.state.email.length && this.state.email.includes("@") && this.state.email.includes(".")) {
      valid = true;
    }

    if(!this.state.email.includes("@") && !this.state.email.includes(".") && this.state.email.length > 0 ){
      //this.setState({error:"Please Enter Valid Email Address"})
      //ToastAndroid.show(" Enter Valid Email Address",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show(" Enter Valid Email Address", ToastAndroid.SHORT)
      : Alert.alert(" Enter Valid Email Address")


      return false;
    }
    if(this.validateEmail(this.state.email)== false){
     // ToastAndroid.show(" Enter Valid Email Address",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show(" Enter Valid Email Address", ToastAndroid.SHORT)
      : Alert.alert(" Enter Valid Email Address")


      return false;
    }


   if (this.state.email.length === 0) {
      //this.setState({ error: 'You must enter a email' });
      //  ToastAndroid.show("Enter a email",ToastAndroid.SHORT)

        Platform.OS === 'android' 
        ? ToastAndroid.show("Enter a email", ToastAndroid.SHORT)
        : Alert.alert("Enter a email")


    }


    return valid;
  }



  forgotPassword(){

              if(this.isValid()){
                NetInfo.isConnected.fetch().then(isConnected => {
           if(!isConnected)
           {
             this.props.navigation.navigate("NoNetwork")
             return false;
           }
           else{

       //fetching all fata from server

             this.setState({loading_status:true})
               var formData = new FormData();


               formData.append('email', this.state.email);
               let url = urls.base_url + 'api_forgetpwd'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type':  'multipart/form-data',
               },
               body: formData

             }).then((response) => response.json())
                   .then((responseJson) => {
                         this.setState({loading_status:false})
                   if(!responseJson.error){

                        // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                         Platform.OS === 'android' 
                         ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                         : Alert.alert(responseJson.message)




                 }else{
                           //errror in insering data
                          // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                           Platform.OS === 'android' 
                           ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                           : Alert.alert(responseJson.message)


                             this.setState({ error: responseJson.message.toString() });
                   }


                   }).catch((error) => {
                    Platform.OS === 'android'
                    ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                    : Alert.alert("Error")
                   });
     }

          })

              }
  }

  render() {


        if (this.state.loading_status) {
          return (
            <ActivityIndicator
              animating={true}
              style={styles.indicator}
              size="large"
            />
          );
        }



    return (
      <ScrollView>
      <View style={styles.container}>

      <Image source={require('./logo.png')} style={{height:220, width:220}} />



        <TextInput
          value={this.state.email}
          onChangeText={(email) => this.setState({ email })}
          placeholder={'Enter Email'}
          style={styles.input}
          placeholderTextColor={'black'}
        />







        <TouchableOpacity
                  style={styles.loginScreenButton}
                  onPress={this.forgotPassword.bind(this)}
                  underlayColor='#000000'>
                  <Text style={styles.loginText}>SUBMIT</Text>
         </TouchableOpacity>




      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  input: {
    width: "85%",
    height: 44,
    textAlign: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 15,
  },
  loginScreenButton:{
      width: "85%",
      marginTop:10,
      marginBottom : 18,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#050404',
    borderRadius:20,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#F0ED2E',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});
