import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text, WebView,TouchableOpacity, Image, ScrollView,Linking,Alert,Platform,NetInfo,ToastAndroid, AsyncStorage ,ActivityIndicator,StatusBar} from 'react-native';
import CheckBox from 'react-native-checkbox';
//import { CheckBox } from 'react-native-elements'
import firebase from 'react-native-firebase';
import FormData from 'FormData';
import { colors,urls } from './Constants';




export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error:null,
      username: '',
      password: '',
	    email: '',
      contact:'',
      first_name:'',
      last_name:'',
      checked: false,
      username_error:null,
      showProgress:false,
      loading_status:false,
       token:''
    };
  }

  static navigationOptions = {
    header: null ,
  };

  back(){
    this.props.navigation.goBack()
  }

  checkInternet(){


                  NetInfo.isConnected.fetch().then(isConnected => {
                  if(isConnected)
                  {
                  return true;
                  }
                  else{
                    //this.props.navigation.navigate("NoNetwork")
                    return false;
                  }
                 })



  }


          //1
 checkPermission(){
  // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);

  

       firebase.messaging().hasPermission()
       .then(enabled => {
         if (enabled) {
           // user has permissions
           this.getToken()
         } else {
           // user doesn't have permission
           this.requestPermission()
         } 
       });
   
 }
 
   //3
 getToken= async () => {
  let fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
      // user has a device token
     // ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
    this.setState({token:fcmToken})
  }
  else{
   //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
  }
 }
 
   //2
  requestPermission(){

       firebase.messaging().requestPermission()
     .then(() => {
       // User has authorised  
       this.getToken();
     })
     .catch(error => {
       // User has rejected permissions  
       //ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);

        
       Platform.OS === 'android' 
       ?  ToastAndroid.show("Permission Denied", ToastAndroid.SHORT)
       : Alert.alert("Permission Denied")


     });
       
 }


  webview(){
    return(
      <WebView
          source={{uri: 'https://github.com/facebook/react-native'}}
          style={{marginTop: 20}}
        />
    )

  }
  isValid() {

    let valid = false;


    if (this.state.username.length > 0 && this.state.password.length > 0 && this.state.email.length > 0 && this.state.contact.length > 0 && this.state.email.includes("@") && this.state.checked) {
      valid = true;
    }





    if (this.state.username.length === 0) {
    //  this.setState({ error: 'You must enter your username' });
        //ToastAndroid.show('You must enter your username', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter your username', ToastAndroid.SHORT)
        : Alert.alert('You must enter your username')


        return false;
    }
    if (this.state.first_name.length === 0) {
      //  this.setState({ error: 'You must enter your username' });
         // ToastAndroid.show('You must enter your first name', ToastAndroid.SHORT);

          Platform.OS === 'android' 
          ? ToastAndroid.show('You must enter your first name', ToastAndroid.SHORT)
          : Alert.alert('You must enter your first name')


          return false;
      }
      if (this.state.last_name.length === 0) {
        //  this.setState({ error: 'You must enter your username' });
           // ToastAndroid.show('You must enter your last name', ToastAndroid.SHORT);

            Platform.OS === 'android' 
            ? ToastAndroid.show('You must enter your last name', ToastAndroid.SHORT)
            : Alert.alert('You must enter your last name')

            return false;
        }
    if (this.state.email.length === 0) {
    //  this.setState({ error: 'You must enter a email' });
        //ToastAndroid.show('You must enter a email', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter an email', ToastAndroid.SHORT)
        : Alert.alert('You must enter an email')


        return false;
    }
    if (this.state.contact.length === 0) {
    //  this.setState({ error: 'You must enter a contact' });
       // ToastAndroid.show('You must enter a phone number', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a phone number', ToastAndroid.SHORT)
        : Alert.alert('You must enter a phone number')


        return false;
    }
    if (this.state.password.length === 0) {
    //  this.setState({ error: 'You must enter a password' });
       // ToastAndroid.show('You must enter a password', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a password', ToastAndroid.SHORT)
        : Alert.alert('You must enter a password')


        return false;
    }


    if(!this.state.email.includes("@") || !this.state.email.includes(".") && this.state.email.length > 3 ){
    //  this.setState({username_error:"Please Enter Valid Email Address",error:null})
     // ToastAndroid.show("Please Enter Valid Email Address", ToastAndroid.SHORT);

      Platform.OS === 'android' 
      ? ToastAndroid.show("Please Enter Valid Email Address", ToastAndroid.SHORT)
      : Alert.alert("Please Enter Valid Email Address")


      return false;
    }


    var isnum = /^\d+$/.test(this.state.contact);
    if(!isnum){
     // ToastAndroid.show("Please Enter Valid Contact Number", ToastAndroid.SHORT);

      Platform.OS === 'android' 
      ? ToastAndroid.show("Please Enter Valid Contact Number", ToastAndroid.SHORT)
      : Alert.alert("Please Enter Valid Contact Number")


      return false;
    }

    if(this.state.contact.length < 10 || this.state.contact.length > 13){
     // ToastAndroid.show("Contact Number is not valid", ToastAndroid.SHORT);

      Platform.OS === 'android' 
      ? ToastAndroid.show("Contact Number is not valid", ToastAndroid.SHORT)
      : Alert.alert("Contact Number is not valid")


      return false;
    }

    if(!this.state.checked){
     this.setState({ error: 'Please check Checkbox' });
       //ToastAndroid.show('Kindly Accept Terms & Conditions', ToastAndroid.SHORT);

       Platform.OS === 'android' 
       ? ToastAndroid.show('Kindly Accept Terms & Conditions', ToastAndroid.SHORT)
       : Alert.alert('Kindly Accept Terms & Conditions')


       return false;
   }



    return valid;
  }
  componentDidMount() {

      StatusBar.setBackgroundColor('#32CD32')
      this.checkPermission()
 }
  onSignup(){
    fetch('http://192.168.0.76/fuelex/signup.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      user_name: this.state.username,
      password: this.state.password,
      contact: this.state.contact,
      email: this.state.email,


    }),

  }).then((response) => response.json())
        .then((responseJson) => {
        //Alert.alert(responseJson);

        if(responseJson === 'True'){
        //success in inserting data
        Alert.alert("success");

      }else if(responseJson === 'False'){
          //errror in insering data
          Alert.alert("failed");
        }


        }).catch((error) => {
          console.error(error);
        });

  }
  onSignupWithServer(){

    if (this.isValid()) {
      NetInfo.isConnected.fetch().then(isConnected => {
             if(!isConnected)
             {
               this.props.navigation.navigate("NoNetwork")
               return false;
             }
       else{

         //fetching all fata from server

                 this.setState({loading_status:true})
                   var check_status = 1
                   if(this.state.checked){
                     check_status = 0
                   }

                   var formData = new FormData();

                   formData.append('username', this.state.username);
                   formData.append('password', this.state.password);
                   formData.append('mobile', this.state.contact);
                   formData.append('first_name', this.state.first_name);
                   formData.append('last_name', this.state.last_name);
                   formData.append('email', this.state.email);
                 formData.append('agree',check_status)
                 formData.append('device_token', this.state.token);
                // Alert.alert(JSON.stringify(formData));
                let url = urls.base_url + 'api_signup'
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type':  'multipart/form-data',
                   },
                   body: formData

                 }).then((response) => response.json())
                       .then((responseJson) => {
                         this.setState({loading_status:false})
                       if(!responseJson.error){
                             //success in inserting data
                             Platform.OS === 'android' 
                            ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                            : Alert.alert(JSON.stringify(responseJson.message))




                             var id = responseJson.userid
                             AsyncStorage.setItem('uname', id);
                               //this.setState({showProgress:false})
                             this.props.navigation.navigate('Home');

                     }else{

                               this.setState({error:responseJson.message,showProgress:false})

                               
                               Platform.OS === 'android' 
                               ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                               : Alert.alert(JSON.stringify(responseJson.message))
                       }


                       }).catch((error) => {
                        Platform.OS === 'android'
                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                        : Alert.alert("Error")
                       });

       }

            })


          }
  }


  render() {

    if (this.state.loading_status) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }




    return (
      <ScrollView>
      <View style={styles.container}>

      <Image source={require('./logo.png')} style={{height:200, width:220}} />


        <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username : username})}
          placeholder={'Username'}
          style={styles.input}
          autoCorrect={false}
          autoCapitalize={'none'}
          underlineColorAndroid="#FFFFFF"
          placeholderTextColor={'black'}
        />


        <TextInput
        value={this.state.first_name}
        onChangeText={(first_name) => this.setState({ first_name : first_name})}
        placeholder={'First Name'}
        style={styles.input}
        autoCorrect={false}
        autoCapitalize={'none'}
        underlineColorAndroid="#FFFFFF"
        placeholderTextColor={'black'}
      />

      <TextInput
      value={this.state.last_name}
      onChangeText={(last_name) => this.setState({last_name : last_name})}
      placeholder={'Last Name'}
      style={styles.input}
      autoCorrect={false}
      autoCapitalize={'none'}
      underlineColorAndroid="#FFFFFF"
      placeholderTextColor={'black'}
    />


        <TextInput
          value={this.state.email}
          onChangeText={(email) => this.setState({ email:email.trim() })}
          placeholder={'Email ID'}
          autoCorrect={false}
          autoCapitalize={'none'}
          underlineColorAndroid="#FFFFFF"
          style={styles.input}
          placeholderTextColor={'black'}
        />

        <TextInput
          value={this.state.contact}
          onChangeText={(contact) => this.setState({ contact:contact.trim() })}
          placeholder={'Contact no'}
          style={styles.input}
          underlineColorAndroid="transparent"
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
          placeholderTextColor={'black'}
        />

        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password:password.trim() })}
          placeholder={'Password'}
          secureTextEntry={true}
          autoCorrect={false}
          autoCapitalize={'none'}
           underlineColorAndroid="#FFFFFF"
          style={styles.input}
          placeholderTextColor={'black'}
        />





            <View style={{flexDirection: 'row',justifyContent: 'space-around',alignItems: 'center',marginBottom:5 }}>

            <CheckBox

                label=''
                checked={this.state.checked}
                onChange={(checked) => {
                  this.setState({checked :!this.state.checked})
                  //ToastAndroid.show("Status "+ this.state.checked, ToastAndroid.LONG);
                }}
                />

              <Text style={{fontSize : 15,color:'black',marginLeft:15}} onPress={() => {  this.props.navigation.navigate('Terms')}}>Accept all Terms & Conditions</Text>

           </View>



         { this.state.showProgress &&
           <ActivityIndicator animating={true} size="large"/>
         }

        <TouchableOpacity
                  style={styles.loginScreenButton}
                  onPress={this.onSignupWithServer.bind(this)}
                  underlayColor='#000000'>
                  <Text style={styles.loginText}>SIGN UP</Text>
         </TouchableOpacity>

         <View style={{flexDirection: 'row'}}>
         <Text style = {{ color: 'black' ,fontSize: 20}}>Already Have an Account ?</Text>
       <Text onPress={this.back.bind(this)} style = {{ color: 'black', fontSize: 20 ,fontWeight: 'bold' }}> Login</Text>
       </View>


      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  input: {
    width: "85%",
    height: 44,
    textAlign: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 15,
  },
  loginScreenButton:{
      width: "85%",
      marginTop:10,
      marginBottom : 18,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#050404',
    borderRadius:20,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#F0ED2E',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});
