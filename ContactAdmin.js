import React from 'react';
import { StyleSheet, Text, View , TextInput,Button,TouchableOpacity,AsyncStorage,Platform,ToastAndroid,NetInfo,ActivityIndicator,Alert } from 'react-native';
import FormData from 'FormData';
import { colors,urls } from './Constants';

export default class ContactAdmin extends React.Component{

    constructor(props){
        super(props);
        this.state = {

          subject: '',
          message:'',
          loading_status:false
        };
    }

    componentDidMount(){

    }

    isValid(){



      let valid = false;

      if (this.state.subject.length > 0 && this.state.message.length > 0) {
        valid = true;
      }

      if (this.state.subject.length === 0) {

       // ToastAndroid.show('You must enter subject', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter subject', ToastAndroid.SHORT)
        : Alert.alert('You must enter subject')



      } else if (this.state.message.length === 0) {

     //   ToastAndroid.show('You must enter message', ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter message', ToastAndroid.SHORT)
        : Alert.alert('You must enter message')




      }

      return valid;
    }

continue = async () =>{
  if(this.isValid()){
  var formData = new FormData();
    var id = -1
    await  AsyncStorage.getItem('uname')
          .then((item) => {
          if (item) {
           id = item
              // ToastAndroid.show(id, ToastAndroid.LONG);
                //ToastAndroid.show(id, ToastAndroid.LONG);
          formData.append('userid', id);
          formData.append('subject', this.state.subject);
          formData.append('message', this.state.message);
              //  formData.append('userid', id);

             //Alert.alert(JSON.stringify(formData))
        NetInfo.isConnected.fetch().then(isConnected => {
        if(!isConnected)
        {
          this.props.navigation.navigate("NoNetwork")
          return;
        }
        else{
          let url = urls.base_url + 'api_contact_toadmin'
                          this.setState({loading_status:true})
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type':  'multipart/form-data',
                          },
                          body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                                console.log(responseJson)
                                this.setState({loading_status:false})
//Alert.alert(JSON.stringify(responseJson))
//Alert.alert('responseJson.message')
                              if(!responseJson.error){

                                  //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                                  Platform.OS === 'android' 
                                  ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)

                                  



                            }else{
                                      //errror in insering data
                                    // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                                     Platform.OS === 'android' 
                                     ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                     : Alert.alert(responseJson.message)
               


                              }


                              }).catch((error) => {
                                 Platform.OS === 'android' 
                                     ? ToastAndroid.show("Error ", ToastAndroid.SHORT)
                                     : Alert.alert("Error ")
                              });

        }

       })

          }
          else {

          //ToastAndroid.show("No id found for user", ToastAndroid.LONG);
          }
          });
        
     }
  }

    render(){
      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }

        return(
            <View style={styles.container}>


                <Text style = {{color: 'green', fontSize: 25,fontWeight: 'bold' ,marginBottom:15}}>Send Message To Admin</Text>

                <TextInput
                  placeholder={'Subject'}
                  style={styles.input}
                  value={this.state.subject}
                  onChangeText={(subject) => this.setState({ subject })}

                />

                <TextInput
                  placeholder={'Message...'}
                  style={styles.inputTwo}
                  value={this.state.message}
                  onChangeText={(message) => this.setState({ message })}
                />




                <TouchableOpacity
                          style={styles.locationScreenButton}
                          onPress={this.continue.bind(this)}
                          underlayColor='#fff'>
                          <Text style={styles.locationText}>CONTINUE</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
      marginLeft:15,
      marginRight:15
    },
    text: {
        padding: 10,
        fontSize: 14,
    },
    input: {
      width: "100%",
      height: 45,
      padding: 10,
      textAlign: 'center',
      borderRadius: 10,
      borderWidth: 2,
      borderColor: 'black',
      marginBottom: 20,
    },
    inputTwo: {
      width: "100%",
      height: "50%",
      padding: 10,
      textAlign: 'center',
      borderRadius: 10,
      borderWidth: 2,
      borderColor: 'black',
      marginBottom: 20,
    },
    locationScreenButton:{
      width: "100%",
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#FFC300',
      borderRadius:10,
      borderWidth: 1,
      borderColor: '#fff'
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    }

})
