import React from 'react';
import { StyleSheet,TextInput, Text, View ,Image,Button, Platform,ScrollView,TouchableOpacity,AsyncStorage,ToastAndroid,ActivityIndicator,StatusBar,NetInfo,Alert} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

import FormData from 'FormData';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import PayPal from 'react-native-paypal-wrapper';
import firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';
import { colors,urls } from './Constants';


 export default class LPGSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        data:[],
        loading_status:false,
        selectedValue:'key0',
        id:0,
        price:0,
        name:'',
        quantity:0,
        total_price:0,
        cash_status:'cash',
        isDateTimePickerVisible: false,
        isDatePickerVisible: false,
        date:'',
        time:'',
        user_id:0,
        delivery_fee:0,
        currency:''
  };

  }
  //this is used to remove toolbar in given screen
  static navigationOptions = {
    header: null ,
  };



  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
       // this.showAlert(title, body);
        //ToastAndroid.show("Notfictatoin recieved......"+JSON.stringify(notification),ToastAndroid.LONG)
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        //const { title, body } = notificationOpen.notification;
       // this.showAlert(title, body);
      // ToastAndroid.show("Notfictatoin openingggggg......"+JSON.stringify(notification),ToastAndroid.LONG)
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
       // const { title, body } = notificationOpen.notification;
        //this.showAlert(title, body);
        //ToastAndroid.show("notification inintalss....",ToastAndroid.LONG)
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
     // console.log(JSON.stringify(message));
     // ToastAndroid.show(JSON.stringify(message),ToastAndroid.LONG)
      this.displayNotification(message)
    });
  }
  displayNotification = (notification) => {
      if (Platform.OS === 'android') {
          const localNotification = new firebase.notifications.Notification({
              sound: 'default',
              show_in_foreground: true,
          }).setNotificationId(notification._from)
          .setTitle(notification._data.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
              .android.setChannelId('notification_channel_name') // e.g. the id you chose above
              .android.setSmallIcon('logo') // create this icon in Android Studio
              .android.setColor('#D3D3D3') // you can set a color here
              .android.setPriority(firebase.notifications.Android.Priority.High);

          firebase.notifications()
              .displayNotification(localNotification)
              .catch(err => console.error(err));

      }
      else if (Platform.OS === 'ios') {
        console.log(notification);
        const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification._from)
            .setTitle(notification._data.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification._data.content)
            .setData(notification.data)
            //.ios.setBadge(notification.ios.badge);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));

    }
  }


  isValid() {






    if (this.state.selectedValue > 0 && this.state.selectedValue!== 'key0' && this.state.quantity > 0 && this.state.date.length > 0 && this.state.time.length > 0) {

      return true
    }

    else if (this.state.selectedValue < 0 || this.state.selectedValue === 'key0' || this.state.selectedValue == 0 ) {

     // ToastAndroid.show('Select LPG Type', ToastAndroid.SHORT);

      Platform.OS === 'android'
      ? ToastAndroid.show('Select LPG Type', ToastAndroid.SHORT)
      : Alert.alert('Select LPG Type')


     return false

    } else if (this.state.quantity == 0) {

      //ToastAndroid.show('Enter Quantity', ToastAndroid.SHORT);

      Platform.OS === 'android'
      ? ToastAndroid.show('Enter Quantity', ToastAndroid.SHORT)
      : Alert.alert('Enter Quantity')


      return false
    }
    else if (this.state.date.length === 0 || this.state.date.trim().length === 0 || this.state.date ==='') {

     // ToastAndroid.show('Enter a Date', ToastAndroid.SHORT);

      Platform.OS === 'android'
      ? ToastAndroid.show('Enter a Date', ToastAndroid.SHORT)
      : Alert.alert('Enter a Date')


      return false
    } else if (this.state.time.length === 0) {
    //  this.setState({ error: 'You must enter a password' });
     // ToastAndroid.show('Enter a time', ToastAndroid.SHORT);

      Platform.OS === 'android'
      ? ToastAndroid.show('Enter a time', ToastAndroid.SHORT)
      : Alert.alert('Enter a time')


      return false
    }



}




  order(){
    if(this.isValid()){
      var result  = this.props.navigation.getParam('result')

      //result["location_name"] = this.state.address
    }




      //ToastAndroid.show("name  ..."+this.state.address+"LAtit..."+this.state.latitude, ToastAndroid.LONG);
    //this.props.navigation.navigate('AddVehicleOne',{result : result});
  }



  getDelivery(){
    let url = urls.base_url + 'api_get_delivery_fee'
                                fetch(url, {
                                method: 'GET',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'multipart/form-data',
                                },


                              }).then((response) => response.json())
                                    .then((responseJson) => {
                                      this.setState({loading_status:false})

                                        if(!responseJson.error){

                                            var fee = responseJson.result.DeliveryFee.amount
                                            var fee_africa = responseJson.result.DeliveryFee.amount_in_africa
                                            this.setState({
                                                delivery_fee :  this.state.currency == 'USD' ? fee : fee_africa
                                            })
                                        }


                                  }
                                    ).catch((error) => {
                                      Platform.OS === 'android'
                                      ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
                                      : Alert.alert("Error !")

                                    });



  }

  fetch(){
    NetInfo.isConnected.fetch().then(isConnected => {
    if(!isConnected)
    {
      this.props.navigation.navigate("NoNetwork")
      return;
    }
    else{
      var formData = new FormData();
      // if(!this.state.loading_status){
      //   this.setState({loading_status:true,fuel_id:fuelid})
      // }

      this.setState({loading_status:true})

      formData.append('fuel_id', 4);
      let url = urls.base_url + 'api_get_all_subfueltype'
                                fetch(url, {
                                method: 'POST',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'multipart/form-data',
                                },
                                body: formData

                              }).then((response) => response.json())
                                    .then((responseJson) => {
                                      this.setState({loading_status:false})
                                      // if(this.state.loading_status){
                                      //   this.setState({loading_status:false,fuel_id:fuelid})
                                      // }
                                      if(!responseJson.error){
                                        var length = responseJson.result.length
                                        if(length == 0){
                                        //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                                        //ToastAndroid.show("No types found", ToastAndroid.LONG);

                                        Platform.OS === 'android'
                                        ? ToastAndroid.show("No types found", ToastAndroid.SHORT)
                                        : Alert.alert("No types found")


                                        }
                                        else{
                                          //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                                          for(var i = 0 ; i < length ; i++){
                                          //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                                          var name = responseJson.result[i].Subfuel.sub_fuel_type
                                          var subfuelid= responseJson.result[i].Subfuel.id
                                          var price = responseJson.result[i].Subfuel.price
                                          var price_africa = responseJson.result[i].Subfuel.price_africa

                                                const array = [...this.state.data];

                                                array[i] = { ...array[i], name: name };
                                                array[i] = { ...array[i], id : subfuelid };
                                                array[i] = { ...array[i], price: price };
                                                  array[i] = { ...array[i], price_africa: price_africa };
                                                this.setState({data: array });

                                          }
                                        }



                                      }


                                    else{
                                     // ToastAndroid.show("Error !", ToastAndroid.LONG);

                                      Platform.OS === 'android'
                                      ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
                                      : Alert.alert("Error !")


                                    }

                                  }
                                    ).catch((error) => {
                                      Platform.OS === 'android'
                                      ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
                                      : Alert.alert("Error !")

                                    });

    }

   })



  }

  async getUserId(){
    var id = await AsyncStorage.getItem('uname')
    return id
  }


  async componentDidMount() {

    this.createNotificationListeners();



      var result  = this.props.navigation.getParam('result')
    //  ToastAndroid.show(result['currency'],ToastAndroid.LONG)
      this.setState({
        currency:result['currency']
      })
    await this.fetch()
    await this.getDelivery()


  }
  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }



  onSelect(index, value){
    if(value === 'cash'){
      this.setState({cash_status:'cash'})
    }
    else if(value === 'online'){
      this.setState({cash_status:'online'})
    }
  }

    //for time
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (time) => {
      console.log('A date has been picked: ', time);
      this.setState({time:time.toString().substring(16,24)})
      this._hideDateTimePicker();
    };


   //for date
    showDatePicker = () => this.setState({ isDatePickerVisible: true });

     hideDatePicker = () => this.setState({ isDatePickerVisible: false });

     handleDatePicked = (date) => {

       //console.log('A date has been picked: ', date);
       this.setState({date:this.convertDate(date)})
       this.hideDatePicker();
     };


  cashOnDelivery(){


       AsyncStorage.getItem('uname')
			.then((item) => {
     if (item) {

      var result  = this.props.navigation.getParam('result')
      var loc_name = result["location_name"]
      var loc_lat = result["location_lat"]
      var loc_long = result["location_long"]


      //ToastAndroid.show("Please Wait !",ToastAndroid.LONG)


      var formData = new FormData();
      formData.append('user_id',item);
      formData.append('service_id', 10);
      formData.append('sub_service_id', 46);
      formData.append('price',this.state.total_price);
      formData.append('location_name',loc_name);
      formData.append('location_lat',loc_lat);
      formData.append('location_long',loc_long);
      formData.append('fuel_id',4);
      formData.append('fuel_subtype_id',this.state.id);
      formData.append('qty',this.state.quantity);
      formData.append('booking_date',this.state.date);
      formData.append('discount_amt',0);
formData.append('currency',this.state.currency);
      formData.append('booking_time',this.state.time);
         formData.append('booking_amt',this.state.total_price);
         formData.append('net_amt',parseInt(this.state.total_price) + parseInt(this.state.delivery_fee));
         formData.append('delivery_fee',this.state.delivery_fee);
      formData.append('payment_method',"cash");
         this.setState({loading_status:true})


      console.log(JSON.stringify(formData))
       // Alert.alert(JSON.stringify(formData))


       let url = urls.base_url + 'api_make_booking_lpg'
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body: formData

      }).then((response) => response.json())
            .then((responseJson) => {

                this.setState({loading_status:false})
               // Alert.alert(JSON.stringify(responseJson))


             if(!responseJson.error){
               //  ToastAndroid.show("Booked Succesfully", ToastAndroid.LONG);

                 Platform.OS === 'android'
                 ? ToastAndroid.show("Booked Succesfully", ToastAndroid.SHORT)
                 : Alert.alert("Booked Succesfully")


              this.props.navigation.navigate('HomeScreen');
             }

            }).catch((error) => {
            //  ToastAndroid.show("Error !",ToastAndroid.LONG)
console.log(error.message)
              Platform.OS === 'android'
              ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
              : Alert.alert("Error !")


            });
   }
   else {
          // do something else means go to login
        //  ToastAndroid.show("Error !",ToastAndroid.LONG)

          Platform.OS === 'android'
          ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
          : Alert.alert("Error !")


      }
});





     }

     paypalpaymentapi(price,bid,pid){

      //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

                var result  = this.props.navigation.getParam('result')
                var uid = result["user_id"]
                  //ToastAndroid.show(price+"...."+bid+"..."+uid+"...", ToastAndroid.SHORT);
                 var date = this.convertDate(new Date())

                //ToastAndroid.show("price"+price+"bokingid...."+bid+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
                var formData = new FormData();
                //txnid and payal id should have same value
                formData.append('user_id', uid.toString());
                formData.append('order_id',bid.toString());
                formData.append('txn_status',"approved".toString());
                formData.append('txn_id', pid.toString());
                formData.append('txn_amt',price);
                formData.append('txn_date',date.toString());
                formData.append('paypal_id',pid.toString());
         formData.append('currency',this.state.currency)
         let url = urls.base_url + 'api_make_payment'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type':  'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {

                    if(!responseJson.error){
                          //success in inserting data
                         // ToastAndroid.show("Booked Succesfully", ToastAndroid.LONG);

                          Platform.OS === 'android'
                          ? ToastAndroid.show("Booked Succesfully", ToastAndroid.SHORT)
                          : Alert.alert("Booked Succesfully")


                          this.props.navigation.navigate('HomeScreen');

                  }else{


                           // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                            Platform.OS === 'android'
                            ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                            : Alert.alert(JSON.stringify(responseJson.message))


                    }


                    }).catch((error) => {
                      console.error(error);
                    });
      }


     paypal(price,bookingId){

     // PayPal.initialize(PayPal.SANDBOX, "ARFiHtTEV2P17X4Hdei6M4vqoLx_UVGAvELcTpNprLC4hNklLmNq7cl_z5OqhRGoBzzVRmS9sG-RfhXx");
     PayPal.initialize(PayPal.PRODUCTION, "ASwGC44QIOilJNxdU0IVmTalJ1Y2zjS-k7NkTykVXld9fzDJ-XEQq5pVkpSbkPC0W_SaNcB8D6yhgZGb");
      PayPal.pay({
        price: price.toString().substring(0,3),
        currency: "USD" ,
        description: 'Payment for fuel delivery app',
      }).then(confirm => {

        var pid = confirm.response.id
        this.paypalpaymentapi(price,bookingId,pid)


        //ToastAndroid.show("price"+price+"bokingid...."+bookingId+"uiddd..."+pid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
      })
        .catch(error =>{
          //ToastAndroid.show("Transaction declined by user", ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show("Transaction declined by user", ToastAndroid.SHORT)
          : Alert.alert("Transaction declined by user")

          this.setState({cash_status:'cash'})

        })


  }


  convertCurrency(price,bid){
    this.setState({loading_status:true})
    var formData = new FormData();
  
    formData.append('amount', price);
  
  console.log("price......"+price)
    let url = urls.base_url + 'get_converted_currency'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
  
         }).then((response) => response.json())
               .then((responseJson) => {
              this.setState({loading_status:false})
              console.log(JSON.stringify(responseJson))
               if(!responseJson.error){


                var amt = responseJson.converted_amt
                this.paypal(amt,bid)
               }
  
  
  
  
  
               }).catch((error) => {
                Platform.OS === 'android'
                ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                : Alert.alert("Error")
               });
  }

onlinePayment(){
      this.setState({loading_status:true})

      AsyncStorage.getItem('uname')
     .then((item) => {
    if (item) {

     var result  = this.props.navigation.getParam('result')
     var loc_name = result["location_name"]
     var loc_lat = result["location_lat"]
     var loc_long = result["location_long"]


   //  ToastAndroid.show("Please Wait !",ToastAndroid.LONG)


     var formData = new FormData();
     formData.append('user_id',item);
     formData.append('service_id', 10);
     formData.append('sub_service_id', 46);
     formData.append('price',this.state.total_price);
     formData.append('location_name',loc_name);
     formData.append('location_lat',loc_lat);
     formData.append('location_long',loc_long);
     formData.append('fuel_id',4);
     formData.append('fuel_subtype_id',this.state.id);
     formData.append('qty',this.state.quantity);
     formData.append('booking_date',this.state.date);
     formData.append('booking_time',this.state.time);
     formData.append('net_amt',parseInt(this.state.total_price) + parseInt(this.state.delivery_fee));
         formData.append('delivery_fee',this.state.delivery_fee);
     formData.append('payment_method',"online");
        formData.append('discount_amt',0);
        formData.append('booking_amt',this.state.total_price);
        formData.append('currency',this.state.currency);
console.log(JSON.stringify(formData))
        let url = urls.base_url + 'api_make_booking_lpg'
     fetch(url, {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'multipart/form-data',
       },
       body: formData

     }).then((response) => response.json())
           .then((responseJson) => {
               // ToastAndroid.show(JSON.stringify(responseJson.message),ToastAndroid.LONG)
               // Alert.alert(responseJson)
            this.setState({loading_status:false})


            if(!responseJson.error){
              var booking_id = responseJson.booking_id
                          // if(!empty(booking_id)){
                          //   booking_id = 0.toString()
                          // }

                           //ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
                           let price = parseFloat(this.state.total_price) + parseFloat(this.state.delivery_fee)


                           this.state.currency == 'USD' 
                           ?  this.paypal(price,booking_id)
                           : this.convertCurrency(price,booking_id)
                          //this.props.navigation.pop(3)
                            return true;
            }

           }).catch((error) => {

          Platform.OS === 'android'
          ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
          : Alert.alert("Error !")

           });
  }
  else {
         // do something else means go to login

         Platform.OS === 'android'
         ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
         : Alert.alert("Error !")

     }
});



}

     backup() {
      let url = urls.base_url + 'api_make_booking'
          fetch(url, {
                 method: 'POST',
                 headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data',
                 },
                 body: formData

               }).then((response) => response.json())
                     .then((responseJson) => {
                      // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                      this.setState({loading_status:false})
                     if(!responseJson.error){
                       //use this booking id to hit api
                          var booking_id = responseJson.booking_id
                          // if(!empty(booking_id)){
                          //   booking_id = 0.toString()
                          // }
                          var result  = this.props.navigation.getParam('result')
                           //ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
                          this.paypal(this.state.global_price,booking_id)
                          //this.props.navigation.pop(3)
                            return true;
                         }else{

                           }

                     }).catch((error) => {
                       console.error(error);
                     });

     }

     continue(){
      if(this.isValid()){
      //  ToastAndroid.show('going in cassssshhh', ToastAndroid.SHORT);
            if(this.state.cash_status === 'cash'){


                                      NetInfo.isConnected.fetch().then(isConnected => {
                                                     if(!isConnected)
                                                     {
                                                       this.props.navigation.navigate("NoNetwork")
                                                       return false;
                                                     }
                                                     else{

                                                       //fetching all fata from server
                                                    //   ToastAndroid.show('going in cassssshhh', ToastAndroid.SHORT);
                                                       this.cashOnDelivery()

                                                     }

                              })

             //
            }
            else if(this.state.cash_status === 'online'){
              //ToastAndroid.show('going in onlineeeeeee', ToastAndroid.SHORT);
                //go to paypal payment

                NetInfo.isConnected.fetch().then(isConnected => {
                               if(!isConnected)
                               {
                                 this.props.navigation.navigate("NoNetwork")
                                 return false;
                               }
                               else{

                                 //fetching all fata from server
                                //

                                   this.onlinePayment()

                               }

        })
            }
          }
     }

       pickerSelect(value) {
        var arr = this.state.data
          for(var i = 0 ; i < arr.length ; i++){
                                            //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                                            var id = arr[i].id
              if (id == value) {

                  if(this.state.currency == 'USD'){
                    this.setState({id:arr[i].id,price:arr[i].price ,name:arr[i].name,total_price:this.state.quantity*arr[i].price})
                  }
                  else{
                    this.setState({id:arr[i].id,price:arr[i].price_africa ,name:arr[i].name,total_price:this.state.quantity*arr[i].price_africa})
                  }
                 // this.setState({id:item.id,price:item.price , name:item.name,total_price:this.state.quantity*item.price})
              }


                                            }
    }


  render() {



            if (this.state.loading_status) {
              return (
                <ActivityIndicator
                  animating={true}
                  style={styles.indicator}
                  size="large"
                />
              );
            }


            let makeSubfuel =this.state.data.map((subfuel) => {
              return <Picker.Item label={subfuel.name} value={subfuel.id} key={subfuel.price}/>
})






    return (

	 	    <ScrollView  style={{ flexGrow: 1 ,marginLeft:15,marginRight :15}}>

        <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginTop:10,alignSelf:'center'}}>Book Your Order</Text>


        <Form>
          <Card style={{marginBottom:20}}>
              <Picker style={{ width :"100%" ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}

                mode="dropdown"
                selectedValue={this.state.selectedValue}
                onValueChange={(itemValue, itemIndex) =>
                  {
                    //this.setState({selected_model: value})
                  //  ToastAndroid.show(itemValue, ToastAndroid.LONG);
                    this.setState({selectedValue: itemValue})
                    if(itemValue === 'key0'){
                      this.setState({total_price:0})
                    }

                    this.pickerSelect(itemValue)





                  }
                  }
                >
                <Picker.Item label="Select LPG Type" value="key0" />
                {makeSubfuel}

              </Picker>
              </Card>
              </Form>

              <TextInput
                    value={this.state.quantity}
                    onChangeText={(quantity) => this.setState({ quantity:parseInt(quantity),total_price:quantity*this.state.price})}
                    placeholder={'Quantity'}
                    placeholderTextColor={'black'}
                    style={styles.input}
                    autoCorrect={false}
                    keyboardType = 'numeric'
                    textContentType='telephoneNumber'
                    autoCapitalize={'none'}

        />


<DateTimePicker
                     mode="date"
                     isVisible={this.state.isDatePickerVisible}
                     onConfirm={this.handleDatePicked}
                     onCancel={this.hideDatePicker}
                     minimumDate = {new Date()}
                     >
                     </DateTimePicker>



                     <View style={{flexDirection: 'row',borderRadius: 10,
                             borderWidth: 2,
                             borderColor: 'black', width :'100%',marginBottom:20,justifyContent:'space-between',paddingRight:3,alignItems:'center'}}>
                     <TextInput style = {{width :'70%',height:45,fontWeight:'bold',color:'black'}} editable={false} placeholder={'Date'} value ={this.state.date} />

                     <TouchableOpacity  onPress={this.showDatePicker}>
                           <Image style = {{width:35,height:35,justifyContent:'flex-end'}} source={require('./calendar.png')}></Image>
                     </TouchableOpacity>

                     </View>



            <View style={{flexDirection: 'row',borderRadius: 10,
                    borderWidth: 2,
                    borderColor: 'black', width :'100%',marginBottom:15,justifyContent:'space-between',paddingRight:3,alignItems:'center'}}>
            <TextInput style = {{width :'70%',height:45,fontWeight:'bold',color:'black'}} placeholder={'Time'} editable={false} value ={this.state.time} />

            <TouchableOpacity  onPress={this._showDateTimePicker}>
                  <Image style = {{width:35,height:35,justifyContent:'flex-end'}} source={require('./clock-circular-outline.png')}></Image>
            </TouchableOpacity>

            </View>



            <DateTimePicker
            mode="time"
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
            >
            </DateTimePicker>
            <RadioGroup
                            size={24}
                            thickness={2}
                            color='#0A0A0A'
                            selectedIndex={0}
                            style={{marginBottom:10}}
                            onSelect = {(index, value) => this.onSelect(index, value)}>

                                <RadioButton value={'cash'} >
                                  <Text style ={{width:'100%'}}>Card Machine</Text>
                                </RadioButton>

                                <RadioButton value={'online'}>
                                  <Text style ={{width:'100%'}}>Paypal</Text>
                                </RadioButton>

                </RadioGroup>


               <View style={{flexDirection:'row',justifyContent:'flex-start',marginLeft:7}}>
                <Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>Delivery Fee : </Text>
                {
                              this.state.currency == 'USD'
                              ? <Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>$</Text>
                              :<Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>R</Text>

                            }
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:10,marginLeft:6}}>{this.state.delivery_fee}</Text>
            </View>



          <View style={{flexDirection:'row',justifyContent:'flex-start',marginLeft:7}}>
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>Estimated Price  :</Text>
                            {
                              this.state.currency == 'USD'
                              ? <Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>$</Text>
                              :<Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>R</Text>

                            }
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginLeft:6}}>{this.state.total_price.toString().length == 0|| Number.isNaN(this.state.total_price) ? "" : parseInt(this.state.total_price) + parseInt(this.state.delivery_fee)}</Text>
                </View>




        <TouchableOpacity
                  style={styles.locationScreenButton}
                  onPress={this.continue.bind(this)}
                  underlayColor='#fff'>
                  <Text style={styles.locationText}>ORDER NOW</Text>
         </TouchableOpacity>

      </ScrollView>

	  )
  }
}
//<Image source={require('./maps.jpg')} style={{height:350, width:"85%",marginBottom:10,marginLeft:17,borderColor:'black',borderWidth:1}} />
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft:15,
    marginRight:15
  },
  input: {
    width: "100%",
    height: 40,
    padding: 5,


    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'black',
    marginBottom: 20,
  },
  submit:{
  marginRight:40,
  marginLeft:40,
  marginTop:10,
  paddingTop:20,
  paddingBottom:20,
  backgroundColor:'#68a0cf',
  borderRadius:20,
  borderWidth: 1,
  borderColor: '#fff'
},
submitText:{
    color:'#fff',
    textAlign:'center',
},
locationScreenButton:{
  width:"100%",
  marginTop:10,
  paddingTop:10,
  paddingBottom:10,
  backgroundColor:'#FFC300',
  borderRadius:10,
  borderWidth: 1,
  borderColor: '#fff',
  marginBottom:10,
},
locationText:{
    color:'black',
    textAlign:'center',
    paddingLeft : 10,
    paddingRight : 10,
    fontSize :20,
    fontWeight : 'bold'
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
});
