import React, { Component } from 'react';
import { View, WebView, StyleSheet,StatusBar } from 'react-native';
import { colors,urls } from './Constants';
export default class TermsWebview extends React.Component {

  // componentDidMount(){
  //     StatusBar.setBackgroundColor('#32CD32')
  //   }

  render() {
    return (
       <View style = {styles.container}>
         <WebView
         source = {{ uri:
          urls.base_url + 'api_terms_condition_page' }}
         />
      </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      height: '100%',
   }
})
