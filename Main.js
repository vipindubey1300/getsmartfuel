import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text, AsyncStorage } from 'react-native';

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.check();
  }
  static navigationOptions = {
      header: null
  }

check = async () =>{
/*
    try {
       let value =  AsyncStorage.getItem('uname');
       if (value != null){
          // go to home
		  this.props.navigation.navigate('Home')
       }
       else {
          // do something else means go to login
		  this.props.navigation.navigate('Login')
      }
    } catch (error) {
      // Error retrieving data
    }
*/
	await AsyncStorage.getItem('uname')
			.then((item) => {
     if (item) {
       this.props.navigation.navigate('Home')
   }
   else {
          // do something else means go to login
		  this.props.navigation.navigate('Login')
      }
});

}

  render() {

    return (
		null
    );
  }
}

/*
AsyncStorage.getItem('user')
.then((item) => {
     if (item) {
       // do the damage
   }
});
*/
