import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text,Platform, TouchableOpacity, Image, ScrollView,Alert,ToastAndroid, AsyncStorage,ActivityIndicator,NetInfo } from 'react-native';
//import { CheckBox } from 'react-native-elements';
import { colors,urls } from './Constants';
import FormData from 'FormData';
import { StackActions } from 'react-navigation';


export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {

	    oldpass: '',
      newpass:'',
      againnewpass:'',
      error:null,
        loading_status:false,

    };
  }

  static navigationOptions = {
    header: null ,
  };

  back(){
    this.props.navigation.goBack()
  }

  isValid() {

    let valid = false;
  //  this.setState({username_error:null,error:null}
  if (this.state.oldpass.length > 0 && this.state.newpass.length > 0 && this.state.againnewpass.length > 0) {
    valid = true;
  }
   if (this.state.oldpass.length === 0) {
    //  this.setState({ error: 'You must enter your old password' });
     // ToastAndroid.show('You must enter your old password',ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show('You must enter your old password', ToastAndroid.SHORT)
      : Alert.alert('You must enter your old password')


    }
    else if (this.state.newpass.length === 0){
    //  this.setState({ error: 'You must enter a new password' });
       // ToastAndroid.show('You must enter a new password',ToastAndroid.SHORT)

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a new password', ToastAndroid.SHORT)
        : Alert.alert('You must enter a new password')


    }
    else if(this.state.againnewpass.length === 0){
    //  this.setState({ error: 'You must enter new password' });
        //ToastAndroid.show('You must enter a new password',ToastAndroid.SHORT)

        Platform.OS === 'android' 
        ? ToastAndroid.show('You must enter a new password', ToastAndroid.SHORT)
        : Alert.alert('You must enter a new password')

        
    }



    return valid;
  }

  checkInternet(){


                  NetInfo.isConnected.fetch().then(isConnected => {
                  if(isConnected)
                  {
                  return true;
                  }
                  else{
                    //this.props.navigation.navigate("NoNetwork")
                    return false;
                  }
                 })



  }
  removeItemValue = async () =>{
          // await AsyncStorage.removeItem('uname', (err) => {
          //
          // this.props.navigation.navigate("HomeScreen")
          // });

          this.props.navigation.navigate('HomeScreen');
          //below is used to reset stacki navigator so that goes infirst screen only
          const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
        });
        this.props.navigation.dispatch(resetAction);


  }

  remove = async () =>{

    await AsyncStorage.removeItem('uname', (err) => {
      const popAction = StackActions.pop({  n: 1,  });

    this.props.navigation.dispatch(popAction);
    this.props.navigation.navigate('Login');

    });


          //below is used to reset stacki navigator so that goes infirst screen only



    //   const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
    // });
    // this.props.navigation.dispatch(resetAction);


  }


  resetPassword= async () =>{
          var a = 0;
              if(this.isValid()){
                this.setState({loading_status:true})
                  var formData = new FormData();

                  await AsyncStorage.getItem('uname')
                  .then((item) => {
                     if (item) {
                       a=item;
                       formData.append('id', item);

                   }
                   else {  }
                  });

                //  formData.append('id', this.state.oldpass);
                //  ToastAndroid.show("ID;;"+a,ToastAndroid.SHORT)
                  formData.append('oldpass', this.state.oldpass);
                  formData.append('password', this.state.newpass);
                  formData.append('cpass', this.state.againnewpass);

                  NetInfo.isConnected.fetch().then(isConnected => {
           if(!isConnected)
           {
             this.props.navigation.navigate("NoNetwork")
             return false;
           }
     else{
             // Alert.alert(JSON.stringify(formData))
       //fetching all fata from server

       let url = urls.base_url + 'api_resetpwd' 
           fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type':  'multipart/form-data',
               },
               body: formData

             }).then((response) => response.json())
               .then((responseJson) => {
                   console.log("SDfdsfdsfsdfdsf")
                       this.setState({loading_status:false})
                       
                   if(!responseJson.error){

                       
                    Platform.OS === 'android' 
                    ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                    : Alert.alert(JSON.stringify(responseJson.message))
                        // this.props.navigation.navigate("HomeScreen")
                    //  this.removeItemValue()


                       this.remove();



                 }else{
                           //errror in insering data
                           
                           Platform.OS === 'android' 
                           ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                           : Alert.alert(JSON.stringify(responseJson.message))
                          // ToastAndroid.show("Password & confirm password are not same", ToastAndroid.SHORT);
                           //  this.setState({ error: responseJson.message.toString() });
                   }


                   }).catch((error) => {
                       console.error(error);
                       Platform.OS === 'android'
                       ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                       : Alert.alert("Error")
                   });


     }

          })

              }
  }

  render() {



                if (this.state.loading_status) {
                  return (
                    <ActivityIndicator
                      animating={true}
                      style={styles.indicator}
                      size="large"
                    />
                  );
                }


    return (
      <ScrollView>
      <View style={styles.container}>

      <Image source={require('./logo.png')} style={{height:220, width:220}} />

          <Text style={{ color: 'red',marginBottom:10 }}>{this.state.error}</Text>

        <TextInput
          value={this.state.oldpass}
          onChangeText={(oldpass) => this.setState({ oldpass })}
          placeholder={'Enter Old Password'}
          style={styles.input}
          placeholderTextColor={'black'}
            secureTextEntry={true}
        />

        <TextInput
          value={this.state.newpass}
          onChangeText={(newpass) => this.setState({ newpass })}
          placeholder={'Enter New Password'}
          style={styles.input}
          placeholderTextColor={'black'}
            secureTextEntry={true}
        />

        <TextInput
          value={this.state.againnewpass}
          onChangeText={(againnewpass) => this.setState({ againnewpass })}
          placeholder={'Confirm Password'}
          style={styles.input}
            secureTextEntry={true}
          placeholderTextColor={'black'}
        />









        <TouchableOpacity
                  style={styles.loginScreenButton}
                  onPress={this.resetPassword.bind(this)}
                  underlayColor='#000000'>
                  <Text style={styles.loginText}>RESET PASSWORD</Text>
         </TouchableOpacity>




      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  input: {
    width: "85%",
    height: 44,
    textAlign: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 15,
  },
  loginScreenButton:{
      width: "85%",
      marginTop:10,
      marginBottom : 18,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#050404',
    borderRadius:20,
    borderWidth: 1,
    borderColor: '#fff'
  },
  loginText:{
      color:'#F0ED2E',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});
