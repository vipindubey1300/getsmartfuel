import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text, SectionList,FlatList,TouchableHighlight,  NetInfo ,AsyncStorage,  ToastAndroid,ActivityIndicator,Platform} from 'react-native';
import { colors,urls } from './Constants';

export default class HistoryDetails extends Component {
  constructor(props) {
    super(props);

        this.state = {
          date:'',
          quantity:'',
            quantity_next:'',
          price:'',
          price_next:'',
          location:'',
          time:'',
          delivery_status:'',
          fueltype:'',
          vehicle:'',
          carmodel:'',
          services:[],
          loading_status:false,
          global:'',
          deliveryfee:0,
          currency:'$',
          price_single:0.0,
          delivery_fee_single:0.0

        };


  }
  //this is used to remove toolbar in given screen
  static navigationOptions = {
    header: null ,
  };

  fetch(booking_id){
    this.setState({loading_status:true})

    let url = urls.base_url + 'api_get_all_bookings_by_order_id?order_id='+booking_id
         fetch(url, {
         method: 'GET',

       }).then((response) => response.json())
             .then((responseJson) => {
               this.setState({loading_status:false})
                // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.SHORT)
             if(!responseJson.error){
               var length = responseJson.result.length
               if(length == 0){

                Platform.OS === 'android'
                ? ToastAndroid.show("No bookings find !!", ToastAndroid.SHORT)
                : Alert.alert("No bookings find !!")


                 // ToastAndroid.show("No bookings find !!",ToastAndroid.SHORT)
               }
               else{
                var make = responseJson.result[0].make_name
                var model = responseJson.result[0].model_name
                var service_id = responseJson.result[0].service_id
                var vehicle = responseJson.result[0].vechicle_name
                var color = responseJson.result[0].color_name
                var fuel = responseJson.result[0].fuel_name
                var date = responseJson.result[0].booking_date
                var time = responseJson.result[0].booking_time
                var sub_fuel = responseJson.result[0].sub_fuel_type
                var location = responseJson.result[0].location_name
                var price = responseJson.result[0].price
                var delivery_status = responseJson.result[0].delivery_status
                var quantity = responseJson.result[0].qty
                var vehicle = responseJson.result[0].vehicle_reg_num
                var delivery_fee = responseJson.result[0].delivery_fee
                var currency = responseJson.result[0].currency

                this.setState({date: `Date             :    ${date}`,
                               global:service_id,
                              quantity_next:fuel === 'LPG Gas' ? `Quantity         :    ${quantity} EA` :`Quantity         :    ${quantity} Litres`,
                          //  quantity:`Quantity         :    ${quantity} Litres`,
                             quantity:service_id === '10' ? `Quantity         :    ${quantity} EA` :`Quantity         :    ${quantity} Litres`,

                            time:time,
                            delivery_status:delivery_status,
                            location:`Location        :    ${location}`,
                            fueltype:`Fuel Type       :    ${fuel}`,
                            vehicle:`Vehicle Type  :    ${vehicle}`,
                            deliveryfee :`Delivery Fee  :     ${currency} ${delivery_fee}`,
                            price:`Price              :     ${currency} ${price}`,
                            price_single:price,
                            delivery_fee_single:delivery_fee,
                            currency:currency,
                            carmodel:model == null ? `Car Model      :    `:`Car Model      :    ${model}`})
                            //
                            // for(var i = 0 ; i < length ; i++){
                            //   const services = [...this.state.services];
                            //   const servicesData = responseJson.result;
                            //   servicesData.map((services, index) => {
                            //        services[index] = {}
                            //        services[index]["id"] = i + 1
                            //        services[index]["service"] = services.service_name
                            //        services[index]["sub_service"] = services.sub_service_name
                            //
                            //     });
                            //   this.setState({services});
                            //
                            // }


                            if(length >  0){
                              for(var i = 0 ; i < length ; i++){
                              var name = responseJson.result[i].service_name
                              var sub_name= responseJson.result[i].sub_service_name

                                    // this.setState({
                                    //     make: [...this.state.make, year]
                                    // });
                                    const array = [...this.state.services];
                                    array[i] = { ...array[i], id:i+1};
                                    array[i] = { ...array[i], service: name };
                                    array[i] = { ...array[i], sub_service :sub_name };
                                    this.setState({ services: array});
                                   // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                              }
                            }

                            else{
                              let arr=[];
                              let array=[];
                              this.setState({services:array})

                            }


               }





             }
             else{
               Alert.alert("Cant Connect to server");
             }


             }).catch((error) => {
              Platform.OS === 'android'
              ? ToastAndroid.show("Error", ToastAndroid.SHORT)
              : Alert.alert("Error")
             });


  }



    componentDidMount(){
      var result  = this.props.navigation.getParam('result')
      this.fetch(result["bid"])
      //ToastAndroid.show(result["bid"]+"...."+result["price"],ToastAndroid.LONG)
    // this.setState({

    //       price:`Price              :     ${this.state.currency}${result["price"]}`,
    //       price_next:result["price"],
    // })

      NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {

      }
      else{
        this.props.navigation.navigate("NoNetwork")
        return;
      }
     })


    }





  track(){

          var obj={}


          obj["quantity"] = this.state.quantity_next
          obj["price"] = parseFloat(this.state.price_single) + parseFloat(this.state.delivery_fee_single)
          obj["time"] = this.state.time.toString()
          obj["orderstatus"] = this.state.delivery_status
          obj["id"] = this.state.global
          obj["currency"] = this.state.currency

      this.props.navigation.navigate("TrackOrder",{result : obj})

  }

  render() {


                if (this.state.loading_status) {
                  return (
                    <ActivityIndicator
                      animating={true}
                      style={styles.indicator}
                      size="large"
                    />
                  );
                }



    return (
		<View style={styles.container}>
        <View style={{margin:10,borderWidth: 1,borderColor: 'black', borderRadius:3}}>
        <View style={{backgroundColor: "green"}}>
            <Text style={{ color: "white",alignItems:'center',marginBottom:7,fontSize:16,marginLeft:"5%"}}>{this.state.date}</Text>
        </View>



            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.quantity}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.price}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.deliveryfee}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.location}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.fueltype}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.vehicle}</Text>
            <Text style ={{color:'black',marginTop:7,marginBottom:7,marginLeft:"5%"}}>{this.state.carmodel}</Text>
            

            {
              this.state.services.length > 0 ?
              <Text style ={{color : 'green',fontFamily: 'Verdana',fontSize: 20,fontWeight: 'bold',margin:15,fontStyle: 'italic'}}> Services</Text>:
              null
            }

            <FlatList
              data={this.state.services}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) =>


              <View style={{ marginBottom:7,borderWidth: 1,borderColor: 'black', borderRadius:4,marginLeft:5,marginRight:5,marginTop:10}}>





                  <Text style={{fontFamily: 'Verdana',fontSize: 16,fontWeight: 'bold',marginLeft:'3%'}}>Service {item.id} : {item.service}</Text>




                <Text style={{fontFamily: 'Verdana',fontSize: 16,fontWeight: 'bold',marginLeft:'3%'}}>Sub-Service - {item.sub_service}</Text>


              </View>

              }
              keyExtractor={item => item.id}
            />

            <TouchableHighlight onPress={() => {
              this.track()}}>
            <Text style={{color : 'green',fontFamily: 'Verdana',fontSize: 20,fontWeight: 'bold',margin:15,textDecorationLine: 'underline',alignSelf:'center'}}>Track..</Text>
            </TouchableHighlight>


        </View>



      </View>
	  )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
      backgroundColor: "white"
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },

});
