
import React ,{ Component, PropTypes } from 'react';
import { Text, View, Button, StyleSheet,SafeAreaView, Dimensions, ScrollView, Image, FlatList, TouchableOpacity,ToastAndroid,AsyncStorage,Platform } from 'react-native';
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { createStackNavigator , createAppContainer ,createDrawerNavigator, DrawerItems, StackActions, NavigationActions} from 'react-navigation';


import Login from './Login';
import SplashScreen from './SplashScreen';
import Signup from './Signup';
import Main from './Main';
//import Home from './Home';
import HomeScreen from './HomeScreen';
import Location from './Location';
import LocationLPG from './LocationLPG';
import AddVehicleOne from './AddVehicleOne';
import AddVehicleTwo from './AddVehicleTwo';
import BookService from './BookService';
import ContactAdmin from './ContactAdmin';
import OrderHistory from './OrderHistory';
import TrackOrder from './TrackOrder';
import HistoryDetails from './HistoryDetails';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import GetProfile from './GetProfile';
import UpdateProfile from './UpdateProfile';
import TermsWebview from './TermsWebview';
import TermsCondition from './TermsCondition';
import HowWorks from './HowWorks';
import UpdatePicture from './UpdatePicture';
import NoNetwork from './NoNetwork';
import LPGSelection from './LPGSelection';
import UserVehicleAdd from './UserVehicleAdd';
import UserFuelAdd from './UserFuelAdd';
import GetVehicles from './GetVehicles';
import SelectCurrency from './SelectCurrency';
import SelectCurrencyLPG from './SelectCurrencyLPG';

import Peach from './Peach';

//renderitem in flatlist is used to provide view for particular list item
class SettingsScreen extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

      </View>
    );
  }
}

class Logout extends React.Component {

  constructor(props){
    super(props);
    this.removeItemValue()
  }

  removeItemValue = async () =>{
          await AsyncStorage.removeItem('uname', (err) => {
            // key 'key' will be removed, if they existed
            // callback to do some action after removal of item
          ToastAndroid.show("Successfully logged out", ToastAndroid.LONG);
          this.props.navigation.navigate("Login")
          });
  }

  clear(){

  }
  render() {
    return (
        null
    );
  }
}



//key is thhe 


// reset(){
//
//   const resetAction = StackActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'GetProfile' })],
// });
// this.props.navigation.dispatch(resetAction);
// this.props.navigation.navigate('ProfileScreen');
// }

//replace side menu with this to customize drawer ...custome compoonent in drawer is to customize drawer itesm
const CustomDrawerComponent = (props)=>(
  <SafeAreaView>
    <View style={{height:150, backgroundColor:'white', alignItems:'center', justifyContent:'center'}}>
      <Image source={require('./logo.png')} style={{height:120, width:120, borderRadius:60}} />
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView> )

  //customizing drawer items here
class SideMenu extends React.Component {
    static navigationOptions = {
      header: null ,
    };

      render() {
          return (
              <View>
                  <ScrollView>
                  <View style={{height:150, backgroundColor:'white', alignItems:'center', justifyContent:'center'}}>
                    <Image source={require('./logo.png')} style={{height:150, width:150, borderRadius:60}} />
                  </View>


                            <View style={styles.drawerborder}/>

          					  <TouchableOpacity onPress={() =>{
                        this.props.navigation.navigate('HomeScreen');
                        //below is used to reset stacki navigator so that goes infirst screen only
                        const resetAction = StackActions.reset({
                        index: 0,
                        key:'HomeScreen',
                        actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                      }}>
          					  <View style={styles.drawerlayout}>
          						<Text style={styles.drawertext}>Home</Text>
          						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
          					  </View>
          					  </TouchableOpacity>

                     <View style={styles.drawerborder}/>

          					  <TouchableOpacity onPress={() =>{
                                  this.props.navigation.navigate('ProfileScreen');
                                  //below is used to reset stacki navigator so that goes infirst screen only
                                  const resetAction = StackActions.reset({
                                  index: 0,
                                  key:'GetProfile',
                                  actions: [NavigationActions.navigate({ routeName: 'GetProfile' })],
                                });
                                this.props.navigation.dispatch(resetAction);

                      }}>
          					  <View style={styles.drawerlayout}>
          						<Text style={styles.drawertext}>Get Profile</Text>
          						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
          					  </View>
          					  </TouchableOpacity>



                      <View style={styles.drawerborder}/>

                          <TouchableOpacity onPress={() =>{
                                      this.props.navigation.navigate('UserVehicleAdd');
                                      const resetAction = StackActions.reset({
                                        index: 0,
                                        key:'UserVehicleAdd',
                                        actions: [NavigationActions.navigate({ routeName: 'UserVehicleAdd' })],
                                      });
                                      this.props.navigation.dispatch(resetAction);


                          }}>
                          <View style={styles.drawerlayout}>
                          <Text style={styles.drawertext}>Add Vehicle</Text>
                          <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                          </View>
                          </TouchableOpacity>


          					  <View style={styles.drawerborder}/>




          					  <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('MOrderScreen')
                            const resetAction = StackActions.reset({
                            index: 0,
                            key:'OrderHistory',
                            actions: [NavigationActions.navigate({ routeName: 'OrderHistory' })],
                          });
                          this.props.navigation.dispatch(resetAction);

                      }}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>My Orders</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                    					  </View>
          					   </TouchableOpacity>

          					 <View style={styles.drawerborder}/>
          					  <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('ContactAdmin')
                        const resetAction = StackActions.reset({
                        index: 0,
                        key:'ContactAdmin',
                        actions: [NavigationActions.navigate({ routeName: 'ContactAdmin' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                      }}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>Contact Admin</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                    					  </View>
          					   </TouchableOpacity>

          					    <View style={styles.drawerborder}/>
          					  <TouchableOpacity onPress={() => this.props.navigation.navigate('ResetPasswordScreen')}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>Reset Password</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                    					  </View>
          					   </TouchableOpacity>

          					    <View style={styles.drawerborder}/>
          					  <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('HowWorks')
                        const resetAction = StackActions.reset({
                        index: 0,
                        key:'HowWorks',
                        actions: [NavigationActions.navigate({ routeName: 'HowWorks' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                      }}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>How It Works?</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage}/>
                    					  </View>
          					   </TouchableOpacity>

          					    <View style={styles.drawerborder}/>
          					  <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('TermsCondition')
                        const resetAction = StackActions.reset({
                        index: 0,
                        key:'TermsCondition',
                        actions: [NavigationActions.navigate({ routeName: 'TermsCondition' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                      }}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>Terms & Condition</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                    					  </View>
          					   </TouchableOpacity>


          					  <View style={styles.drawerborder}/>
          					  <TouchableOpacity onPress={() => this.props.navigation.navigate('LogoutScreen')}>
                    					  <View style={styles.drawerlayout}>
                        						<Text style={styles.drawertext}>Logout</Text>
                        						 <Image source={require('./right-arrow.png')} style={styles.drawerimage} />
                    					  </View>
          					   </TouchableOpacity>


                                <View style={styles.drawerborder}/>
                  </ScrollView>
              </View>
          );
      }
  }

console.disableYellowBox = true;

const profile = createStackNavigator({
  GetProfile : {screen : GetProfile},
  UpdateProfile: {screen : UpdateProfile},
  UpdatePicture: {screen : UpdatePicture},
  NoNetwork : { screen : NoNetwork }


},{
  headerMode: 'none',
  initialRouteName:'GetProfile',
  navigationOptions:({navigation}) => ({
    header: null,
  }),
})


const customstack = createStackNavigator({
  HomeScreen : {screen : HomeScreen},
  Peach : {screen : Peach},
  Location : {screen : Location},
  GetVehicles : {screen : GetVehicles},
  LocationLPG : {screen : LocationLPG},
  LPGSelection : {screen : LPGSelection},
  SelectCurrency : {screen : SelectCurrency},
  SelectCurrencyLPG : {screen : SelectCurrencyLPG},
  AddVehicleOne : {screen : AddVehicleOne},
  AddVehicleTwo : {screen : AddVehicleTwo},
  BookService : {screen : BookService},
  NoNetwork : { screen : NoNetwork }

},{
  headerMode: 'none',
  initialRouteName:'HomeScreen',
  navigationOptions:({navigation}) => ({
    header: null,
  }),

})




const orderstack = createStackNavigator({
  OrderHistory : {screen : OrderHistory},
  HistoryDetails: {screen : HistoryDetails},
  TrackOrder: {screen : TrackOrder},
  NoNetwork : { screen : NoNetwork }

},{
  headerMode: 'none',

  navigationOptions:({navigation}) => ({
    header: null,
  }),

})


const vehiclestack = createStackNavigator({

  UserVehicleAdd: {screen : UserVehicleAdd},
  UserFuelAdd : {screen : UserFuelAdd},
  NoNetwork : { screen : NoNetwork }

},{
  headerMode: 'none',
  initialRouteName:'UserVehicleAdd',
  navigationOptions:({navigation}) => ({
    header: null,
  }),

})



const contactstack = createStackNavigator({

  ContactAdmin : {screen : ContactAdmin},
  

},{
  headerMode: 'none',
  initialRouteName:'ContactAdmin',
  navigationOptions:({navigation}) => ({
    header: null,
  }),

})

const howworksstack = createStackNavigator({

  HowWorks : {screen : HowWorks},
  

},{
  headerMode: 'none',
  initialRouteName:'HowWorks',
  navigationOptions:({navigation}) => ({
    header: null,
  }),

})

const termsstack = createStackNavigator({

  TermsCondition : {screen : TermsCondition},
  

},{
  headerMode: 'none',
  initialRouteName:'TermsCondition',
  navigationOptions:({navigation}) => ({
    header: null,
  }),

})



const drawerNavigator = createDrawerNavigator({
  HomeScreen: {
    screen: customstack,
    navigationOptions: {drawerLabel: () => null}
  },
  ProfileScreen:{
    screen: profile,
    navigationOptions: {drawerLabel: () => null}
  },
  MOrderScreen: {
    screen: orderstack,
    navigationOptions: {drawerLabel: () => null}
  },
  ContactAdmin : {screen : contactstack},

  ResetPasswordScreen: {
    screen: ResetPassword,
    navigationOptions: {drawerLabel: 'Reset Password',}
  },
  HowWorks: {
    screen: howworksstack,
    navigationOptions: {drawerLabel: 'How It Works',}
  },
  UserVehicleAdd: {
    screen: vehiclestack,
    navigationOptions: {drawerLabel: 'Add Vehicle',}
  },

  TermsCondition: {
    screen: termsstack,
    navigationOptions: {drawerLabel: 'Terms & Condition',}
  },

  LogoutScreen: {
    screen: Logout,
    navigationOptions: {drawerLabel: 'Logout',}
  },
  NoNetwork : { screen : NoNetwork }
},
  {
    initialRouteName: 'HomeScreen',
	   gesturesEnabled: true,
     style: {
      leftDrawerWidth: 40,
    },
    drawerPosition :"left",   contentComponent:SideMenu,
    contentOptions: {
        activeTintColor: '#e91e63',
      },
      navigationOptions: ({ navigation }) => ({


         headerTitle: (
           <View style={{flexDirection : 'row',flex :1,justifyContent:'center',marginLeft:Platform.OS  == 'android' ? -60 : -10}}>
          <Image style={{width: 70, height: 40,backgroundColor:'white'}}  source={require('./logo.png')} />
          </View>
          ),
           headerLeft:(<TouchableOpacity onPress={() =>navigation.toggleDrawer()}>
                         <Image style={{width: 25, height: 25,marginLeft:15}}  source={require('./menu.png')} />
                         </TouchableOpacity>),
           headerStyle: {
                        backgroundColor: '#008000'
                     }
           })
  },

  );




const RootStack = createStackNavigator({
  SplashScreen: { screen: SplashScreen },
  Login: { screen: Login },
  Signup: {screen: Signup},
  Main: {screen: Main},
  Terms:{screen: TermsWebview},
  NoNetwork:{screen:NoNetwork},
  ForgotPassword :{screen : ForgotPassword},
  Home: {screen: drawerNavigator,
     // navigationOptions: ({ navigation }) => ({
     //    headerTitle: (
     //     <Image style={{width: 70, height: 40,marginLeft:75,backgroundColor:'white'}}  source={require('./logo.png')} />
     //     ),
     //      headerLeft:(<TouchableOpacity onPress={() =>navigation.toggleDrawer()}>
     //                    <Image style={{width: 30, height: 30,marginLeft:15}}  source={require('./menu.png')} />
     //                    </TouchableOpacity>),
     //      headerStyle: {
     //                   backgroundColor: '#008000'
     //                }
     //      })

         },

}, {
  initialRouteName: 'SplashScreen',

}


)


const styles = StyleSheet.create({
drawerborder: {
borderBottomColor: '#008000',borderBottomWidth: 1
},
drawerlayout: {
flexDirection: 'row',
justifyContent: 'space-between',
alignItems: 'center',
},
drawertext:{
fontSize : 20,

color:'black',
marginTop:10,
marginBottom:10,
marginLeft:10
},
drawerimage:{
height:25,
width:15,
marginRight:10
},

});


const AppNavigator = createAppContainer(RootStack)
export default AppNavigator;
