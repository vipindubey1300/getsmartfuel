import React, {Component} from 'react';
import {Text, View, Image,Dimensions} from 'react-native';

export default class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    static navigationOptions = {
        header: null
    }
    componentWillMount(){
      this.image = (<Image source={require('./splash.jpg')} style={{height:Dimensions.get('window').height, width:Dimensions.get('window').width}} />)
    }
    componentDidMount() {
        setTimeout(() => {
            // go to Home page
            this.props.navigation.navigate('Main')
        }, 2000)
        //abhove should be 2500
    }

    render() {
        return (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>

            {this.image}

          </View>

        )
    }
}
