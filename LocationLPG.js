import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text, AsyncStorage,Platform, TouchableOpacity , Image,ScrollView,ToastAndroid,ActivityIndicator,NetInfo,BackHandler} from 'react-native';
import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';
import { PermissionsAndroid } from 'react-native';
//import Geocoder from 'react-native-geocoding';
import { Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { withNavigationFocus } from 'react-navigation';
import Geocoder from 'react-native-geocoder';
import DismissKeyboard from 'dismissKeyboard';
import { colors,urls } from './Constants';



 class LocationLPG extends Component {
  constructor(props) {
    super(props);
    this.state = {
    location: null,
    latitude: 0.00000,
    longitude: 0.000,
    address:'',
    global_address:'',
    google_search_status : false,
    loading_status:false,
    region: {
        latitude: 0.0,
        longitude: 0.0,
        latitudeDelta: 0.2,
        longitudeDelta: 0.9
      },
      coordinates:{
        latitude: 0.0,
        longitude: 0.0,
      },
      global_latitude:0.00,
      global_longitude:0.00


  };

  }
  //this is used to remove toolbar in given screen
  static navigationOptions = {
    header: null ,
  };


  signup(){

    var result  = this.props.navigation.getParam('result')

    result["location_name"] = this.state.address
    result["location_lat"] = this.state.latitude
    result["location_long"] = this.state.longitude

      //ToastAndroid.show("name  ..."+this.state.address+"LAtit..."+this.state.latitude, ToastAndroid.LONG);


    //this.props.navigation.navigate('LPGSelection',{result : result});

    this.props.navigation.navigate('SelectCurrencyLPG',{result : result});
  }

  //another way to get addr never used but
  getadd(){
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+ this.state.latitude + ',' + this.state.longitude + '&key=AIzaSyDg8PQMB0zL8QEiWboQ1v-h0vUr-fXgERE')
      .then((response) => response.json())
      .then((responseJson) => {
          //console.log('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
          //let city = responseJson.results[0].address_components[1].long_name

          //ToastAndroid.show("latitude "+this.state.latitude + "laong +" + this.state.longitude, ToastAndroid.LONG);
          Alert.alert(JSON.stringify(responseJson))
          //let state =  responseJson.results[0].address_components[2].long_name
          //Alert.alert(state)
        //  let country =  responseJson.results[0].address_components[3].long_name


        //  let add = city + state + country
        //  this.setState({address:add})


}).
catch((error) => {
console.error(error);
});

  }


//thiis wwas main..means in first update tak ye tha...

  getaddres(lat,long){
    Geocoder.init('AIzaSyDg8PQMB0zL8QEiWboQ1v-h0vUr-fXgERE');
    Geocoder.from(lat, long)
        .then(json => {
        	var addressComponent = json.results[0].address_components[1].long_name.toString()
          addressComponent = addressComponent + " " +json.results[0].address_components[2].long_name.toString()
          addressComponent = addressComponent + " " +json.results[0].address_components[3].long_name.toString()
          this.setState({address:addressComponent.toString(),loading_status:false})

        //  forceUpdate();
        })
        .catch(error =>{
          //ToastAndroid.show("Cannot get your current location !",ToastAndroid.SHORT);
          Platform.OS === 'android'
          ? ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT)
          : Alert.alert(JSON.stringify(error))
          this.setState({loading_status:false})
        });
  }


//this one current
  getaddress(lat,long){
    // Position Geocoding
              var NY = {
                lat:lat,
                lng: long
              };

              Geocoder.geocodePosition(NY).then(res => {
                //  ToastAndroid.show(JSON.stringify(res), ToastAndroid.LONG);
                var addr  = res[0].formattedAddress
                  this.setState({address: addr.toString(),loading_status:false,global_address:addr.toString()})


              })
              .catch(err =>{
                //ToastAndroid.show("Cannot get this location !",ToastAndroid.SHORT);

                Platform.OS === 'android'
                ? ToastAndroid.show("Cannot get this location !", ToastAndroid.SHORT)
                : Alert.alert("Cannot get this location !")


              this.setState({loading_status:false});
              })
  }



  async  requestLocationPermission(){
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Example App',
        'message': 'Example App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

      //alert("You can use the location");
      navigator.geolocation.getCurrentPosition(
        position => {
          this.setState({
            region: {
             latitude: position.coords.latitude,
             longitude: position.coords.longitude,
             latitudeDelta: 0.23,
             longitudeDelta: 0.5,
             accuracy: position.coords.accuracy
           },
           coordinates:{
             latitude: position.coords.latitude,
             longitude: position.coords.longitude
           },
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           global_latitude: position.coords.latitude,
           global_longitude: position.coords.longitude,


          });
          this.getaddress(position.coords.latitude, position.coords.longitude)
        },
        error => {
        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
          : Alert.alert("Check your internet speed connection")



          this.setState({loading_status:false})
        },
        { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
      );


    } else {
      console.log("location permission denied")
      //alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err)
  }
}


  async  requestLocationPermissionIOS(){
     navigator.geolocation.getCurrentPosition(
        position => {
          this.setState({
            region: {
             latitude: position.coords.latitude,
             longitude: position.coords.longitude,
             latitudeDelta: 0.23,
             longitudeDelta: 0.5,
             accuracy: position.coords.accuracy
           },
           coordinates:{
             latitude: position.coords.latitude,
             longitude: position.coords.longitude
           },
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           global_latitude: position.coords.latitude,
           global_longitude: position.coords.longitude,


          });
          this.getaddress(position.coords.latitude, position.coords.longitude)
        },
        error => {
        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
          : Alert.alert("Check your internet speed connection")



          this.setState({loading_status:false})
        },
        { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
      );
}



 async componentDidMount() {
     this.setState({loading_status:true})


     NetInfo.isConnected.fetch().then(isConnected => {
     if(!isConnected)
     {
       this.props.navigation.navigate("NoNetwork")
       return;
     }

    })

   Platform.OS === 'android'
          ?  await this.requestLocationPermission()
          :  await this.requestLocationPermissionIOS()


  }


  //
  // handleBackWithAlert = () => {
  // 	// const parent = this.props.navigation.dangerouslyGetParent();
  // 		// const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
  // 	// ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)
  //
  // 	if (this.props.isFocused) {
  //
  // 	if(this.state.google_search_status){
  // 		this.setState({google_search_status:false})
  // 	}
  //
  //
  //
  // return true;
  // }
  // }
  //
  //
  //
  // componentWillMount() {
  // BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
  //
  // }
  //
  // componentWillUnmount() {
  //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
  //
  // }
onRegionChange(region) {
 // ToastAndroid.show(JSON.stringify(region),ToastAndroid.LONG)
 let latitude = region.latitude
 let longitud = region.longitude
}



onMarkerDragEnd(lat,long){
//ToastAndroid.show(JSON.stringify(coord),ToastAndroid.SHORT)
  //ToastAndroid.show("COORd..."+lat+"INDEEEXXX///" +long,ToastAndroid.SHORT)
this.setState({latitude:lat,longitude:long})
this.getaddress(lat,long)
//this.getaddress()
}

getCurrentLocation = async () =>{
//  await this.requestLocationPermission()
this.setState({
  region: {
   latitude: this.state.global_latitude,
   longitude: this.state.global_longitude,
   latitudeDelta: 0.23,
   longitudeDelta: 0.5,

 },

})
}

  render() {



            if (this.state.loading_status) {
              return (
                <ActivityIndicator
                  animating={true}
                  style={styles.indicator}
                  size="large"
                />
              );
            }

            if (this.state.google_search_status) {
              return (
                <View style = {{margin:10,padding:20,flex:1}}>
                <GooglePlacesAutocomplete
                                  placeholder='Search'
                                  minLength={2} // minimum length of text to search
                                  autoFocus={false}
                                  returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                  listViewDisplayed='auto'    // true/false/undefined
                                  fetchDetails={true}
                                  renderDescription={row => row.description} // custom description render
                                  onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                 //  ToastAndroid.show(JSON.stringify(data),ToastAndroid.LONG)

                                    let city = details.address_components[0].long_name
                                    let lat = details.geometry.location.lat
                                    let lng = details.geometry.location.lng
                                    let full_addr= details.formatted_address
                                  //	this.setState({location:full_addr})

                                  this.setState({
                                    region: {
                                     latitude: lat,
                                     longitude: lng,
                                     latitudeDelta: 0.23,
                                     longitudeDelta: 0.5,

                                   },
                                   coordinates:{
                                     latitude: lat,
                                     longitude:lng
                                   },
                                   latitude: lat,
                                   longitude: lng,
                                   address:full_addr,
                                   google_search_status:false

                                  });

                                  //	ToastAndroid.show(city+"..."+lat+"..."+lng+".."+full_addr,ToastAndroid.LONG)
                                   // this.setState({google_search_status:false,lat:lat,long:lng,address:full_addr})
                                  }}



                                  query={{
                                    // available options: https://developers.google.com/places/web-service/autocomplete
                                    key: 'AIzaSyALI7BGuHD6Y2TDuvkZbcWac_HWZYzehfw',
                                    language: 'en', // language of the results
                                    types:['street_address,locality,sublocality','establishment','address '] // default: 'geocode'
                                  }}

                                  styles={{
                                    textInputContainer: {
                                      width: '100%'
                                    },
                                    description: {
                                      fontWeight: 'bold'
                                    }
                                  }}

                              //		currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                                //	currentLocationLabel="Current location"
                                  nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch


                                  GoogleReverseGeocodingQuery={{
                                     // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                   }}
                                   GooglePlacesSearchQuery={{
                                     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                     rankby: 'distance',
                                     types: 'food'
                                   }}

                                 filterReverseGeocodingByTypes={['street_address','political','sublocality', 'locality','administrative_area_level_2','administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities




                                  debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.

              />
                </View>
              );
            }


    return (
      <ScrollView>
		<View style={styles.container}>

        <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginTop:10}}>Please pin your Location</Text>

        <View style={{flexDirection:'row',marginBottom:15}}>
        <TextInput
          placeholder={'Enter Location'}
          style={styles.input}
           multiline={true}
           onFocus={() =>{
 					DismissKeyboard()
 					this.setState({google_search_status:true})

 				}}
          value ={`${this.state.address}`}
        />
{/**
        <TouchableOpacity onPress={this.getCurrentLocation }>
        <Image source={require('./mark.png')} style={{height:35, width:35,marginLeft:10}} />
        </TouchableOpacity>

        */}
        </View>



        <MapView
          provider = {PROVIDER_GOOGLE}
           style = {{height:350, width:"100%",marginBottom:20}}
           region={this.state.region}
           showsMyLocationButton={true}
           onRegionChange={this.onRegionChange}
           showsUserLocation = {true}
           followUserLocation = {true}
           zoomEnabled = {true}
    >
    <MapView.Marker
        draggable
        coordinate={this.state.coordinates}
        title={"Move it"}
        onDragEnd={(e) => {
          //ToastAndroid.show("COORd..."+e.nativeEvent.coordinate.latitude.toString()+"INDEEEXXX///" +e.nativeEvent.coordinate.longitude.toString(),ToastAndroid.SHORT)
        this.setState({latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude} )
        this.getaddress(e.nativeEvent.coordinate.latitude,e.nativeEvent.coordinate.longitude)


        }}
      />
    </MapView>


        <TouchableOpacity
                  style={styles.locationScreenButton}
                  onPress={this.signup.bind(this)}
                  underlayColor='#fff'>
                  <Text style={styles.locationText}>CONTINUE</Text>
         </TouchableOpacity>

      </View>
      </ScrollView>
	  )
  }
}
//<Image source={require('./maps.jpg')} style={{height:350, width:"85%",marginBottom:10,marginLeft:17,borderColor:'black',borderWidth:1}} />
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft:15,
    marginRight:15
  },
  input: {
    width: "85%",
    height: null,
    padding: 5,
    flexWrap:'wrap',
    textAlign: 'center',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'black',
    marginBottom: 20,
  },
  submit:{
  marginRight:40,
  marginLeft:40,
  marginTop:10,
  paddingTop:20,
  paddingBottom:20,
  backgroundColor:'#68a0cf',
  borderRadius:20,
  borderWidth: 1,
  borderColor: '#fff'
},
submitText:{
    color:'#fff',
    textAlign:'center',
},
locationScreenButton:{
  width:"100%",
  marginTop:10,
  paddingTop:10,
  paddingBottom:10,
  backgroundColor:'#FFC300',
  borderRadius:10,
  borderWidth: 1,
  borderColor: '#fff',
  marginBottom:10,
},
locationText:{
    color:'black',
    textAlign:'center',
    paddingLeft : 10,
    paddingRight : 10,
    fontSize :20,
    fontWeight : 'bold'
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
});

export default withNavigationFocus(LocationLPG);
