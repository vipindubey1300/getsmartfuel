import React from 'react';
import { StyleSheet,Text, View, Button, SafeAreaView, Platform,Dimensions, ScrollView, Image,NetInfo, FlatList, TouchableOpacity,Alert ,ToastAndroid,AsyncStorage,BackHandler,TouchableHighlight,ActivityIndicator,StatusBar} from 'react-native';
import {Icon} from 'native-base';
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { withNavigationFocus } from 'react-navigation';
import { Card } from 'react-native-elements'
import { colors,urls } from './Constants';
import PeachMobile from 'react-native-peach-mobile';











class HomeScreen extends React.Component {

  constructor(props){
    super(props);
    this._next = this._next.bind(this)

    this.state = {
      uid : '',
      loading_status:false,
      services:
        [
            {
                "key" :'',
                "name": "1",
                "icon": require('./home-icon1.png')
            },
            {
                "key" :'',
                "name": "2",
                "icon": require('./home-icon2.png')
            },
            {
                "key" :'',
                "name": "3",
                "icon": require('./home-icon3.png')
            },
            {
                "key" :'',
                "name": "4",
                "icon": require('./home-icon4.png')
            },

          ]
    };
  }


  




fetch(){

                      this.setState({loading_status:true})
                      let url = urls.base_url + 'api_get_all_services'
                      fetch(url, {
                      method: 'GET',

                    }).then((response) => response.json())
                          .then((responseJson) => {
                              this.setState({loading_status:false})
                          if(!responseJson.error){
                            var length = responseJson.result.length.toString();
                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            for(var i = 0 ; i < length ; i++){


                              const services = [...this.state.services];
                              services[i].name =  responseJson.result[i].Service.service_name
                              services[i].key = responseJson.result[i].Service.id
                              this.setState({ services : services });

                            //as stackoverflow
                            // const services = [...this.state.services];
                            // const serviceData = responseJson.result;
                            // serviceData.map((service, index) => { services[index][name] = service.Service.service_name; services[index][icon] = iconComponent; });
                            // this.setState({ services });


                            }
                             //Alert.alert(this.state.services.length.toString());
                          }
                          else{
                            Alert.alert("Cant Connect to server");
                          }


                          }).catch((error) => {
                            Platform.OS === 'android'
                            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                            : Alert.alert("Error")
                          });
}
componentWillMount(){
   StatusBar.setBackgroundColor('#32CD32')
                //  Alert.alert(this.state.services[2].name);
                //BackHandler.exitApp()

                NetInfo.isConnected.fetch().then(isConnected => {
                if(isConnected)
                {
                this.fetch()
                }
                else{
                  this.props.navigation.navigate("NoNetwork")
                  return;
                }
               })


}
handleBackButton = () => {
   if (this.props.isFocused) {
     BackHandler.exitApp();
     return true;
}
};

componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
}

componentWillUnmount() {
   BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}

  _renderDotIndicator() {
		return <PagerDotIndicator pageCount={3} />;
	}

  _next(){
  //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    let key = 1
      if(parseInt(key) == 1){
        let id = -1
        AsyncStorage.getItem('uname')
        .then((item) => {
           if (item) {
             id = item


            // this.setState({uid:item})
              this.nextscreen(item)
         }
         else {  }
        });
}




          // let obj = {
          //   "user_id" : id,
          //   "service_id":1,
          // }
          // this.props.navigation.navigate("Location",{result : obj})
}

_nextLPG(){
  //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

        let id = -1
        AsyncStorage.getItem('uname')
        .then((item) => {
           if (item) {
             id = item

          //   ToastAndroid.show("GOinfffffffffffff............."+item,ToastAndroid.LONG)


                    let obj = {
                        "user_id" : item,
                        //"service_id": 1,
                      }
        this.props.navigation.navigate("LocationLPG",{result : obj})
         }
         else {  }
        });

}

  nextscreen(userid){
  //  ToastAndroid.show("Your user id "+ userid, ToastAndroid.LONG);
    let obj = {
      "user_id" : userid,
      "service_id": 1,
    }
    this.props.navigation.navigate("Location",{result : obj})
  }

  setPeachMobileRef(ref) {
    this.peachMobile = ref;
}

  render() {

    if (this.state.loading_status) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }


    return (
                  <ScrollView style={{ flex: 1,backgroundColor:'#DCDCDC' ,height:'100%'}}>

<PeachMobile
        mode="test"
        urlScheme="com.getsmartfuel.com.payments"
       
        ref={(ref) => this.peachMobile = ref }
    />
      <View style={{ flex: 1,backgroundColor:'#DCDCDC' ,height:'100%'}}>
        {/*Example of Dot Indicator*/}
        <IndicatorViewPager
          style={{ height: 200 }}
          indicator={this._renderDotIndicator()}>
          {/*_renderDotIndicator() will return the PagerDotIndicator*/}
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>

            <Image source={require('./viewone.jpg')} style={{height:200, width:Dimensions.get('window').width,}} />

          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image source={require('./viewtwo.jpg')} style={{height:200, width:Dimensions.get('window').width,}} />
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image source={require('./viewthree.jpg')} style={{height:200, width:Dimensions.get('window').width, }} />
          </View>
        </IndicatorViewPager>

        <Text onPress={()=>this.props.navigation.navigate("Peach")}
        style={{ fontSize: 21, color: 'black',
        alignItems:'center',marginTop:30,marginBottom:20,
        fontWeight: 'bold',alignSelf:'center' }}>Services </Text>

        <Card
        style={{
          padding:9,
          elevation:10,
          width:'100%',
          margin:13
        }}>
        <TouchableOpacity onPress={this._next.bind(this)}>
          <View style={{flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center'}}>

             <Image source={require('./home-icon1.png')} style={{height:45, width:45}} />
             <Text style={{fontFamily: 'Verdana',fontSize: 20,marginLeft:20,marginRight:20,fontWeight: 'bold',color:'green'}}>Fuel Delivery</Text>
             <Image source={require('./right-arrow.png')} style={{height:35, width:25,marginLeft:10}} />


         </View>

        </TouchableOpacity>
        </Card>


        <Card
        style={{
          padding:9,
          elevation:10,
          width:'100%',
          margin:13
        }}>
        <TouchableOpacity onPress={this._nextLPG.bind(this)}>
          <View style={{flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center'}}>

             <Image source={require('./lpg.png')} style={{height:45, width:45}} />
             <Text style={{fontFamily: 'Verdana',fontSize: 20,marginLeft:20,marginRight:20,fontWeight: 'bold',color:'green'}}>LPG Delivery</Text>
             <Image source={require('./right-arrow.png')} style={{height:35, width:25,marginLeft:10}} />


         </View>

        </TouchableOpacity>
        </Card>

                  {/*  ios remove */}
                {/* 
        <Text style={{ fontSize: 21, color: 'black',alignItems:'center',marginTop:20,marginBottom:20,fontWeight: 'bold',marginLeft:10 }}>Additional Services </Text>
        <ScrollView
          horizontal={true}
		  showsHorizontalScrollIndicator={false}
          >
        <View style={{flexDirection: 'row' ,marginBottom:10,padding:10,justifyContent:'space-around'}}>

          <Card style={{margin:5,elevation: 3}}>

									<View style={{justifyContent:'center',alignItems:'center'}}>
										<Image style={{width:80,height:80}}  source={require('./home-icon2.png')} />
										<Text style={{fontWeight:'bold',fontSize:16,color:'black',marginLeft:25,marginRight:25}}>Oil Service</Text>
									</View>

						</Card>

            <Card style={{margin:5,elevation: 3}}>

                    <View style={{justifyContent:'center',alignItems:'center'}}>
                      <Image style={{width:80,height:80}}  source={require('./home-icon3.png')} />
                      <Text style={{fontWeight:'bold',fontSize:16,color:'black'}}>Cleaning Service</Text>
                    </View>

              </Card>


              <Card style={{margin:5,elevation: 3}}>

                      <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Image style={{width:80,height:80}}  source={require('./home-icon4.png')} />
                        <Text style={{fontWeight:'bold',fontSize:16,color:'black'}}>Tyre Pressure Check</Text>
                      </View>

                </Card>
        </View>
        </ScrollView>


*/}


        </View>
</ScrollView>
    );
  }
}



const styles = StyleSheet.create({

  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});

export default withNavigationFocus(HomeScreen );
