import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text, Platform,AsyncStorage,ScrollView ,ToastAndroid} from 'react-native';
import Timeline from 'react-native-timeline-listview';
import { Toast } from 'native-base';
import { colors,urls } from './Constants';



 const image = require('./checked.png');
  const title_image = (props) => <Image source={image} {...props} />;
export default class TrackOrder extends Component {

  constructor() {
      super();
       this.data = [
      {time: '09:00', title: 'Event 1', description: 'Event 1 Description'},
      {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
      {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
      {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
      {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
    ]
    this.state = {
      data: [],
      time:'',
      quantity:'',
      price:'',
      delivery_fee:0

    };


  }

componentDidMount(){
  var result  = this.props.navigation.getParam('result')
  var obj =[]
  var price = result["price"]
  var quantity = result["quantity"]
  var time = result["time"]
  var status = result["orderstatus"]
  var id = result["id"]
  var currency = result["currency"]
  //ToastAndroid.show("Modeel id!!! without empty...."+id, ToastAndroid.LONG);

  switch(parseInt(status)){

    case 0:
      obj=[{title: 'Order Placed', description:'      ',circleColor:'#006400',lineColor:'#050505'},
      {title: 'Ready to Dispatch', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'},
       {title: 'Order Dispatched', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'},
       {title: id === '10' ? 'LPG Delivered' : 'Fuel Delivered', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'}]

      this.setState({data:obj,time:`Delivery time         :   ${time}`,quantity:quantity,price:`Estimated Price    :   ${currency} ${price.toFixed(2)}`})
      break;
    case 1:
    obj=[{title: 'Order Placed', description:'      ',circleColor:'#006400',lineColor:'#050505'},
    {title: 'Ready to Dispatch', description:'     ',circleColor:'#006400',lineColor:'#050505'},
     {title: 'Order Dispatched', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'},
     {title: id === '10' ? 'LPG Delivered' : 'Fuel Delivered', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'}]

    this.setState({data:obj,time:`Delivery time         :   ${time}`,quantity:quantity,price:`Estimated Price     :   ${currency} ${price.toFixed(2)}`})
      break;
    case 2:
    obj=[{title: 'Order Placed', description:'      ',circleColor:'#006400',lineColor:'#050505'},
    {title: 'Ready to Dispatch', description:'     ',circleColor:'#006400',lineColor:'#050505'},
     {title: 'Order Dispatched', description:'     ',circleColor:'#006400',lineColor:'#050505'},
     {title: id === '10' ? 'LPG Delivered' : 'Fuel Delivered', description:'     ',circleColor:'#D6D6D6',lineColor:'#050505'}]

      this.setState({data:obj,time:`Delivery time         :   ${time}`,quantity:quantity,price:`Estimated Price     :    ${currency} ${price.toFixed(2)}`})
      break;
    case 3:
    obj=[{title: 'Order Placed', description:'      ',circleColor:'#006400',lineColor:'#050505'},
    {title: 'Ready to Dispatch', description:'     ',circleColor:'#006400',lineColor:'#050505'},
     {title: 'Order Dispatched', description:'     ',circleColor:'#006400',lineColor:'#050505'},
     {title:id === '10' ? 'LPG Delivered' : 'Fuel Delivered', description:'     ',circleColor:'#006400',lineColor:'#050505'}]
      this.setState({data:obj,time:`Delivery time         :   ${time}`,quantity:quantity,price:`Estimated Price     :    ${currency} ${price.toFixed(2)}`})
      break;
  }

}

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
      <View style={{ borderWidth:1 ,borderRadius:1, padding : 10 ,marginTop:15,marginLeft:8,marginRight:8,marginBottom:"15%"}}>
      <Text style={{ color: 'black', fontSize: 20 }}>{this.state.time}</Text>
      <Text style={{ color: 'black', fontSize: 20 }}>{this.state.quantity}</Text>
      <Text style={{ color: 'black', fontSize: 20 }}>{this.state.price}</Text>
      </View>

      <Text style={{ color: 'green', fontSize: 20,marginBottom:"12%",marginLeft:'3%' }}>Your order Status</Text>

{/*      <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
        <Text>Delivey title</Text>
        <Text>:<Text>
        <Text>{this.state.title}</Text>
      <View>
      <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
        <Text>Quantity</Text>
        <Text>:<Text>
        <Text>`${this.state.quantity}Litres`</Text>
      <View>
      <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
        <Text>Estimated Price</Text>
        <Text>:<Text>
        <Text>`$${this.state.price}`</Text>
        <View>   */}
              



                <View style={{width:'100%',height:300,borderRadius:20,borderWidth:1,borderColor:'white'}}>
      <Timeline
            //circleSize={20}
            //circleColor='#006400'

            //columnFormat='single-column-right'
            showTime={false}
            data={this.state.data}

                        />
                    </View>


    </View>
    </ScrollView>
    );
  }

  onPageChange(position){
    this.setState({currentPosition: position});
}

  }


const styles = StyleSheet.create({
  container: {
    flex: 1,
      backgroundColor: "white",
      padding:"5%"
  },
  stepIndicator: {
    marginVertical:50,
    paddingHorizontal:20
  },
  rowItem: {
    flex:3,
    paddingVertical:20
  },
  title: {
    flex: 1,
    fontSize:20,
    color:'#333333',
    paddingVertical:16,
    fontWeight:'600'
  },
  body: {
    flex: 1,
    fontSize:15,
    color:'#606060',
    lineHeight:24,
    marginRight:8
  }
});
