import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text, TouchableOpacity,Image, ScrollView,Alert,ToastAndroid, AsyncStorage ,ActivityIndicator,NetInfo,Platform} from 'react-native';
//import { CheckBox } from 'react-native-elements';
import { colors,urls } from './Constants';
import FormData from 'FormData';
import ImagePicker from "react-native-image-picker";
import RNFetchBlob from 'rn-fetch-blob';



export default class GetProfile extends React.Component {

  constructor(props){
    super(props);

    this.state = {

      name: '',
      location:'',
      email:'',
      mobile:'',
      image_error:false,
      error:null,
      image_url:'',
      loading_status:false,
      ImageSource: null,
      data: null,
      user_name:'',
      first_name:'',
      last_name:'',

    };
  }



fetch(){
  this.setState({loading_status:true})
AsyncStorage.getItem('uname')
.then((item) => {
   if (item) {
//Alert.alert(item)

let url = urls.base_url + 'api_getprofile?id='+item
     fetch(url, {
     method: 'GET',
     headers: {
       'Accept': 'application/json',
       'Content-Type':  'multipart/form-data',
     },


   }).then((response) => response.json())
         .then((responseJson) => {

           this.setState({loading_status:false})
         if(!responseJson.error){

               //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

               //chechikng if present ot not image
               if(responseJson.result.User.user_pic.length === 0){
                 this.setState({
                   first_name:responseJson.result.User.name,
                   last_name:responseJson.result.User.last_name,
                   user_name:responseJson.result.User.username,
                   email:responseJson.result.User.email,
                   mobile:responseJson.result.User.mobile,
                   image_error:true

                 })
               }
               else{
                 this.setState({
                  first_name:responseJson.result.User.name,
                  last_name:responseJson.result.User.last_name,
                   //username:responseJson.result.User.location,
                   user_name:responseJson.result.User.username,
                   email:responseJson.result.User.email,
                   mobile:responseJson.result.User.mobile,
                   image_error:false,
                   image_url : responseJson.result.User.user_pic

                 })

               }


       }else{
                 //errror in insering data

                 Platform.OS === 'android'
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                  // this.setState({ error: responseJson.message.toString() });
         }


         }).catch((error) => {
          Platform.OS === 'android'
          ? ToastAndroid.show("Error", ToastAndroid.SHORT)
          : Alert.alert("Error")
         });
 }
 else {  }
});


}

selectPhotoTapped() {
   const options = {
     maxWidth: 500,
     maxHeight: 500,
     storageOptions: {
       skipBackup: true
     }
   };

   ImagePicker.showImagePicker(options, (response) => {
     console.log('Response = ', response);

     if (response.didCancel) {
       console.log('User cancelled photo picker');
     }
     else if (response.error) {
       console.log('ImagePicker Error: ', response.error);
     }
     else if (response.customButton) {
       console.log('User tapped custom button: ', response.customButton);
     }
     else {
      //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
       let source = { uri: response.uri};
       this.setState({
         ImageSource: source ,
         data: response.data,

       },this.upload());
     }
   });
}



upload = () =>{
this.setState({loading_status:true})
 AsyncStorage.getItem('uname')
 .then((item) => {
    if (item) {
      NetInfo.isConnected.fetch().then(isConnected => {
             if(!isConnected)
             {
               this.props.navigation.navigate("NoNetwork")
               return false;
             }
       else{

         //fetching all fata from server
         var formData = new FormData();
     formData.append('id', item);
     formData.append('image',JSON.stringify(this.state.data));


     let url = urls.base_url + 'api_updateprofilepic'
     fetch(url, {
     method: 'POST',
     headers: {
       'Accept': 'application/json',
       'Content-Type': 'multipart/form-data',
     },
     body: formData

   }).then((response) => response.json())
         .then((responseJson) => {
             this.setState({loading_status:false})
       //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                                           if(!responseJson.error){
                                           //  ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.LONG);

                                             Platform.OS === 'android' 
                                             ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                                             : Alert.alert(JSON.stringify(responseJson.message))
                  



                                           }
                                           else{
                                            // ToastAndroid.show('failed', ToastAndroid.LONG);

                                             Platform.OS === 'android' 
                                             ? ToastAndroid.show('failed', ToastAndroid.SHORT)
                                             : Alert.alert('failed')
                  


                                           }

         }).catch((error) => {
             this.setState({loading_status:false})
             Platform.OS === 'android'
             ? ToastAndroid.show("Error", ToastAndroid.SHORT)
             : Alert.alert("Error")
         });


       }

            })


  }
  else {  }
 });

}

  componentDidMount(){

    NetInfo.isConnected.fetch().then(isConnected => {
    if(isConnected)
    {
    this.fetch()
    }
    else{
      this.props.navigation.navigate("NoNetwork")
      return;
    }
   })

  }

  update(){
    this.props.navigation.navigate("UpdateProfile")
  }
  updatePic(){
      this.props.navigation.navigate("UpdatePicture")
  }



   validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

  check(){

    if(this.state.ImageSource === null && this.state.image_error){
      return (<Image source={require('./profile.jpg')} style={styles.image} />)
    }
    else if(this.state.ImageSource === null && !this.state.image_error){
      return (<Image source={{uri: this.state.image_url}} style={styles.image} />)
    }
    else{
        return (<Image source={this.state.ImageSource} style={styles.image} />)
    }


  }

  updateProfile = async () =>{


    if(this.validateEmail(this.state.email) == false){
     // ToastAndroid.show("Enter Valid Email",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show("Enter Valid Email", ToastAndroid.SHORT)
      : Alert.alert("Enter Valid Email")



      return false;
    }

    if(this.state.first_name.trim().length == 0){
    //  ToastAndroid.show("Enter First Name",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show("Enter First Name", ToastAndroid.SHORT)
      : Alert.alert("Enter First Name")



      return false;
    }


    if(this.state.last_name.trim().length == 0){
    //  ToastAndroid.show("Enter Last Name",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show("Enter Last Name", ToastAndroid.SHORT)
      : Alert.alert("Enter Last Name")



      return false;
    }

    if(this.state.mobile.trim().length == 0){
    //  ToastAndroid.show("Enter Contact !",ToastAndroid.SHORT)

      Platform.OS === 'android' 
      ? ToastAndroid.show("Enter Contact !", ToastAndroid.SHORT)
      : Alert.alert("Enter Contact !")



      return false;
    }


    var isnum = /^\d+$/.test(this.state.mobile);
    if(!isnum){
      //ToastAndroid.show("Please Enter Valid Contact Number", ToastAndroid.SHORT);

      Platform.OS === 'android' 
      ? ToastAndroid.show("Please Enter Valid Contact Number", ToastAndroid.SHORT)
      : Alert.alert("Please Enter Valid Contact Number")



      return false;
    }

      this.setState({loading_status:true})
          var formData = new FormData();

          await AsyncStorage.getItem('uname')
          .then((item) => {
             if (item) {
              // formData.append('id', item);
                this.setState({id:parseInt(item)})

           }
           else {  }
          });


          formData.append('id', this.state.id);
          formData.append('first_name', this.state.first_name);
          formData.append('last_name', this.state.last_name);
          formData.append('email', this.state.email);
          formData.append('mobile', this.state.mobile);
        //  ToastAndroid.show("ID;;"+this.state.id,ToastAndroid.SHORT)


        NetInfo.isConnected.fetch().then(isConnected => {
     if(!isConnected)
     {
       this.props.navigation.navigate("NoNetwork")
       return false;
     }
else{

 //fetching all fata from server
 let url = urls.base_url + 'api_updateprofile'
 fetch(url, {
 method: 'POST',
 headers: {
   'Accept': 'application/json',
   'Content-Type':  'multipart/form-data',
 },
 body: formData

}).then((response) => response.json())
     .then((responseJson) => {
     this.setState({loading_status:false})
     if(!responseJson.error){

             //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

             Platform.OS === 'android' 
             ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
             : Alert.alert(JSON.stringify(responseJson.message))




   }else{

    // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

     Platform.OS === 'android' 
     ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
     : Alert.alert(JSON.stringify(responseJson.message))

     //  this.setState({ error: responseJson.message.toString() });
     }


     }).catch((error) => {
      Platform.OS === 'android'
      ? ToastAndroid.show("Error", ToastAndroid.SHORT)
      : Alert.alert("Error")
     });


}

    })



  }


  render() {

        let result = this.check()


        if (this.state.loading_status) {
          return (
            <ActivityIndicator
              animating={true}
              style={styles.indicator}
              size="large"
            />
          );
        }


    return (
        <ScrollView>
      <View style={styles.container}>
      <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
        {result}
        </TouchableOpacity>



      <Text style ={{color:'black',fontSize :20,fontWeight :'bold',alignItems:'center',marginBottom:50}}>{this.state.name}</Text>

      <Text style={{width:'100%',marginBottom:10}}>First Name:</Text>
      <TextInput
        value={this.state.first_name}
        onChangeText={(first_name) => this.setState({ first_name })}
        placeholder={'First Name'}
        style={styles.input}
        placeholderTextColor={'black'}
      />


      <Text style={{width:'100%',marginBottom:10}}>Last Name:</Text>
      <TextInput
        value={this.state.last_name}
        onChangeText={(last_name) => this.setState({ last_name })}
        placeholder={'Last Name'}
        style={styles.input}
        placeholderTextColor={'black'}
      />

      <Text style={{width:'100%',marginBottom:10}}>Username:</Text>
      <TextInput
        value={this.state.user_name}
        editable={false}
        onChangeText={(user_name) => this.setState({ user_name })}
        placeholder={'Username'}
        style={styles.input}
        placeholderTextColor={'black'}
      />

      <Text style={{width:'100%',marginBottom:10}}>Email:</Text>
      <TextInput
        value={this.state.email}
        onChangeText={(email) => this.setState({ email })}
        placeholder={'Email'}
         editable={false}
        style={styles.input}
        placeholderTextColor={'black'}
      />

      <Text style={{width:'100%',marginBottom:10}}>Mobile:</Text>
      <TextInput
        value={this.state.mobile === null ? " ":this.state.mobile}
        onChangeText={(mobile) => this.setState({ mobile })}
        placeholder={'Contact'}
        style={styles.input}
        keyboardType = 'numeric'
        textContentType='telephoneNumber'
        placeholderTextColor={'black'}
      />






<TouchableOpacity
                 style={styles.locationScreenButton}
                 onPress={this.updateProfile.bind(this)}
                 underlayColor='#fff'>
                 <Text style={styles.locationText}>UPDATE</Text>
        </TouchableOpacity>
      </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginLeft:15,
    marginRight:15,
    margin:25,
    alignItems:'center',
    justifyContent:'center'
  },
  image: {
      width: 120,
      height: 120,
      borderRadius: 120 / 2,
      overflow: "hidden",
      borderWidth: 3,
      borderColor: "#008000",
      marginBottom:15,
      alignItems:'center',
    },
    locationScreenButton:{
      width: "100%",
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      alignItems:'center',
      justifyContent:'flex-end',
      backgroundColor:'#FFC300',
      borderRadius:2,
      borderWidth: 1,
      borderColor: '#fff'
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    text:{
      color:'black',
      fontSize :18,
      fontWeight : 'bold',
      marginBottom:20,
      width:'100%'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    },
    input: {
      width: "100%",
      height: 44,
      textAlign: 'center',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
    }
});
