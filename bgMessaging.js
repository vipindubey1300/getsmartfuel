import React from 'react';
import { StyleSheet, Text, View ,Button, AsyncStorage,TouchableOpacity,Platform,ToastAndroid,ActivityIndicator,StatusBar,NetInfo,Alert} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
//import { Dropdown } from 'react-native-material-dropdown';
import FormData from 'FormData';
import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";

// @flow
import firebase from 'react-native-firebase';
// Optional flow type
import type { RemoteMessage } from 'react-native-firebase';

export default async (notification: RemoteMessage) => {
    // handle your message
 //  ToastAndroid.show(JSON.stringify(notification),ToastAndroid.LONG)
    if (Platform.OS === 'android') {
        const localNotification = new firebase.notifications.Notification({
            sound: 'default',
            show_in_foreground: true,
        }).setNotificationId(notification.from)
        .setTitle(notification.data.title)
        .setSubtitle(notification.subtitle)
        .setBody(notification.data.content)
        .setData(notification.data)
            .android.setChannelId('notification_channel_name') // e.g. the id you chose above
            .android.setSmallIcon('logo') // create this icon in Android Studio
            .android.setColor('#D3D3D3') // you can set a color here
            .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => ToastAndroid.show(err.message,ToastAndroid.LONG));

    }
    else if (Platform.OS === 'ios') {
      console.log(notification);
      const localNotification = new firebase.notifications.Notification()
          .setNotificationId(notification._from)
          .setTitle(notification._data.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
          .ios.setBadge(notification.ios.badge);

      firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));

  }
    return Promise.resolve();
}



displayNotification = (notification) => {
    //ToastAndroid.show("notification inintalss....",ToastAndroid.LONG)




      if (Platform.OS === 'android') {
          const localNotification = new firebase.notifications.Notification({
              sound: 'default',
              show_in_foreground: true,
          }).setNotificationId(notification._from)
          .setTitle(notification._data.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
              .android.setChannelId('notification_channel_name') // e.g. the id you chose above
              .android.setSmallIcon('logo') // create this icon in Android Studio
              .android.setColor('#D3D3D3') // you can set a color here
              .android.setPriority(firebase.notifications.Android.Priority.High);

          firebase.notifications()
              .displayNotification(localNotification)
              .catch(err => ToastAndroid.show("err.message",ToastAndroid.LONG));

      }
      else if (Platform.OS === 'ios') {
        console.log(notification);
        const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification._from)
            .setTitle(notification._data.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification._data.content)
            .setData(notification.data)
            .ios.setBadge(notification.ios.badge);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));

    }
  }
