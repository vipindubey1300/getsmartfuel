import React from 'react';
import { StyleSheet, Text, View , TextInput,Button, TouchableOpacity,ScrollView, Platform, Image,ToastAndroid,Alert,ActivityIndicator,NetInfo} from 'react-native';
import { colors,urls } from './Constants';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

import FormData from 'FormData';
import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import DateTimePicker from 'react-native-modal-datetime-picker';
import PayPal from 'react-native-paypal-wrapper';
import firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';

export default class BookService extends React.Component{

    constructor(){
        super();
      this.state = {date:"",
                    date_obj:"",
                    selectedHours: 0,
                    selectedMinutes: 0,
                    selected:0,
                    isDateTimePickerVisible: false,
                    time:"",
                    fueltype:"",
                    total:'',
                    loading_status:false,
                    quantity:1.0,
                    isDatePickerVisible: false,
                    cash_status:'cash',
                    quantity_textinput:1,
                    price_prev:0,
                    total_amt:0.0,
                    got_price:0.0,
                    global_price:0.0,
                    promo_code:0,
                    promo_code_text:'',
                    promo:'',
                    discount_amount:0,
                    net_amount:0,
                    delivery_fee:0,
                    currency:'',

                    //extra Services

                    service_array:[],
                    sub_service_array:[],
                    price_array:[],
                    services:[],
                    sub_services_one:[],
                    sub_services_two:[],
                    sub_services_three:[],
                    sub_services_four:[],
                    selected_service_one:'key0',
                    selected_service_two:'key0',
                    selected_service_three:'key0',
                    selected_service_four:'key0',
                    selected_sub_service_two:'key0',
                    selected_sub_service_one:'key0',
                    selected_sub_service_three:'key0',
                    selected_sub_service_four:'key0',
                    service_one_text:true,
                    service_two_text:false,
                    service_three_text:false,
                    service_four_text:false,
                    service_one_picker:false,
                    service_two_picker:false,
                    service_three_picker:false,
                    service_four_picker:false,



                  };
    }



      getDelivery(){
        let url = urls.base_url + 'api_get_delivery_fee'
                                fetch(url, {
                                method: 'GET',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'multipart/form-data',
                                },


                              }).then((response) => response.json())
                                    .then((responseJson) => {
                                      this.setState({loading_status:false})

                                        if(!responseJson.error){

                                            var fee = responseJson.result.DeliveryFee.amount
                                            var fee_africa = responseJson.result.DeliveryFee.amount_in_africa
                                            this.setState({
                                                delivery_fee :  this.state.currency == 'USD' ? fee : fee_africa
                                            })
                                        }


                                  }
                                    ).catch((error) => {
                                      Platform.OS === 'android'
                                      ? ToastAndroid.show("Error !", ToastAndroid.SHORT)
                                      : Alert.alert("Error !")

                                    });



  }


    fetch(){

      this.setState({loading_status:true})
      let url = urls.base_url + 'api_get_all_services_new'
      fetch(url, {
      method: 'GET',

    }).then((response) => response.json())
          .then((responseJson) => {
              this.setState({loading_status:false})
          if(!responseJson.error){
            var length = responseJson.result.length.toString();

          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
            for(var i = 0 ; i < length ; i++){
                var id = responseJson.result[i].Service.id


                  const array = [...this.state.services];
                  array[i] = { ...array[i], name: responseJson.result[i].Service.service_name };
                  array[i] = { ...array[i], key: responseJson.result[i].Service.id };
                  this.setState({ services : array });



            //as stackoverflow
            // const services = [...this.state.services];
            // const serviceData = responseJson.result;
            // serviceData.map((service, index) => { services[index][name] = service.Service.service_name; services[index][icon] = iconComponent; });
            // this.setState({ services });


            }
             //Alert.alert(this.state.services.length.toString());
          //   ToastAndroid.show(JSON.stringify(this.state.services),ToastAndroid.LONG)
          }
          else{
            Alert.alert("Cant Connect to server");
          }


          }).catch((error) => {
            Platform.OS === 'android'
            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
            : Alert.alert("Error")
          });
}

getSubServicesOne(serviceId){
      let arr=[];
      //ToastAndroid.show(makeId, ToastAndroid.LONG);
      NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected)
      {
        this.props.navigation.navigate("NoNetwork")
        return;
      }
      else{

         var formData = new FormData();

         formData.append('service_id', serviceId);


            this.setState({loading_status:true,sub_services_one:[]})
                 let url = urls.base_url + 'api_get_sub_services'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                    this.setState({loading_status:false})
                    if(!responseJson.error){
                      var length = responseJson.result.length
                      if(length >  0){
                        for(var i = 0 ; i < length ; i++){
                        var id = responseJson.result[i].SubServices.id.toString()
                        var name = responseJson.result[i].SubServices.sub_service_name.toString()
                        var price = responseJson.result[i].SubServices.price
                          var price_africa = responseJson.result[i].SubServices.price_africa
                              const array = [...this.state.sub_services_one];
                              array[i] = { ...array[i], id: id };
                              array[i] = { ...array[i], name : name };
                              if(this.state.currency == 'USD'){
                                array[i] = { ...array[i], price : price };
                              }
                              else{
                                array[i] = { ...array[i], price : price_africa };
                              }

                              this.setState({ sub_services_one : array });
                             // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                        }
                          // ToastAndroid.show(JSON.stringify("this.state."+stateValue),ToastAndroid.LONG)
                      }

                      else{
                        let arr=[];
                        let array=[];
                      this.setState({sub_services_one:arr})

                      }
                    }





                    }).catch((error) => {
                      Platform.OS === 'android'
                      ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                      : Alert.alert("Error")
                    });
      }

     })



   }



   getSubServicesTwo(serviceId){
         //ToastAndroid.show(makeId, ToastAndroid.LONG);
         NetInfo.isConnected.fetch().then(isConnected => {
         if(!isConnected)
         {
           this.props.navigation.navigate("NoNetwork")
           return;
         }
         else{
             this.setState({loading_status:true})
            var formData = new FormData();

            formData.append('service_id', serviceId);


            let url = urls.base_url + 'api_get_sub_services'
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data',
                   },
                   body: formData

                 }).then((response) => response.json())
                       .then((responseJson) => {
                      this.setState({loading_status:false,sub_services_two:[]})
                       if(!responseJson.error){
                         var length = responseJson.result.length
                         if(length >  0){
                           for(var i = 0 ; i < length ; i++){
                           var id = responseJson.result[i].SubServices.id.toString()
                           var name = responseJson.result[i].SubServices.sub_service_name.toString()
                             var price = responseJson.result[i].SubServices.price
                             var price_africa = responseJson.result[i].SubServices.price_africa


                                 const array = [...this.state.sub_services_two];
                                 array[i] = { ...array[i], id: id };
                                 array[i] = { ...array[i], name : name };
                                 if(this.state.currency == 'USD'){
                                   array[i] = { ...array[i], price : price };
                                 }
                                 else{
                                   array[i] = { ...array[i], price : price_africa };
                                 }
                                 this.setState({ sub_services_two : array });
                                // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                           }
                             // ToastAndroid.show(JSON.stringify("this.state."+stateValue),ToastAndroid.LONG)
                         }

                         else{
                           let arr=[];
                           let array=[];
                         this.setState({sub_services_two:arr})

                         }
                       }





                       }).catch((error) => {
                        Platform.OS === 'android'
                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                        : Alert.alert("Error")
                       });
         }

        })



      }



      getSubServicesThree(serviceId){
            //ToastAndroid.show(makeId, ToastAndroid.LONG);
            NetInfo.isConnected.fetch().then(isConnected => {
            if(!isConnected)
            {
              this.props.navigation.navigate("NoNetwork")
              return;
            }
            else{

               var formData = new FormData();

               formData.append('service_id', serviceId);


                 this.setState({loading_status:true})
                 let url = urls.base_url + 'api_get_sub_services'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                        this.setState({loading_status:false,sub_services_three:[]})
                          if(!responseJson.error){
                            var length = responseJson.result.length
                            if(length >  0){
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].SubServices.id.toString()
                              var name = responseJson.result[i].SubServices.sub_service_name.toString()
                                var price = responseJson.result[i].SubServices.price
                                var price_africa = responseJson.result[i].SubServices.price_africa


                                    const array = [...this.state.sub_services_three];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name : name };
                                    if(this.state.currency == 'USD'){
                                      array[i] = { ...array[i], price : price };
                                    }
                                    else{
                                      array[i] = { ...array[i], price : price_africa };
                                    }
                                    this.setState({ sub_services_three : array });
                                   // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                              }
                                // ToastAndroid.show(JSON.stringify("this.state."+stateValue),ToastAndroid.LONG)
                            }

                            else{
                              let arr=[];
                              let array=[];

                            this.setState({sub_services_three:arr})

                            }
                          }





                          }).catch((error) => {
                            Platform.OS === 'android'
                            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                            : Alert.alert("Error")
                          });
            }

           })



         }


               getSubServicesFour(serviceId){
                     //ToastAndroid.show(makeId, ToastAndroid.LONG);
                     NetInfo.isConnected.fetch().then(isConnected => {
                     if(!isConnected)
                     {
                       this.props.navigation.navigate("NoNetwork")
                       return;
                     }
                     else{

                        var formData = new FormData();
                          this.setState({loading_status:true})
                        formData.append('service_id', serviceId);


                        let url = urls.base_url + 'api_get_sub_services'
                               fetch(url, {
                               method: 'POST',
                               headers: {
                                 'Accept': 'application/json',
                                 'Content-Type': 'multipart/form-data',
                               },
                               body: formData

                             }).then((response) => response.json())
                                   .then((responseJson) => {
                                    this.setState({loading_status:false,sub_services_four:[]})
                                   if(!responseJson.error){
                                     var length = responseJson.result.length
                                     if(length >  0){
                                       for(var i = 0 ; i < length ; i++){
                                       var id = responseJson.result[i].SubServices.id.toString()
                                       var name = responseJson.result[i].SubServices.sub_service_name.toString()
                                       var price = responseJson.result[i].SubServices.price
                                       var price_africa = responseJson.result[i].SubServices.price_africa


                                             const array = [...this.state.sub_services_four];
                                             array[i] = { ...array[i], id: id };
                                             array[i] = { ...array[i], name : name };
                                             if(this.state.currency == 'USD'){
                                               array[i] = { ...array[i], price : price };
                                             }
                                             else{
                                               array[i] = { ...array[i], price : price_africa };
                                             }
                                             this.setState({ sub_services_four : array });
                                            // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                                       }
                                         // ToastAndroid.show(JSON.stringify("this.state."+stateValue),ToastAndroid.LONG)
                                     }

                                     else{
                                       let arr=[];
                                       let array=[];
                                     this.setState({sub_services_four:arr})

                                     }
                                   }





                                   }).catch((error) => {
                                    Platform.OS === 'android'
                                    ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                    : Alert.alert("Error")
                                   });
                     }

                    })



                  }


                  showAlert(title, body) {
                    Alert.alert(
                      title, body,
                      [
                          { text: 'OK', onPress: () => console.log('OK Pressed') },
                      ],
                      { cancelable: false },
                    );
                  }




////////////////////// Add these methods //////////////////////

  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
       // this.showAlert(title, body);
       // ToastAndroid.show("Notfictatoin recieved......"+JSON.stringify(notification),ToastAndroid.LONG)
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        //const { title, body } = notificationOpen.notification;
       // this.showAlert(title, body);
      // ToastAndroid.show("Notfictatoin openingggggg......"+JSON.stringify(notification),ToastAndroid.LONG)
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
       // const { title, body } = notificationOpen.notification;
        //this.showAlert(title, body);
      //  ToastAndroid.show("notification inintalss....",ToastAndroid.LONG)
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      // console.log(JSON.stringify(message));
      //ToastAndroid.show(JSON.stringify(message),ToastAndroid.LONG)
     // this.displayNotification(message)
    });
  }
  displayNotification = (notification) => {
      if (Platform.OS === 'android') {
          const localNotification = new firebase.notifications.Notification({
              sound: 'default',
              show_in_foreground: true,
          }).setNotificationId(notification._from)
          .setTitle(notification._data.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
              .android.setChannelId('notification_channel_name') // e.g. the id you chose above
              .android.setSmallIcon('logo') // create this icon in Android Studio
              .android.setColor('#D3D3D3') // you can set a color here
              .android.setPriority(firebase.notifications.Android.Priority.High);

          firebase.notifications()
              .displayNotification(localNotification)
              .catch(err => console.error(err));

      }
      else if (Platform.OS === 'ios') {
        console.log(notification);
        const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification._from)
            .setTitle(notification._data.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification._data.content)
            .setData(notification.data)
            .ios.setBadge(notification.ios.badge);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));

    }
  }



  subscribeToNotificationListeners() {
        const channel = new firebase.notifications.Android.Channel(
            'notification_channel_name', // To be Replaced as per use
            'Notifications', // To be Replaced as per use
            firebase.notifications.Android.Importance.Max
        ).setDescription('A Channel To manage the notifications related to Application');
        firebase.notifications().android.createChannel(channel);

        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('onNotification notification-->', notification);
            console.log('onNotification notification.data -->', notification.data);
            console.log('onNotification notification.notification -->', notification.notification);
            // Process your notification as required
            this.displayNotification(notification)
        });
    }



    componentDidMount(){

      firebase.messaging().hasPermission().then(hasPermission => {
        if (hasPermission) {
            //App already has permission to recieve notifications we just need to add recivers for the notifications
            //this.subscribeToNotificationListeners()
        } else {
            //App does not have permission to recieve notifications we first need to request for permission and then add recivers for the notifications
            firebase.messaging().requestPermission().then(() => {
               // this.subscribeToNotificationListeners()
            }).catch(error => {
                console.error(error);

            })
        }
    })




      this.createNotificationListeners(); //add this line



        this.fetch()
       
      var result  = this.props.navigation.getParam('result')
 //ToastAndroid.show("id "+ result["currency"] , ToastAndroid.LONG);
      this.setState({
        fueltype : result["dummy_name"],
        total_amt:result["dummy_price"],
        total:result["dummy_price"].toString(),
        price_prev:result["dummy_price"],
        currency:result["currency"],
        got_price:parseFloat(result["dummy_price"]),
	    	global_price:parseFloat(result["dummy_price"]).toFixed(2),
        service_array:[...this.state.service_array ,result["service_id"].toString()],
        sub_service_array:[...this.state.sub_service_array ,0 ],
        price_array:[...this.state.price_array ,result["dummy_price"].toString()],
    })
    this.getDelivery()

}
    convertDate(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString();
      var dd  = date.getDate().toString();

      var mmChars = mm.split('');
      var ddChars = dd.split('');

      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    }

    onSelect(index, value){
      if(value === 'cash'){
        this.setState({cash_status:'cash'})
      }
      else if(value === 'online'){
        this.setState({cash_status:'online'})
      }
    }
    isValid(){
      let valid = false;
      if (this.state.date_obj.toString().length > 0 && this.state.time.length > 0) {
        valid = true;

      }

      console.log(this.state.selected_service_two)
      console.log(this.state.selected_sub_service_two)
      console.log(this.state.service_two_picker)


      if(this.state.quantity < 1.0 ){
      //  ToastAndroid.show('Enter a valid Quantity', ToastAndroid.SHORT);

        Platform.OS === 'android'
        ? ToastAndroid.show('Enter a valid Quantity', ToastAndroid.SHORT)
        : Alert.alert('Enter a valid Quantity')


        return false;
      }

       else if (this.state.date_obj.length === 0) {
        //this.setState({ error: 'You must enter an email address' });
       // ToastAndroid.show('Enter a Date', ToastAndroid.SHORT);

        Platform.OS === 'android'
        ? ToastAndroid.show('Enter a Date', ToastAndroid.SHORT)
        : Alert.alert('Enter a Date')



        return false;
      } else if (this.state.time.length === 0) {
      //  this.setState({ error: 'You must enter a password' });
      //  ToastAndroid.show('Enter a time', ToastAndroid.SHORT);

        Platform.OS === 'android'
        ? ToastAndroid.show('Enter a time', ToastAndroid.SHORT)
        : Alert.alert('Enter a time')


        return false;
      }

     if(this.state.service_one_picker && this.state.selected_service_one > 0 ||this.state.selected_sub_service_one > 0 ){
        if(this.state.selected_sub_service_one < 0 ||  this.state.selected_sub_service_one ==="key0" ||this.state.selected_sub_service_one.length === 0){
         // ToastAndroid.show('Select Sub Service one ', ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show('Select Sub Service one ', ToastAndroid.SHORT)
          : Alert.alert('Select Sub Service one ')


          return false;
        }

      }
      
       if(this.state.service_two_picker && this.state.selected_service_two > 0 ||this.state.selected_sub_service_two > 0){
        console.log("Sdfsd")
        if(this.state.selected_sub_service_two < 0 ||  this.state.selected_sub_service_two =="key0" ||this.state.selected_sub_service_two.length === 0){
         // ToastAndroid.show('Select Sub Service two ', ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show('Select Sub Service two ', ToastAndroid.SHORT)
          : Alert.alert('Select Sub Service two ')


          return false;
        }

      }
      if(this.state.service_three_picker  && this.state.selected_service_three > 0 ||this.state.selected_sub_service_three > 0){
        if(this.state.selected_sub_service_three < 0 ||  this.state.selected_sub_service_three ==="key0" ||this.state.selected_sub_service_three.length === 0){
        //  ToastAndroid.show('Select Sub Service three ', ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show('Select Sub Service three ', ToastAndroid.SHORT)
          : Alert.alert('Select Sub Service three ')


          return false;
        }

      }
     if(this.state.service_four_picker && this.state.selected_service_four > 0 ||this.state.selected_sub_service_four > 0){
        if(this.state.selected_sub_service_four < 0 ||  this.state.selected_sub_service_four ==="key0" ||this.state.selected_sub_service_four.length == 0){
        //  ToastAndroid.show('Select Sub Service four', ToastAndroid.SHORT);

          Platform.OS === 'android'
          ? ToastAndroid.show('Select Sub Service four', ToastAndroid.SHORT)
          : Alert.alert('Select Sub Service four')



          return false;
        }

      }

      return valid;
    }


    applyPromo(){

      if(this.state.promo.trim().length ===0){
       // ToastAndroid.show('Enter promocode', ToastAndroid.SHORT);

        Platform.OS === 'android'
        ? ToastAndroid.show('Enter promocode', ToastAndroid.SHORT)
        : Alert.alert('Enter promocode')


        return
      }
      var formData = new FormData();
      //txnid and payal id should have same value
      formData.append('promo_code', this.state.promo);
      this.setState({loading_status:true})
      let url = urls.base_url + 'api_verify_promocode'
      fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':  'multipart/form-data',
      },
      body: formData

    }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({loading_status:false})

          if(!responseJson.error){
                //success in inserting data
               // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                Platform.OS === 'android'
                ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                : Alert.alert(responseJson.message)




                var promo_id = responseJson.result.Promocode.id
                var promo_discount = responseJson.result.Promocode.discount
                var promo_code = responseJson.result.Promocode.code


                var price = this.state.global_price
                var discount_amt = (price * promo_discount)/100
                price = price - (price * promo_discount)/100
                this.setState({global_price:price,promo_code:promo_id,discount_amount:discount_amt,net_amount:price})
        }else{


                  //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);


                  Platform.OS === 'android'
                  ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                  : Alert.alert(responseJson.message)
          }


          }).catch((error) => {
            Platform.OS === 'android'
            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
            : Alert.alert("Error")
          });
    }


dummy(){
  // ToastAndroid.show(JSON.stringify(confirm), ToastAndroid.LONG)
   //ToastAndroid.show(confirm.response.id+"...."+price+"..."+bookingId, ToastAndroid.LONG)
    var pid = confirm.response.id
    //this.paypalpaymentapi(price,bookingId,pid)
    var result  = this.props.navigation.getParam('result')
    var uid = result["user_id"]
      //ToastAndroid.show(price+"...."+bid+"..."+uid+"...", ToastAndroid.SHORT);
    var date = this.convertDate(Date.now())

    ToastAndroid.show("price"+price+"bokingid...."+bookingId+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
    var formData = new FormData();
    //txnid and payal id should have same value
    formData.append('user_id', uid);
    formData.append('order_id',bookingId);
    formData.append('txn_status',"approved");
    formData.append('txn_id', pid);
    formData.append('txn_amt',price);
    formData.append('txn_date',date);
    formData.append('paypal_id',pid);

    let url = urls.base_url + 'api_make_payment'
    fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type':  'multipart/form-data',
    },
    body: formData

  }).then((response) => response.json())
        .then((responseJson) => {

        if(!responseJson.error){
              //success in inserting data
              ToastAndroid.show("Booked Succesfully", ToastAndroid.LONG);
              this.props.navigation.navigate('HomeScreen');

      }else{


                ToastAndroid.show("Booked failed", ToastAndroid.SHORT);
        }


        }).catch((error) => {
                                        Platform.OS === 'android'
                                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                        : Alert.alert("Error")
        });
}
paypalpaymentapi(price,bid,pid){

//  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

          var result  = this.props.navigation.getParam('result')
          var uid = result["user_id"]
            //ToastAndroid.show(price+"...."+bid+"..."+uid+"...", ToastAndroid.SHORT);
            var date = this.convertDate(new Date())

          //ToastAndroid.show("price"+price+"bokingid...."+bid+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
          var formData = new FormData();
          //txnid and payal id should have same value
          formData.append('user_id', uid.toString());
          formData.append('order_id',bid.toString());
          formData.append('txn_status',"approved".toString());
          formData.append('txn_id', pid.toString());
          formData.append('txn_amt',price);
          formData.append('txn_date',date.toString());
          formData.append('paypal_id',pid.toString());
    formData.append('currency',this.state.currency)
    let url = urls.base_url + 'api_make_payment'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type':  'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {

              if(!responseJson.error){
                    //success in inserting data
                 //   ToastAndroid.show("Booked Succesfully", ToastAndroid.LONG);

                    Platform.OS === 'android'
                    ? ToastAndroid.show("Booked Succesfully", ToastAndroid.SHORT)
                    : Alert.alert("Booked Succesfully")



                    this.props.navigation.navigate('HomeScreen');

            }else{


                    //  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                      Platform.OS === 'android'
                      ? ToastAndroid.show("Booked Failed", ToastAndroid.SHORT)
                      : Alert.alert("Booked Failed")


              }


              }).catch((error) => {
                Platform.OS === 'android'
                ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                : Alert.alert("Error")
              });
}
paypal(price,bookingId){
  //ToastAndroid.show("Initializing paypal",ToastAndroid.LONG)

    // PayPal.initialize(PayPal.SANDBOX, "ARFiHtTEV2P17X4Hdei6M4vqoLx_UVGAvELcTpNprLC4hNklLmNq7cl_z5OqhRGoBzzVRmS9sG-RfhXx");
    PayPal.initialize(PayPal.LIVE, "ASwGC44QIOilJNxdU0IVmTalJ1Y2zjS-k7NkTykVXld9fzDJ-XEQq5pVkpSbkPC0W_SaNcB8D6yhgZGb");
    PayPal.pay({
      price: price.toString(),
      currency:"USD" ,
      description: 'Payment for fuel delivery app',
    }).then(confirm => {

      var pid = confirm.response.id
      this.paypalpaymentapi(price,bookingId,pid)


      //ToastAndroid.show("price"+price+"bokingid...."+bookingId+"uiddd..."+pid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
    })
      .catch(error =>{
       // ToastAndroid.show("Transaction declined by user", ToastAndroid.SHORT);


        Platform.OS === 'android'
        ? ToastAndroid.show("Transaction declined by user", ToastAndroid.SHORT)
        : Alert.alert("Transaction declined by user")


          this.setState({cash_status:'cash'})

      })


}

convertCurrency(price,bid){
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('amount', price);


  let url = urls.base_url + 'get_converted_currency'
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData

       }).then((response) => response.json())
             .then((responseJson) => {
            this.setState({loading_status:false})
             if(!responseJson.error){
              var amt = responseJson.converted_amt
              this.paypal(amt,bid)
             }





             }).catch((error) => {
              Platform.OS === 'android'
              ? ToastAndroid.show("Error", ToastAndroid.SHORT)
              : Alert.alert("Error")
             });
}


cashOnDelivery(){
  this.setState({loading_status:true})
  //  ToastAndroid.show('quantitytytyt'+this.state.quantity , ToastAndroid.SHORT);
  var result  = this.props.navigation.getParam('result')

  result["qty"] = this.state.quantity
  result["booking_date"] = this.state.date_obj
  result["booking_time"] = this.state.time
  result["booking_amt"] = this.state.global_price



  var uid = result["user_id"]
  var serviceid = result["service_id"]
  var loc_name = result["location_name"]
  var loc_lat = result["location_lat"]
  var loc_long = result["location_long"]
  var vehicleid = result["vehicle_id"]
  var makeid = result["make_id"]
  var modelid = result["model_id"]
  var colorid = result["color_id"]
  var vehiclereg = result["vehicle_reg_num"]
  var fuelid = result["fuel_id"]
  var fuelsubid = result["fuel_subtype_id"]
  var qty =this.state.quantity
  var bookingdate = result["booking_date"]
  var bookingtime = result["booking_time"]
  var bookingamt = result["booking_amt"] + this.state.discount_amount

  var formData = new FormData();


  formData.append('user_id', uid);

  for(var i = 0 ; i < this.state.service_array.length ; i++){
          formData.append('service_id[' +i+']',this.state.service_array[i].toString());
    }

    for(var i = 0 ; i < this.state.sub_service_array.length ; i++){
            formData.append('sub_service_id[' +i+']',this.state.sub_service_array[i].toString());
      }

      for(var i = 0 ; i < this.state.price_array.length ; i++){
              formData.append('price[' +i+']',this.state.price_array[i].toString());
        }
//  formData.append('service_id',this.state.service_array);
  //formData.append('sub_service_id',this.state.sub_service_array);
  //formData.append('sub_service_id',this.state.price_array);
  formData.append('location_name',loc_name );
  formData.append('location_lat',loc_lat );
  formData.append('location_long',loc_long );
  formData.append('vehicle_id',vehicleid );
  formData.append('make_id', makeid);
  formData.append('model_id', modelid);
  formData.append('color_id', colorid);
  formData.append('vehicle_reg_num',vehiclereg );
  formData.append('fuel_id',fuelid );
  formData.append('fuel_subtype_id',fuelsubid );
  formData.append('qty',qty );
  formData.append('promocode_id',this.state.promo_code );
  formData.append('booking_date', bookingdate);
  formData.append('booking_time',bookingtime );
  formData.append('booking_amt', bookingamt);
  formData.append('payment_method',"cash")
  formData.append('discount_amt', this.state.discount_amount)
    formData.append('net_amt',parseInt(bookingamt) - parseInt(this.state.net_amount) + parseInt(this.state.delivery_fee))
    formData.append('delivery_fee',this.state.delivery_fee);
formData.append('currency',this.state.currency);

console.log(formData)
//ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)
let url = urls.base_url + 'api_make_booking'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData

         }).then((response) => response.json())
               .then((responseJson) => {
                console.log(responseJson)
               //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                    this.setState({loading_status:false})

                   if(!responseJson.error){
                   //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)

                       // ToastAndroid.show("Successfully booked !",ToastAndroid.SHORT)

                        Platform.OS === 'android'
                        ? ToastAndroid.show("Successfully booked !", ToastAndroid.SHORT)
                        : Alert.alert("Successfully booked !")


                        this.props.navigation.navigate("HomeScreen")

                       }else{

                         }

               }).catch((error) => {
               // ToastAndroid.show(error.toString(),ToastAndroid.LONG)
               this.setState({loading_status:false})
               console.log(error.message)
                Platform.OS === 'android'
                ? ToastAndroid.show("Failed !", ToastAndroid.SHORT)
                : Alert.alert("Failed !")


               });
}



onlinePayment(){
//  ToastAndroid.show('quantitytytyt'+this.state.quantity , ToastAndroid.SHORT);
  this.setState({loading_status:true})
  var result  = this.props.navigation.getParam('result')

  result["qty"] = this.state.quantity
  result["booking_date"] = this.state.date_obj
  result["booking_time"] = this.state.time
  result["booking_amt"] = this.state.global_price



  var uid = result["user_id"]
  var serviceid = result["service_id"]





  var loc_name = result["location_name"]
  var loc_lat = result["location_lat"]
  var loc_long = result["location_long"]
  var vehicleid = result["vehicle_id"]
  var makeid = result["make_id"]
  var modelid = result["model_id"]
  var colorid = result["color_id"]
  var vehiclereg = result["vehicle_reg_num"]
  var fuelid = result["fuel_id"]
  var fuelsubid = result["fuel_subtype_id"]
  var qty = this.state.quantity
  var bookingdate = result["booking_date"]
  var bookingtime = result["booking_time"]
  var bookingamt = result["booking_amt"] + this.state.discount_amount

  var formData = new FormData();



  formData.append('user_id', uid);
  //formData.append('service_id',serviceid );
  for(var i = 0 ; i < this.state.service_array.length ; i++){
          formData.append('service_id[' +i+']',this.state.service_array[i].toString());
    }

    for(var i = 0 ; i < this.state.sub_service_array.length ; i++){
            formData.append('sub_service_id[' +i+']',this.state.sub_service_array[i].toString());
      }

      for(var i = 0 ; i < this.state.price_array.length ; i++){
              formData.append('price[' +i+']',this.state.price_array[i].toString());
        }
  formData.append('location_name',loc_name );
  formData.append('location_lat',loc_lat );
  formData.append('location_long',loc_long );
  formData.append('vehicle_id',vehicleid );
  formData.append('make_id', makeid);
  formData.append('model_id', modelid);
  formData.append('color_id', colorid);
  formData.append('vehicle_reg_num',vehiclereg);
  formData.append('fuel_id',fuelid );
  formData.append('fuel_subtype_id',fuelsubid );
  formData.append('qty',qty );
  formData.append('booking_date', bookingdate);
  formData.append('booking_time',bookingtime );
  formData.append('booking_amt', bookingamt);
  formData.append('payment_method',"online");
  formData.append('promocode_id',this.state.promo_code );
  formData.append('discount_amt', this.state.discount_amount)
  formData.append('net_amt',parseInt(bookingamt) - parseInt(this.state.net_amount) + parseInt(this.state.delivery_fee))
    formData.append('delivery_fee',this.state.delivery_fee);
    formData.append('currency',this.state.currency);

    let url = urls.base_url + 'api_make_booking'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData

         }).then((response) => response.json())
               .then((responseJson) => {
                // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                this.setState({loading_status:false})
               if(!responseJson.error){
                 //use this booking id to hit api
                    var booking_id = responseJson.booking_id
                    // if(!empty(booking_id)){
                    //   booking_id = 0.toString()
                    // }
                    var result  = this.props.navigation.getParam('result')
                     //ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
                     let price = parseFloat(this.state.global_price) + parseFloat(this.state.delivery_fee)

                     this.state.currency == 'USD' 
                     ?  this.paypal(price,booking_id)
                     : this.convertCurrency(price,booking_id)
                   
                    //this.props.navigation.pop(3)
                      return true;
                   }else{

                     }

               }).catch((error) => {
                 console.error(error);
               });

}

// convertDate(date) {
//   var yyyy = date.getFullYear().toString();
//   var mm = (date.getMonth()+1).toString();
//   var dd  = date.getDate().toString();
//
//   var mmChars = mm.split('');
//   var ddChars = dd.split('');
//
//   return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
// }


    continue(){
      if(this.isValid()){
      //  ToastAndroid.show('going in cassssshhh', ToastAndroid.SHORT);
            if(this.state.cash_status === 'cash'){
              //ToastAndroid.show('going in cassssshhh', ToastAndroid.SHORT);

                                      NetInfo.isConnected.fetch().then(isConnected => {
                                                     if(!isConnected)
                                                     {
                                                       this.props.navigation.navigate("NoNetwork")
                                                       return false;
                                                     }
                                                     else{

                                                       //fetching all fata from server
                                                    //   ToastAndroid.show('going in cassssshhh', ToastAndroid.SHORT);
                                                       this.cashOnDelivery()

                                                     }

                              })

             //
            }
            else if(this.state.cash_status === 'online'){
              //ToastAndroid.show('going in onlineeeeeee', ToastAndroid.SHORT);
                //go to paypal payment

                NetInfo.isConnected.fetch().then(isConnected => {
                               if(!isConnected)
                               {
                                 this.props.navigation.navigate("NoNetwork")
                                 return false;
                               }
                               else{

                                 //fetching all fata from server
                                //

                                   this.onlinePayment()

                               }

        })
            }
          }
    }

    getItem(sub_service_arr,value){
      let length = sub_service_arr.length

      for(var i = 0 ; i < length ; i++){
            if(sub_service_arr[i].id == value){
              return sub_service_arr[i]
            }
        }
    }

        //for time
        _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

         _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

         _handleDatePicked = (time) => {
           console.log('A date has been picked: ', time);
           this.setState({time:time.toString().substring(16,24)})
           this._hideDateTimePicker();
         };


        //for date
         showDatePicker = () => this.setState({ isDatePickerVisible: true });

          hideDatePicker = () => this.setState({ isDatePickerVisible: false });

          handleDatePicked = (date) => {

            //console.log('A date has been picked: ', date);
            this.setState({date_obj:this.convertDate(date),date:date.toString().substring(4,15)})
            this.hideDatePicker();
          };

    render(){


      let makeServices =this.state.services.map((makes) => {
                        return <Item label={makes.name} value={makes.key} key={makes.key}/>
                    })

      let makeSubServices =this.state.sub_services_one.map((makes) => {
                                      return <Item label={makes.name} value={makes.id} key={makes.id}/>
                    })

        let makeSubServicesTwo =this.state.sub_services_two.map((makes) => {
                                                    return <Item label={makes.name} value={makes.id} key={makes.id}/>
                        })
    let makeSubServicesThree =this.state.sub_services_three.map((makes) => {
                                                                    return <Item label={makes.name} value={makes.id} key={makes.id}/>
                              })
      let makeSubServicesFour =this.state.sub_services_four.map((makes) => {
                                          return <Item label={makes.name} value={makes.id} key={makes.id}/>
                                    })


              if (this.state.loading_status) {
                return (
                  <ActivityIndicator
                    animating={true}
                    style={styles.indicator}
                    size="large"
                  />
                );
              }

        return(
            <ScrollView>
            <View style={styles.container}>


                <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginRight:'40%'}}>Book Your Order</Text>

                <TextInput
                  placeholder={'s'}
                  style={styles.input}
                  editable={false}
                  value ={this.state.fueltype}
                />


                <TextInput
                  value={this.state.quantity}
                  maxLength={4}
                  onChangeText={(quantity) => {
                    //this.setState({ quantity_textinput:quantity.trim() })

                    if(isNaN(quantity) || quantity.toString().length == 0 ){
						let price = parseFloat(this.state.quantity)* parseFloat(this.state.got_price)
						 let price_two =  parseFloat(this.state.global_price)
						 let temp = price_two - price
                        let total_price = parseFloat(this.state.got_price) * parseFloat('1') + parseFloat(temp)
                        let arr = this.state.price_array
                        arr[0] = parseFloat(this.state.got_price) * parseFloat(quantity)
                        this.setState({total_amt:parseFloat(total_price).toFixed(2),total : total_price,quantity:1,
                            price_array:arr,global_price:parseFloat(total_price).toFixed(2)
                        })

                    }
                    else{
                      var result  = this.props.navigation.getParam('result')
                      //ToastAndroid.show("quamtittt id is "+ value, ToastAndroid.LONG);
                      // let price = result["dummy_price"]
                      // let total_price = parseFloat(price) * parseFloat(quantity)
                      // let arr = this.state.price_array
                      // arr[0] = total_price.toString()
                      // this.setState({total_amt:parseFloat(total_price).toFixed(2),total : total_price,quantity:parseFloat(quantity),
                      //     price_array:arr
                      // })


                        let price = parseFloat(this.state.quantity)* parseFloat(this.state.got_price)
                      let price_two =  parseFloat(this.state.global_price)
                      let temp = price_two - price
                        let total_price = parseFloat(this.state.got_price) * parseFloat(quantity) + parseFloat(temp)
                        let arr = this.state.price_array
                        arr[0] = parseFloat(this.state.got_price) * parseFloat(quantity)
                        this.setState({total_amt:parseFloat(total_price).toFixed(2),total : total_price,quantity:quantity,
                            price_array:arr,global_price:parseFloat(total_price).toFixed(2)
                        })

                    }

                  }
                  }
                  placeholder={'1.0'}
                  style={styles.input}
                  keyboardType = 'numeric'
                  placeholderTextColor={'black'}
                />




                     <DateTimePicker
                     mode="date"
                     isVisible={this.state.isDatePickerVisible}
                     onConfirm={this.handleDatePicked}
                     onCancel={this.hideDatePicker}
                     minimumDate = {new Date()}
                     >
                     </DateTimePicker>



                     <View style={{flexDirection: 'row',borderRadius: 10,
                             borderWidth: 2,
                             borderColor: 'black', width :'100%',marginBottom:20,justifyContent:'space-between',paddingRight:3,alignItems:'center'}}>
                     <TextInput style = {{width :'70%',height:45,fontWeight:'bold',color:'black'}} editable={false} placeholder={'Date'} value ={this.state.date_obj} />

                     <TouchableOpacity  onPress={this.showDatePicker}>
                           <Image style = {{width:35,height:35,justifyContent:'flex-end'}} source={require('./calendar.png')}></Image>
                     </TouchableOpacity>

                     </View>



            <View style={{flexDirection: 'row',borderRadius: 10,
                    borderWidth: 2,
                    borderColor: 'black', width :'100%',marginBottom:15,justifyContent:'space-between',paddingRight:3,alignItems:'center'}}>
            <TextInput style = {{width :'70%',height:45,fontWeight:'bold',color:'black'}} placeholder={'Time'} editable={false} value ={this.state.time} />

            <TouchableOpacity  onPress={this._showDateTimePicker}>
                  <Image style = {{width:35,height:35,justifyContent:'flex-end'}} source={require('./clock-circular-outline.png')}></Image>
            </TouchableOpacity>

            </View>



            <DateTimePicker
            mode="time"
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
            >
            </DateTimePicker>



            {/* services one*/}
                         {
                           !this.state.service_one_text ?
                           null :


                           <TouchableOpacity onPress={() => {
                             this.setState({
                               service_one_text:false,
                               service_one_picker:true,
                               service_two_text:true
                             })
                           }}>
                           <View style={{flexDirection:'row',alignItems:'center'}}>
                           <Text style={{textDecorationLine: 'underline', color: 'black' ,fontSize: 18,marginRight:10}}>Add more services</Text>
                           <Image source={require('./plus-button.png')} style={{height:25, width:25,marginRight: 10}} />
                           </View>

                           </TouchableOpacity>

                         }
                          {/* services */}

                          {
                            !this.state.service_one_picker ?
                            null :

                                          <View style={{width:'100%'}}>
                                            <Text style={{ fontSize: 19, color: 'black',alignItems:'center',marginTop:10,marginBottom:10,fontWeight: 'bold',marginLeft:10 }}>Service  one : </Text>
                                        <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                      <Picker

                                                        mode="dropdown"
                                                        selectedValue={this.state.selected_service_one}
                                                        onValueChange={(value) =>
                                                          {

                                                            //this.setState({selected_make: value})
                                                            //ToastAndroid.show("make id is "+ value, ToastAndroid.LONG)
                                                            if(value>0){

																let price = parseFloat(this.state.global_price)
																let price_two = 0.0
																if(this.state.price_array[1] == null )
																{

																	price_two =price - 0.0
																}

																else{
																	price_two =price - parseFloat(this.state.price_array[1])
																}



                                                               this.getSubServicesOne(value);
                                                               let a = this.state.service_array
                                                               a[1] = value

                                                               console.log("one upar wala")
                                                               console.log("service array .." + this.state.service_array)
                                                               console.log("selected_service_one .." + value)
                                                               console.log("Global price .." + parseFloat(price_two))



                                                               this.setState({
                                                                 service_array:a,
                                                                 selected_service_one:value,
																                                 global_price:parseFloat(price_two),
                                                               })
                                                            }


                                                          }
                                                          }
                                                        >
                                                        <Item label="Select a service" value="key0" />
                                                       {makeServices}
                                                     </Picker>
                                                     </Card>

                                                     {/* mcolot */}
                                 <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                 <Picker

                                   mode="dropdown"
                                   selectedValue={this.state.selected_sub_service_one}
                                   onValueChange={(value,index) =>
                                     {


                                       if(value>0){

                                                if(this.state.sub_service_array.indexOf(value) > -1){

                                                  console.log("Already selected same value")


                                                }



										                else{
                                       let item =   this.getItem(this.state.sub_services_one,value)
											                  // let item = this.state.sub_services_one[index-1]
                                        let price = parseFloat(this.state.global_price)
                                        let b = this.state.price_array

                                        if(isNaN(b[1])){

                                         
                                          let price_two = price  + parseFloat(item.price)
                                          //ToastAndroid.show("Price is "+parseFloat(b[1]),ToastAndroid.LONG)
                                           let a = this.state.sub_service_array
                                           //let b = this.state.price_array
                                           b[1]= item.price
                                           a[1] = value

                                           console.log("Price was null.." +  price_two)
                                           console.log("sub service array ..."+a)
                                           console.log("pricecc array ..."+b)
                                           console.log("global price ..."+parseFloat(price_two))


                                           this.setState({
                                             sub_service_array:a,
                                             price_array:b,
                                             selected_sub_service_one:value,

                                             global_price:parseFloat(price_two),
                                           })
                                        }
                                        else{
                                          let price_two = price - parseFloat(b[1]) + parseFloat(item.price)
                                         // ToastAndroid.show("Price is "+parseFloat(b[1]),ToastAndroid.LONG)
                                           let a = this.state.sub_service_array
                                           //let b = this.state.price_array
                                           b[1]= item.price
                                           a[1] = value


                                           console.log("Price was  not null.." +  price_two)
                                           console.log("sub service array ..."+a)
                                           console.log("pricecc array ..."+b)
                                           console.log("global price ..."+parseFloat(price_two))



                                           this.setState({
                                             sub_service_array:a,
                                             price_array:b,
                                             selected_sub_service_one:value,

                                             global_price:parseFloat(price_two),
                                           })
                                        }

										   }



                                       }

                                     }
                                     }
                                   >
                                   <Item label="Sub Service" value="key0" />
                                  {makeSubServices}
                                </Picker>
                                </Card>

                                {
                                  this.state.service_two_picker ?
                                    null :
                                <Text style={{margin:10,fontWeight: 'bold',color:'black'}}
                                onPress={
                                  ()=>{

                                    if(this.state.selected_service_one > 0  && this.state.selected_sub_service_one > 0){
                                      // this.setState({
                                      //   service_two_text:false,
                                      //   service_one_text:true,
                                      //   service_one_picker:false,
                                      //     service_two_picker:false,
                                      //    selected_service_one:'',
                                      //    sub_services_one:[]
                                      // })


                                      let a = this.state.sub_service_array
                                      let b = this.state.price_array
                                      let c=  this.state.service_array

                                      let price = parseFloat(this.state.global_price) -  parseFloat(b[1])



                                      a = a.splice(1,1)
                                      b = b.splice(1,1)
                                      c = c.splice(1,1)



                                      console.log("A  "+JSON.stringify(a))
                                      console.log("B "+JSON.stringify(b))
                                      console.log("C   "+JSON.stringify(c))
                                      console.log("Cproce   "+JSON.stringify(price))
                                      console.log("globaa;;;   "+JSON.stringify(parseFloat(price).toFixed(2)))



                                      this.setState({
                                        sub_service_array:a,
                                        price_array:b,
                                        service_array:c,
                                        service_two_text:false,
                                        service_one_text:true,
                                          service_two_picker:false,
                                        service_one_picker:false,
                                        selected_service_one:'',
                                        sub_services_one:[],
                                        global_price:parseFloat(price).toFixed(2),

                                      })
                                    }
                                    else if(this.state.selected_service_one > 0){

                                      console.log("ONLY one")
                                      console.log(this.state.selected_service_one)
                                      console.log(this.state.selected_sub_service_one)
                                      let c=  this.state.service_array
                                      c = c.splice(1,1)
                                      this.setState({
                                       
                                        service_array:c,
                                        service_two_text:false,
                                        service_one_text:true,
                                          service_two_picker:false,
                                        service_one_picker:false,
                                        selected_service_one:'',
                                        sub_services_one:[],
                                      

                                      })
                                    }
                                    else{

                                      console.log("elseeeee")
                                      console.log(this.state.selected_service_one)
                                      console.log(this.state.selected_sub_service_one)

                                      this.setState({
                                        service_two_text:false,
                                        service_one_text:true,
                                        service_one_picker:false,
                                          service_two_picker:false,
                                         selected_service_one:'',
                                         sub_services_one:[]
                                      })




                                      // let a = this.state.sub_service_array
                                      // let b = this.state.price_array
                                      // let c=  this.state.service_array
                                      //
                                      // let price = parseFloat(this.state.global_price) -  parseFloat(b[1])
                                      //
                                      //
                                      //
                                      // a = a.splice(1,1)
                                      // b = b.splice(1,1)
                                      // c = c.splice(1,1)
                                      //
                                      //
                                      // this.setState({
                                      //   sub_service_array:a,
                                      //   price_array:b,
                                      //   service_array:c,
                                      //   service_two_text:false,
                                      //   service_one_text:true,
                                      //     service_two_picker:false,
                                      //   service_one_picker:false,
                                      //   selected_service_one:'',
                                      //   sub_services_one:[],
                                      //   global_price:parseFloat(price).toFixed(2),
                                      //
                                      // })
                                    }

                                  }
                                }>Remove</Text>

                              }
                            </View>
                          }

                          {/* services two*/}
                                       {
                                         !this.state.service_two_text ?
                                         null :


                                         <TouchableOpacity onPress={() => {
                                           this.setState({
                                             service_two_text:false,
                                             service_two_picker:true,
                                             service_three_text:true
                                           })
                                         }}>
                                         <View style={{flexDirection:'row',alignItems:'center'}}>
                                         <Text style={{textDecorationLine: 'underline', color: 'black' ,fontSize: 18,marginRight:10}}>Add more services</Text>
                                         <Image source={require('./plus-button.png')} style={{height:25, width:25,marginRight: 10}} />
                                         </View>

                                         </TouchableOpacity>

                                       }
                                        {/* services */}

                                        {
                                          !this.state.service_two_picker ?
                                          null :

                                                        <View style={{width:'100%'}}>
                                                        <Text style={{ fontSize: 19, color: 'black',alignItems:'center',marginTop:10,marginBottom:10,fontWeight: 'bold',marginLeft:10 }}>Service two : </Text>
                                                      <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                                    <Picker

                                                                      mode="dropdown"
                                                                      selectedValue={this.state.selected_service_two}
                                                                      onValueChange={(value) =>
                                                                        {

                                                                          //this.setState({selected_make: value})
                                                                          //ToastAndroid.show("make id is "+ value, ToastAndroid.LONG)
                                                                          if(value>0){
																			  let price = parseFloat(this.state.global_price)
																				let price_two = 0.0
																if(this.state.price_array[2] == null )
																{

																	price_two =price - 0.0
																}

																else{
																	price_two =price - parseFloat(this.state.price_array[2])
																}

                                                                             this.getSubServicesTwo(value);
                                                                             let a = this.state.service_array
                                                                             a[2] = value


                                                                             console.log("two upar wala")
                                                                             console.log("service array .." + this.state.service_array)
                                                                             console.log("selected_service_one .." + value)
                                                                             console.log("Global price .." + parseFloat(price_two))



                                                                             this.setState({
                                                                               service_array:a,
                                                                              selected_service_two:value
                                                                             })
                                                                          }


                                                                        }
                                                                        }
                                                                      >
                                                                      <Item label="Select a service" value="key0" />
                                                                     {makeServices}
                                                                   </Picker>
                                                                   </Card>

                                                                   {/* mcolot */}
                                               <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                               <Picker

                                                 mode="dropdown"
                                                 selectedValue={this.state.selected_sub_service_two}
                                                 onValueChange={(value,index) =>
                                                   {


                                                     if(value>0){


														  if(this.state.sub_service_array.indexOf(value) > -1){
										   }
										   else{
                                                         let item =   this.getItem(this.state.sub_services_two,value)
                                                         let price = parseFloat(this.state.global_price)
                                                         let b = this.state.price_array


                                                       if(isNaN(b[2])){
                                                            let price_two =price + parseFloat(item.price)
                                                            let a = this.state.sub_service_array
                                                            b[2]= item.price
                                                            a[2] = value
                                                            //price = price + item.price

                                                            console.log("Price was null.." +  price_two)
                                                            console.log("sub service array ..."+a)
                                                            console.log("pricecc array ..."+b)
                                                            console.log("global price ..."+parseFloat(price_two))
                 

                                                            this.setState({
                                                              sub_service_array:a,
                                                              price_array:b,
                                                              selected_sub_service_two:value,
                                                              global_price:parseFloat(price_two),
                                                            })
                                                       }
                                                       else{
                                                              let price_two =price - parseFloat(b[2]) + parseFloat(item.price)
                                                            let a = this.state.sub_service_array
                                                            b[2]= item.price
                                                            a[2] = value
                                                            //price = price + item.price



                                           console.log("Price was  not null.." +  price_two)
                                           console.log("sub service array ..."+a)
                                           console.log("pricecc array ..."+b)
                                           console.log("global price ..."+parseFloat(price_two))


                                                            this.setState({
                                                              sub_service_array:a,
                                                              price_array:b,
                                                              selected_sub_service_two:value,
                                                                global_price:parseFloat(price_two),
                                                            })
                                                       }


										   }
                                                     }

                                                   }
                                                   }
                                                 >
                                                 <Item label="SubService" value="key0" />
                                                {makeSubServicesTwo}
                                              </Picker>
                                              </Card>

                                              {
                                                this.state.service_three_picker ?
                                                  null :
                                              <Text style={{margin:10,fontWeight: 'bold',color:'black'}}
                                              onPress={
                                                ()=>{
                                                  // this.setState({
                                                    // service_three_text:false,
                                                    // service_two_text:true,
                                                    // service_two_picker:false
                                                  // })


                                                                                      if(this.state.selected_service_two > 0 && this.state.selected_sub_service_two > 0  ){


                                                                                        let a = this.state.sub_service_array
                                                                                        let b = this.state.price_array
                                                                                        let c=  this.state.service_array
                                                                                          let price = parseFloat(this.state.global_price) -  parseFloat(b[2])
                                                                                        a = a.splice(2,1)
                                                                                        b = b.splice(2,1)
                                                                                        c = c.splice(2,1)


                                                                                        console.log("A  "+JSON.stringify(a))
                                                                                        console.log("A with splice  "+ a.splice(2,1))
                                                                                        console.log("B "+JSON.stringify(b))
                                                                                        console.log("C   "+JSON.stringify(c))
                                                                                        console.log("Cproce   "+JSON.stringify(price))
                                                                                        console.log("globaa;;;   "+JSON.stringify(parseFloat(price).toFixed(2)))
                                                                                        

                                                                                        this.setState({
                                                                                          sub_service_array:a,
                                                                                          price_array:b,
                                                                                          service_array:c,
                                                                                          service_three_text:false,
                                                                                          service_two_text:true,
                                                                                          service_two_picker:false,
                                                                                          selected_service_two:'',
                                                                                          sub_services_two:[],
                                                                                          global_price:parseFloat(price).toFixed(2),

                                                                                        })
                                                                                      }
                                                                                      else if(this.state.selected_service_two > 0){
                                                                                        let c=  this.state.service_array
                                                                                        c = c.splice(2,1)

                                                                                        console.log("one")
                                                                                        console.log(this.state.selected_service_two)
                                                                                        console.log(this.state.selected_sub_service_two)



                                                                                        this.setState({
                                                                                         
                                                                                          service_array:c,
                                                                                          service_three_text:false,
                                                                                          service_two_text:true,
                                                                                          service_two_picker:false,
                                                                                           selected_service_two:'',
                                                                                           sub_services_two:[]
                                                                                        
                                                  
                                                                                        })
                                                                                      }
                                                                                      else{


                                                                                        console.log("elseee")
                                                                                        console.log(this.state.selected_service_two)
                                                                                        console.log(this.state.selected_sub_service_two)


                                                                                        this.setState({
                                                                                          service_three_text:false,
                                                                                          service_two_text:true,
                                                                                          service_two_picker:false,
                                                                                           selected_service_two:'',
                                                                                           sub_services_two:[]
                                                                                        })
                                                                                      }
                                                }
                                              }
                                              >Remove</Text>

                                            }
                                          </View>
                                        }



                                        {/* services three*/}
                                                     {
                                                       !this.state.service_three_text ?
                                                       null :


                                                       <TouchableOpacity onPress={() => {
                                                         this.setState({
                                                           service_three_text:false,
                                                           service_three_picker:true,
                                                           service_four_text:true
                                                         })
                                                       }}>
                                                       <View style={{flexDirection:'row',alignItems:'center'}}>
                                                       <Text style={{textDecorationLine: 'underline', color: 'black' ,fontSize: 18,marginRight:10}}>Add more services</Text>
                                                       <Image source={require('./plus-button.png')} style={{height:25, width:25,marginRight: 10}} />
                                                       </View>

                                                       </TouchableOpacity>

                                                     }
                                                      {/* services */}

                                                      {
                                                        !this.state.service_three_picker ?
                                                        null :

                                                                      <View style={{width:'100%'}}>
<Text style={{ fontSize: 19, color: 'black',alignItems:'center',marginTop:10,marginBottom:10,fontWeight: 'bold',marginLeft:10 }}>Service three : </Text>
                                                                    <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                                                  <Picker

                                                                                    mode="dropdown"
                                                                                    selectedValue={this.state.selected_service_three}
                                                                                    onValueChange={(value) =>
                                                                                      {

                                                                                        //this.setState({selected_make: value})
                                                                                        //ToastAndroid.show("make id is "+ value, ToastAndroid.LONG)
                                                                                        if(value>0){
																							let price = parseFloat(this.state.global_price)
																let price_two = 0.0
																if(this.state.price_array[3] == null )
																{

																	price_two =price - 0.0
																}

																else{
																	price_two =price - parseFloat(this.state.price_array[3])
																}
                                                                                           this.getSubServicesThree(value);
                                                                                           let a = this.state.service_array
                                                                                           a[3] = value
                                                                                           this.setState({
                                                                                             service_array:a,
                                                                                              selected_service_three:value
                                                                                           })
                                                                                        }


                                                                                      }
                                                                                      }
                                                                                    >
                                                                                    <Item label="Select a service" value="key0" />
                                                                                   {makeServices}
                                                                                 </Picker>
                                                                                 </Card>

                                                                                 {/* mcolot */}
                                                             <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                             <Picker

                                                               mode="dropdown"
                                                               selectedValue={this.state.selected_sub_service_three}
                                                               onValueChange={(value,index) =>
                                                                 {

                                                                   if(value>0){

																	    if(this.state.sub_service_array.indexOf(value) > -1){
										   }
										   else{


                                                                      let item =   this.getItem(this.state.sub_services_three,value)

                                                                     let b = this.state.price_array
                                                                    let price = parseFloat(this.state.global_price)
                                                                    if(isNaN(b[3])){
                                                                      let a = this.state.sub_service_array
                                                                      let price_two =price + parseFloat(item.price)
                                                                       b[3]= item.price
                                                                       a[3] = value
                                                                       this.setState({
                                                                         sub_service_array:a,
                                                                         price_array:b,
                                                                         selected_sub_service_three:value,
                                                                         global_price:parseFloat(price_two),
                                                                       })
                                                                    }

                                                                    else{
                                                                      let a = this.state.sub_service_array
                                                                      let price_two =price - parseFloat(b[2])+ parseFloat(item.price)
                                                                       b[3]= item.price
                                                                       a[3] = value
                                                                       this.setState({
                                                                         sub_service_array:a,
                                                                         price_array:b,
                                                                         selected_sub_service_three:value,
                                                                         global_price:parseFloat(price_two),
                                                                       })
                                                                    }

										   }

                                                                   }

                                                                 }
                                                                 }
                                                               >
                                                               <Item label="SubService" value="key0" />
                                                              {makeSubServicesThree}
                                                            </Picker>
                                                            </Card>

                                                            {
                                                              this.state.service_four_picker ?
                                                                null :
                                                            <Text style={{margin:10,fontWeight: 'bold',color:'black'}}
                                                            onPress={
                                                              ()=>{

																																				  if(this.state.selected_service_three > 0 && this.state.selected_sub_service_three > 0){
                                                                            let a = this.state.sub_service_array
                                                                            let b = this.state.price_array
                                                                            let c=  this.state.service_array

                                                                            let price = parseFloat(this.state.global_price) -  parseFloat(b[3])
                                                                            a = a.splice(2,1)
                                                                            b = b.splice(2,1)
                                                                            c = c.splice(2,1)


                                                                            this.setState({
                                                                              sub_service_array:a,
                                                                              price_array:b,
                                                                              service_array:c,
                                                                              service_four_text:false,
                                                                              service_three_text:true,
                                                                              service_three_picker:false,
                                                                              selected_service_three:'',
                                                                              sub_services_three:[],
                                                                              global_price:parseFloat(price).toFixed(2),

                                                                            })
                                                                          }
                                                                          else if(this.state.selected_service_three > 0){
                                                                            let c=  this.state.service_array
                                                                            c = c.splice(2,1)
                                                                            this.setState({
                                                                             
                                                                              service_array:c,
                                                                              service_four_text:false,
                                                                              service_three_text:true,
                                                                               service_three_picker:false,
                                                                               selected_service_three:'',
                                                                               sub_services_three:[]
                                                                            
                                      
                                                                            })
                                                                          }
																																				  else{
                                                                            this.setState({
                                                                                service_four_text:false,
                                                                                service_three_text:true,
                                                                               service_three_picker:false,
                                                                               selected_service_three:'',
                                                                               sub_services_three:[]
                                                                            })

                                                                                                                                                      }
                                                              }
                                                            }
                                                            >Remove</Text>

                                                          }
                                                        </View>
                                                      }



                                                      {/* services four*/}
                                                                   {
                                                                     !this.state.service_four_text ?
                                                                     null :


                                                                     <TouchableOpacity onPress={() => {
                                                                       this.setState({
                                                                         service_four_text:false,
                                                                         service_four_picker:true,
                                                                         service_five_text:true
                                                                       })
                                                                     }}>
                                                                     <View style={{flexDirection:'row',alignItems:'center'}}>
                                                                     <Text style={{textDecorationLine: 'underline', color: 'black' ,fontSize: 18,marginRight:10}}>Add more services</Text>
                                                                     <Image source={require('./plus-button.png')} style={{height:25, width:25,marginRight: 10}} />
                                                                     </View>

                                                                     </TouchableOpacity>

                                                                   }
                                                                    {/* services */}

                                                                    {
                                                                      !this.state.service_four_picker ?
                                                                      null :

                                                                                    <View style={{width:'100%'}}>
<Text style={{ fontSize: 19, color: 'black',alignItems:'center',marginTop:10,marginBottom:10,fontWeight: 'bold',marginLeft:10 }}>Service four : </Text>
                                                                                  <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                                                                <Picker

                                                                                                  mode="dropdown"
                                                                                                  selectedValue={this.state.selected_service_four}
                                                                                                  onValueChange={(value) =>
                                                                                                    {

                                                                                                      //this.setState({selected_make: value})
                                                                                                      //ToastAndroid.show("make id is "+ value, ToastAndroid.LONG)
                                                                                                      if(value>0){

																										  let price = parseFloat(this.state.global_price)
																let price_two = 0.0
																if(this.state.price_array[4] == null )
																{

																	price_two =price - 0.0
																}

																else{
																	price_two =price - parseFloat(this.state.price_array[4])
																}

                                                                                                         this.getSubServicesFour(value);
                                                                                                         let a = this.state.service_array
                                                                                                         a[4] = value
                                                                                                         this.setState({
                                                                                                           service_array:a,
                                                                                                            selected_service_four:value
                                                                                                         })
                                                                                                      }


                                                                                                    }
                                                                                                    }
                                                                                                  >
                                                                                                  <Item label="Select a service" value="key0" />
                                                                                                 {makeServices}
                                                                                               </Picker>
                                                                                               </Card>

                                                                                               {/* mcolot */}
                                                                           <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
                                                                           <Picker

                                                                             mode="dropdown"
                                                                             selectedValue={this.state.selected_sub_service_four}
                                                                             onValueChange={(value,index) =>
                                                                               {

                                                                                 if(value>0){

																					  if(this.state.sub_service_array.indexOf(value) > -1){
										   }
										   else{
                                                                                  let item =   this.getItem(this.state.sub_services_four,value)
                                                                                  let b = this.state.price_array

                                                                                  let price = parseFloat(this.state.global_price)




                                                                                  if(isNaN(b[4])){

                                                                                    let price_two =price + parseFloat(item.price)



                                                                                    let a = this.state.sub_service_array

                                                                                    b[4]= item.price
                                                                                    a[4] = value
                                                                                    this.setState({
                                                                                      sub_service_array:a,
                                                                                      price_array:b,
                                                                                      selected_sub_service_four:value,
                                                                                      global_price:parseFloat(price_two),
                                                                                    })

                                                                                  }


                                                                                  else{

                                                                                    let price_two =price - parseFloat(b[4]) + parseFloat(item.price)



                                                                                    let a = this.state.sub_service_array

                                                                                    b[4]= item.price
                                                                                    a[4] = value
                                                                                    this.setState({
                                                                                      sub_service_array:a,
                                                                                      price_array:b,
                                                                                      selected_sub_service_four:value,
                                                                                      global_price:parseFloat(price_two),
                                                                                    })



                                                                                  }

										   }

                                                                                 }

                                                                               }
                                                                               }
                                                                             >
                                                                             <Item label="SubService" value="key0" />
                                                                            {makeSubServicesFour}
                                                                          </Picker>
                                                                          </Card>

                                                                          {
                                                                            this.state.service_five_picker ?
                                                                              null :
                                                                          <Text style={{margin:10,fontWeight: 'bold',color:'black'}}
                                                                          onPress={
                                                                            ()=>{
                                                                                if(this.state.selected_service_four > 0 && this.state.selected_sub_service_four > 0){
                                                                                  let a = this.state.sub_service_array
                                                                                  let b = this.state.price_array
                                                                                  let c=  this.state.service_array
                                                                                   let price = parseFloat(this.state.global_price) -  parseFloat(b[4])
                                                                                  a = a.splice(2,1)
                                                                                  b = b.splice(2,1)
                                                                                  c = c.splice(2,1)


                                                                                  this.setState({
                                                                                    sub_service_array:a,
                                                                                    price_array:b,
                                                                                    service_array:c,
                                                                                    service_four_text:true,
                                                                                    service_five_text:false,
                                                                                    service_four_picker:false,
                                                                                     selected_service_four:'',
                                                                                     sub_services_four:[],
                                          global_price:parseFloat(price).toFixed(2),

                                                                                  })
                                                                                }
                                                                                else if(this.state.selected_service_four > 0){
                                                                                  let c=  this.state.service_array
                                                                                  c = c.splice(2,1)
                                                                                  this.setState({
                                                                                   
                                                                                    service_array:c,
                                                                                    service_four_text:true,
                                                                                    service_five_text:false,
                                                                                    service_four_picker:false,
                                                                                    selected_service_four:'',
                                                                                    sub_services_four:[]
                                                                                  
                                            
                                                                                  })
                                                                                }
                                                                                else{
                                                                                  this.setState({
                                                                                    service_four_text:true,
                                                                                    service_five_text:false,
                                                                                    service_four_picker:false,
                                                                                    selected_service_four:'',
                                                                                    sub_services_four:[]
                                                                                  })

                                                                                }
                                                                            }
                                                                          }
                                                                          >Remove</Text>

                                                                        }
                                                                      </View>
                                                                    }



                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                  <RadioGroup
                                  size={24}
                                  thickness={2}
                                  color='#0A0A0A'
                                  selectedIndex={0}
                                  style={{marginRight:"70%",marginBottom:10}}
                                  onSelect = {(index, value) => this.onSelect(index, value)}>

                                            <RadioButton value={'cash'} >
                                              <Text style ={{width:'100%'}}>Card Machine</Text>
                                            </RadioButton>

                                            <RadioButton value={'online'}>
                                              <Text style ={{width:'100%'}}>Paypal</Text>
                                            </RadioButton>

                                  </RadioGroup>


                            </View>

                            <View style={{width:'100%',marginBottom:10}}>
                              <Text>{this.state.promo_code_text}</Text>
                              <View style={{flexDirection:'row',alignItems:'center'}}>

                              <View style={{flex:2}}>

                                      <TextInput
                                        value={this.state.promo}
                                        maxLength={10}
                                        onChangeText={(promo)=>{
                                        this.setState({promo:promo})
                                      }}
                                        placeholder={'Promo Code'}
                                        style={styles.promoinput}

                                        placeholderTextColor={'black'}
                                      />

                              </View>

                              <View style={{flex:1}}>
                              <TouchableOpacity
                                  style={styles.promoButton}
                                  onPress={this.applyPromo.bind(this)}
                                  underlayColor='#fff'>
                              <Text style={styles.promoText}>Apply</Text>
                             </TouchableOpacity>

                              </View>
                              </View>
                        </View>



               <View style={{flexDirection:'row',justifyContent:'flex-start',marginLeft:7,alignSelf:'flex-start'}}>
                        <Text style={{color: 'black' ,fontSize: 18,marginBottom:20}}>Delivery Fee : </Text>
                         {
                              this.state.currency == 'USD'
                              ? <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginRight:0}}> $ </Text>
                              : <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginRight:0}}> R </Text>
                            }
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:10,marginLeft:6}}>{this.state.delivery_fee}</Text>
       </View>


                            <View style={{flexDirection:'row',justifyContent:'flex-start',marginLeft:7,alignSelf:'flex-start'}}>
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginRight:5}}>Estimated Price : </Text>
                            {
                              this.state.currency == 'USD'
                              ? <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginRight:0}}> $ </Text>
                              : <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginRight:0}}> R </Text>
                            }
                            <Text style={{color: 'black' ,fontSize: 18,marginBottom:20,marginLeft:'2%'}}>{this.state.quantity == 0 || this.state.quantity.toString().length == 0 ? "" : parseFloat(this.state.global_price) + parseFloat(this.state.delivery_fee)}</Text>
                            </View>




                    <TouchableOpacity
                              style={styles.locationScreenButton}
                              onPress={this.continue.bind(this)}
                              underlayColor='#fff'>
                              <Text style={styles.locationText}>SUBMIT</Text>
                     </TouchableOpacity>

            </View>
             </ScrollView>
        )
    }
}

let styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
      marginLeft:15,
      marginRight:15
    },
    text: {
        padding: 10,
        fontSize: 14,
    },

    headerText: {
      fontSize: 20,
      margin: 10,
      fontWeight: "bold"
    },
    menuContent: {
      color: "#000",
      fontWeight: "bold",
      padding: 2,
      fontSize: 20
    },
    input: {
      width: "100%",
      height: 45,
      padding: 10,
      fontWeight:'bold',
      color:'black',
      textAlign: 'left',
      borderRadius: 10,
      borderWidth: 2,
      borderColor: 'black',
      marginBottom: 20,

    },
    picker: {
    backgroundColor: '#E5E5E5'
  },
  locationScreenButton:{
    width: "100%",
    marginTop:10,
    paddingTop:10,
    paddingBottom:10,

    backgroundColor:'#FFC300',
    borderRadius:2,
    borderWidth: 1,
    borderColor: '#fff'
  },
  locationText:{
      color:'black',
      textAlign:'center',
      paddingLeft : 10,
      paddingRight : 10,
      fontSize :20,
      fontWeight : 'bold'
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  promoinput: {
    width: "100%",
    height: 45,
    padding: 10,

    color:'black',
    textAlign: 'left',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'black',


  },
  picker: {
  backgroundColor: '#E5E5E5'
},
promoButton:{
  width: "100%",
  marginTop:-1,
  paddingTop:10,
  paddingBottom:10,
  marginLeft:5,

  backgroundColor:'#FFC300',
  borderRadius:10,
  borderWidth: 1,
  borderColor: '#fff'
},
promoText:{
  color:'black',
  textAlign:'center',
  paddingLeft : 10,
  paddingRight : 10,
  fontSize :19,

},
})

// <DatePicker
//   style={{width: "100%",height:45,marginRight:30,marginTop:10,marginBottom:15,borderRadius: 10,borderWidth: 2,borderColor: 'black',}}
//   date={this.state.date}
//   mode="date"
//   placeholder="Date"
//   format="YYYY-MM-DD"
//   confirmBtnText="Confirm"
//   cancelBtnText="Cancel"
//   iconSource = {require('./calendar.png')}
//   customStyles={{
//     dateIcon: {
//       position: 'absolute',
//       right: 0,
//       top: 4,
//     },
//     dateInput: {
//
//       textAlign :'left'
//     }
//     // ... You can check the source to find the other keys.
//   }}
//   onDateChange={(date) => {this.setState({date: date})}}
// />

// <View style={{ flexDirection: 'row',justifyContent: 'flex-end' , marginBottom :55 ,marginTop :15,marginLeft:'30%'}}>
// <Text style={{ alignItems: 'flex-end', color: 'black' ,fontSize: 18}}>{this.state.total_amt}</Text>
// </View>
