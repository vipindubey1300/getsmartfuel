import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet,Platform, Text, TouchableOpacity, Image, ScrollView,Alert, PixelRatio,ToastAndroid, AsyncStorage ,ActivityIndicator} from 'react-native';
import FormData from 'FormData';
import ImagePicker from "react-native-image-picker";
import RNFetchBlob from 'rn-fetch-blob';
import { colors,urls } from './Constants';




export default class UpdatePicture extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ImageSource: null,
       data: null,
       text:''
    };
  }

  static navigationOptions = {
    header: null ,
  };


  selectPhotoTapped() {
     const options = {
       maxWidth: 500,
       maxHeight: 500,
       storageOptions: {
         skipBackup: true
       }
     };

     ImagePicker.showImagePicker(options, (response) => {
       console.log('Response = ', response);

       if (response.didCancel) {
         console.log('User cancelled photo picker');
       }
       else if (response.error) {
         console.log('ImagePicker Error: ', response.error);
       }
       else if (response.customButton) {
         console.log('User tapped custom button: ', response.customButton);
       }
       else {
        //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
         let source = { uri: response.uri};
         this.setState({
           ImageSource: source ,
           data: response.data,

         });
       }
     });
}



upload = () =>{



   AsyncStorage.getItem('uname')
   .then((item) => {
      if (item) {
          var formData = new FormData();
          formData.append('id', item);
          formData.append('image',JSON.stringify(this.state.data));
          let url = urls.base_url + 'api_updateprofilepic'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {
              this.setState({text:JSON.stringify(responseJson)})


              }).catch((error) => {
                console.error(error);
              });

    }
    else {  }
   });

}

//deprecated method
uploadImageToServer = () => {

           //ToastAndroid.show(JSON.stringify(this.state.data),ToastAndroid.LONG) ;
           //if server need base64 then send like this...otherwise only this,statae.data in data
            AsyncStorage.getItem('uname')
            .then((item) => {
               if (item) {

                      RNFetchBlob.fetch('POST', 'http://dev.webmobrilmedia.com/fulex_app/apis/api_updateprofilepic', {


                        'Content-Type': 'multipart/form-data',
                      }, [
                          { name: 'image', filename: 'image', type: 'image/jpeg', data:`data:image/jpeg;base64,${this.state.data}`},
                          { name: 'id', data:item }

                        ]).then((resp) => {



                         this.setState({text:JSON.stringify(resp)})
                         //ToastAndroid.show(JSON.stringify(resp),ToastAndroid.SHORT)



                        }).catch((err) => {
                          // ...
                          ToastAndroid.show("Can't connect to server",ToastAndroid.SHORT)
                        })
             }
             else {  }
            });


   }





  render() {


    if(this.state.ImageSource === null && this.state.image_error){
      return (<Image source={require('./profile.jpg')} style={styles.image} />)
    }
    else if(this.state.ImageSource === null && !this.state.image_error){
      return (<Image source={{uri: this.state.image_url}} style={styles.image} />)
    }
    else{
        return (<Image source={this.state.ImageSource} style={styles.image} />)
    }


    return (

      <View style={styles.container}>

      <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>

             <View style={styles.ImageContainer}>

               {this.state.ImageSource === null && this.state.image_error ? <Image source={require('./profile.jpg')} style={styles.image} /> :
                 <Image style={styles.ImageContainer} source={this.state.ImageSource} />
               }

             </View>

           </TouchableOpacity>





           <TouchableOpacity onPress={this.upload.bind(this)} activeOpacity={0.6} style={styles.button} >
             <Text style={styles.TextStyle}> UPLOAD IMAGE  </Text>
           </TouchableOpacity>



      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding:'10%'
  },

 ImageContainer: {
   borderRadius: 10,
   width: '200%',
   height: '80%',
   borderColor: '#9B9B9B',
   borderWidth: 1 / PixelRatio.get(),
   justifyContent: 'center',
   alignItems: 'center',
   backgroundColor: '#CDDC39',

 },

 button: {

   width: '70%',
   backgroundColor: '#FFC300',
   borderRadius: 7,
   marginTop: 20
 },

 TextStyle: {
   color: '#000000',
   textAlign: 'center',
   padding: 10
 }

});
