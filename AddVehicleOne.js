import React from 'react';
import { StyleSheet, Text, View ,Button,TouchableOpacity, TextInput,ScrollView,Alert,Platform,ToastAndroid,ActivityIndicator,StatusBar,NetInfo,BackHandler} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";

import { withNavigationFocus } from 'react-navigation';
import { colors,urls } from './Constants';




 class AddVehicleOne extends React.Component{

    constructor(){
        super()
        this.state = {
            text: '',
            selected :' ',
            selected_make :'key0',
            selected_model:'key0',
            loading_status:false,
            selected_color:'key0',
            model_error:true,
            modelerror:true,
            makeerror:true,

            make :[],
            model:[],
            color:[],
            vehicle:[],

            vehicle_id:"",
            make_id:'',
            model_id:'',
            color_id:'',
            vehicle_reg_num:'',

            fetch_status:false,

        }

}

    getmakes(vehicleid){
      //ToastAndroid.show(makeId, ToastAndroid.LONG);
      NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected)
      {
        this.props.navigation.navigate("NoNetwork")
        return;
      }
      else{

               var formData = new FormData();
               this.setState({fetch_status:true})

               formData.append('vehicle_id', vehicleid);
               let url = urls.base_url + 'api_get_all_makes'

                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                          //Alert.alert(responseJson);

                          this.setState({fetch_status:false})

                          if(!responseJson.error){
                            var length = responseJson.result.length
                            if(length > 0){
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].Makes.id
                              var make= responseJson.result[i].Makes.year_name.toString()

                                    // this.setState({
                                    //     make: [...this.state.make, year]
                                    // });
                                    const array = [...this.state.make];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], year: make };
                                    this.setState({ make : array,makeerror:false });
                                    //  ToastAndroid.show("Make id!!! without empty...."+ this.state.selected_make, ToastAndroid.LONG);
                              }
                              return true;
                            }

                                            else{
                                              let arr=[];
                                              this.setState({selected_make:0,makeerror:true,make:arr,model:arr})
                                             // Alert.alert("No Record Found!!!");
                                             //ToastAndroid.show("No Record Found!!!", ToastAndroid.LONG);
                                          // ToastAndroid.show("Make id!!!..with empty.."+ this.state.selected_make, ToastAndroid.LONG);
                                          return false;
                                            }

                          }




                          }).catch((error) => {
                            Platform.OS === 'android'
                            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                            : Alert.alert("Error")
                          });
      }

     })

    }

    onSelect(index, value){
        // this.setState({
        // text: `Selected index: ${index} , value: ${value}`
        // })


        this.setState({vehicle_reg_num:''})

        let val = this.getmakes(value)
       //ToastAndroid.show("vehicle id ...."+ value, ToastAndroid.LONG);
        if(!val){
          this.setState({vehicle_id:value,selected_color:'key0',selected_model:'key0',selected_make:'key0',selected_model:'key0',vehicle_reg_num:''})
        }
        else{
          this.setState({vehicle_id:value,selected_color:'key0',selected_model:'key0',selected_make:'key0',vehicle_reg_num:''})
        }
    }

    continue(){
      if(this.state.fetch_status){
        return
      }
      if(this.state.selected_make.length === 0 || this.state.selected_make ==="key0" || parseInt(this.state.selected_make) < -1){
        //this.setState({text: "Please select make year !"})

                  Platform.OS === 'android'
          ? ToastAndroid.show("Select make year !", ToastAndroid.SHORT)
          : Alert.alert("Select make year !")
        return
      }

      if(this.state.selected_model.length === 0 || this.state.selected_model ==="key0" || parseInt(this.state.selected_model) < -1){
      //  this.setState({text: "Please select model !"})
        //  ToastAndroid.show("Select model !", ToastAndroid.SHORT);
          Platform.OS === 'android'
          ? ToastAndroid.show("Select model !", ToastAndroid.SHORT)
          : Alert.alert("Select model !")
        return
      }

      if(this.state.selected_color.length === 0 || this.state.selected_color ==="key0"){
      //  this.setState({text: "Please select color !"})
          //ToastAndroid.show("Select color !", ToastAndroid.SHORT);
          Platform.OS === 'android'
          ? ToastAndroid.show("Select color !", ToastAndroid.SHORT)
          : Alert.alert("Select color !")
        return
      }

      if(this.state.vehicle_reg_num.length === 0){
      //  this.setState({text: "Please enter vehicle registeration number !"})
         // ToastAndroid.show("Enter vehicle registeration number !", ToastAndroid.SHORT);
          Platform.OS === 'android'
          ? ToastAndroid.show("Enter vehicle registeration number !", ToastAndroid.SHORT)
          : Alert.alert("Enter vehicle registeration number !")
        return
      }


      var result  = this.props.navigation.getParam('result')

      result["vehicle_id"] = this.state.vehicle_id
      result["make_id"] = this.state.selected_make
      result["model_id"] = this.state.selected_model
      result["color_id"] = this.state.selected_color
      result["vehicle_reg_num"] = this.state.vehicle_reg_num

      this.props.navigation.navigate('AddVehicleTwo',{result:result});
    }


    handleBackButton = () => {
      if (this.props.isFocused) {
       this.props.navigation.navigate("Location")
        return true;
   }
   };

   componentWillMount() {
       BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }


    h(){
      var result  = this.props.navigation.getParam('result')
      //ToastAndroid.show(result["user_id"] +'....'+ result["service_id"]+'--------'+result["location_name"]+'...'+result["location_long"], ToastAndroid.LONG);
    }

    fetch(){
      this.setState({loading_status:true})

                      let url = urls.base_url + 'api_get_all_vehicles'
                      fetch(url, {
                      method: 'GET',

                    }).then((response) => response.json())
                          .then((responseJson) => {


                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              for(var i = 0 ; i < length ; i++){
                              var name = responseJson.result[i].Vehicle.vechicle_name.toString()
                              var vehicleid= responseJson.result[i].Vehicle.id
                                    if(i === 0){
                                      this.setState({vehicle_id:vehicleid})
                                      this.getmakes(vehicleid)
                                    }
                                    const array = [...this.state.vehicle];
                                    array[i] = { ...array[i], id: vehicleid };
                                    array[i] = { ...array[i], name: name };
                                    this.setState({vehicle: array });
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            Platform.OS === 'android'
                            ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                            : Alert.alert("Error")
                          });


//fetches from server color dataOne
let url_two = urls.base_url + 'api_get_all_colors'
fetch(url_two, {
method: 'GET',

}).then((response) => response.json())
.then((responseJson) => {

  this.setState({loading_status:false})

  if(responseJson.message === "List of Colour"){
    var length = responseJson.result.length.toString();

    for(var i = 0 ; i < length ; i++){

          var id = responseJson.result[i].Color.id.toString()
          var color= responseJson.result[i].Color.color_name.toString()


          const array = [...this.state.color];
          array[i] = { ...array[i], cid: id };
          array[i] = { ...array[i], color_name: color };
          this.setState({ color : array });
    }

  }


else{
  Alert.alert("Cant Connect to Server");
}

}
).catch((error) => {
  Platform.OS === 'android'
  ? ToastAndroid.show("Error", ToastAndroid.SHORT)
  : Alert.alert("Error")
});
    }



    componentDidMount(){

      //fetcha data for make Dropdown
       StatusBar.setBackgroundColor('#32CD32')

                        //   fetch('http://dev.webmobrilmedia.com/fulex_app/apis/api_get_all_makes', {
                        //   method: 'GET',
                        //
                        // }).then((response) => response.json())
                        //       .then((responseJson) => {
                        //         if(responseJson.message === "List of Years"){
                        //           var length = responseJson.result.length.toString();
                        //           //
                        //           //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                        //           let newarray=[];
                        //           for(var i = 0 ; i < length ; i++){
                        //           //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                        //           var year = responseJson.result[i].Makes.year_name.toString()
                        //           var makeid= responseJson.result[i].Makes.id.toString()
                        //
                        //                 //used for normal array
                        //                 // this.setState({
                        //                 //     make: [...this.state.make, year]
                        //                 // });
                        //
                        //
                        //                 const array = [...this.state.make];
                        //                 array[i] = { ...array[i], id: makeid };
                        //                 array[i] = { ...array[i], year: year };
                        //                 this.setState({ make : array });
                        //           }
                        //
                        //         }
                        //
                        //
                        //       else{
                        //         Alert.alert("Cant Connect to Server");
                        //       }
                        //
                        //     }
                        //       ).catch((error) => {
                        //         console.error(error);
                        //       });

              //fetched radio vehicles

              NetInfo.isConnected.fetch().then(isConnected => {
              if(isConnected)
              {
              this.fetch()
              }
              else{
                this.props.navigation.navigate("NoNetwork")
                return;
              }
             })



    }

    getModelfromMake(makeId,vehicleId){
       //ToastAndroid.show(makeId, ToastAndroid.LONG);
       NetInfo.isConnected.fetch().then(isConnected => {
       if(!isConnected)
       {
         this.props.navigation.navigate("NoNetwork")
         return;
       }
       else{
         if(makeId === "key0" || makeId === 0) {
           this.setState({selected_model:0,modelerror:true})
           return
         }
          var formData = new FormData();
          this.setState({fetch_status:true})

          formData.append('make_id', makeId);
          formData.append('vehicle_id', vehicleId);

              let url = urls.base_url + 'api_get_all_models'
                 fetch(url, {
                 method: 'POST',
                 headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data',
                 },
                 body: formData

               }).then((response) => response.json())
                     .then((responseJson) => {
                     //Alert.alert(responseJson);
                     this.setState({fetch_status:false})
                     if(!responseJson.error){
                       var length = responseJson.result.length
                       if(length >  0){
                         for(var i = 0 ; i < length ; i++){
                         var id = responseJson.result[i].Models.id.toString()
                         var model= responseJson.result[i].Models.model_name.toString()

                               // this.setState({
                               //     make: [...this.state.make, year]
                               // });
                               const array = [...this.state.model];
                               array[i] = { ...array[i], id: id };
                               array[i] = { ...array[i], model_name : model };
                               this.setState({ model : array,model_error:true,modelerror:false });
                              // ToastAndroid.show("Modeel id!!! without empty...."+ this.state.selected_model, ToastAndroid.LONG);
                         }
                       }

                       else{
                         let arr=[];
                         let array=[];
                         this.setState({selected_model:0,model_error:false,modelerror:true,model:arr,make:array})

                       }
                     }





                     }).catch((error) => {
                      Platform.OS === 'android'
                                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                        : Alert.alert("Error")
                     });
       }

      })



    }

    render(){


            //used for normal array
          // let makeItems = this.state.make.map((item, index) => {
          //               return <Item label={item} value={index} key={index}/>
          //           })

          let makeRadioVehicle =this.state.vehicle.map((vehicles) => {
                        return (
                          <RadioButton value={vehicles.id} key ={vehicles.id} >
                              <Text style = {{ fontSize: 20 ,fontWeight: 'bold'}}>{vehicles.name}</Text>
                          </RadioButton>
                        )
                    })

          let makeItems =this.state.make.map((makes) => {
                        return <Item label={makes.year} value={makes.id} key={makes.id}/>
                    })
          let makeModels =this.state.model.map((model) => {
                      return <Item label={model.model_name} value={model.id} key={model.id}/>
                      })
          let makeColors =this.state.color.map((color) => {
                                  return <Item label={color.color_name} value={color.cid} key={color.cid}/>
                      })


                          if (this.state.loading_status) {
                            return (
                              <ActivityIndicator
                                animating={true}
                                style={styles.indicator}
                                size="large"
                              />
                            );
                          }




        return(

          <ScrollView>
            <View style={styles.container}>


            <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginTop:20}}>What size vehicle do you drive?</Text>

            <View style={{flexDirection:'row',justifyContent:'flex-start',marginRight:'45%'}}>
                <RadioGroup
                size={24}
                thickness={2}
                color='#0A0A0A'
                selectedIndex={0}
                onSelect = {(index, value) => this.onSelect(index, value)}>

                    {makeRadioVehicle}

                </RadioGroup>
                </View>

              {/* <Text style={{ color: 'red' }}>{this.state.text }</Text> */}

               {/* makeee */}
               <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
               <Picker

                 mode="dropdown"
                 selectedValue={this.state.selected_make}
                 onValueChange={(value) =>
                   {
                     if(this.state.makeerror){
                       this.setState({selected_make: 0})
                      // ToastAndroid.show("erro  ", ToastAndroid.LONG);
                     }
                     else{
                       this.setState({selected_make: value})
                       //ToastAndroid.show("dfgfdgfdgfdg "+value, ToastAndroid.LONG);
                     }
                     //this.setState({selected_make: value})
                    // ToastAndroid.show("make id is "+ value, ToastAndroid.LONG)

                      this.getModelfromMake(value,this.state.vehicle_id);
                   }
                   }
                 >
                 <Item label="Make" value="key0" />
                {makeItems}
              </Picker>
              </Card>


                 {/* model */}
              <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
              <Picker

                mode="dropdown"
                selectedValue={this.state.selected_model}
                onValueChange={(value) =>
                  {
                    if(this.state.modelerror){
                      this.setState({selected_model: 0})
                      
                    }
                    else{
                      this.setState({selected_model: value})
                     
                    }
                    //this.setState({selected_model: value})
                // ToastAndroid.show("Value of sleected model is  "+ value, ToastAndroid.LONG);


                  }
                  }
                >
                <Item label="Models" value="key0" />
               {/*this.state.modelerror ? makeModels :  <Item label="Model" value="key0"/> */}
               {makeModels}
             </Picker>
             </Card>

               {/* mcolot */}
             <Card style={{ width :'95%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}>
             <Picker

               mode="dropdown"
               selectedValue={this.state.selected_color}
               onValueChange={(value) =>
                 {
                   if(value === "key0"){
                     this.setState({error:true})
                   }
                   this.setState({selected_color: value})
                     //ToastAndroid.show("color id is "+ value, ToastAndroid.LONG);
                 }
                 }
               >
               <Item label="Colors" value="key0" />
              {makeColors}
            </Picker>
            </Card>




                                              <TextInput
                                                placeholder={'Vehicle Registeration Number'}
                                                style={styles.input}
                                                onChangeText={(num) => this.setState({vehicle_reg_num: num })}
                                                value={this.state.vehicle_reg_num}
                                              />


                                      <TouchableOpacity
                                                style={styles.locationScreenButton}
                                                onPress={this.continue.bind(this)}
                                                underlayColor='#fff'>
                                                <Text style={styles.locationText}>CONTINUE</Text>
                                       </TouchableOpacity>
            </View>
            </ScrollView>
        )
    }
}

let styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
      marginLeft:15,
      marginRight:15

    },
    text: {
        padding: 10,
        fontSize: 14,
    },

    input: {
      width: "95%",
      height: 46,
      padding: 10,
      textAlign: 'center',
      borderRadius: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 30,
      marginTop : 10,
    },
    locationScreenButton:{
      width :'95%',
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#FFC300',
      borderRadius:1,
      borderWidth: 1,
      borderColor: '#fff',
      marginBottom:10
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    }
})


export default withNavigationFocus(AddVehicleOne);

//
// <Dropdown
//       label='Make'
//       data={dataOne}/>
//
// <Dropdown
//     label='Model'
//     data={dataTwo}/>
//   <Dropdown
//       label='Colour'
//       data={dataTwo}/>
