import React, { Component } from 'react';
import {Button, TextInput, View, StyleSheet, Text,Platform, TouchableOpacity,FlatList,Image, ScrollView,Alert,ToastAndroid, AsyncStorage ,ActivityIndicator,NetInfo} from 'react-native';
//import { CheckBox } from 'react-native-elements';

import { Card, ListItem, Icon } from 'react-native-elements';
import { colors,urls } from './Constants';



export default class GetVehicles extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      vehicle_error:false,
      loading_status:false,
      data: [],
      result:'',



    };
  }



fetch(){
  this.setState({loading_status:true})
  AsyncStorage.getItem('uname')
  .then((item) => {
   if (item) {
    // ToastAndroid.show("Fetching vehicles data !", ToastAndroid.LONG);
    let url = urls.base_url + 'api_get_vehicle_by_user_id?user_id=' +item 
     fetch(url, {
     method: 'GET',
     headers: {
       'Accept': 'application/json',
       'Content-Type':  'multipart/form-data',
     },


   }).then((response) => response.json())
         .then((responseJson) => {
           this.setState({result:responseJson})

         if(!responseJson.error){
              this.setState({loading_status:false})
        //  ToastAndroid.show(JSON.stringify(this.state.result), ToastAndroid.LONG)

               //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
               //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
               var length = this.state.result.result.length
               if(length > 0){

                //ToastAndroid.show(JSON.stringify(this.state.data), ToastAndroid.LONG)

                var temp_arr=[]
                for(var i = 0 ; i < length ; i++){
                  var id = this.state.result.result[i].MyVehicle.id


                  var vehicle_id = this.state.result.result[i].Vehicle.id
                  var vehicle_name = this.state.result.result[i].Vehicle.vechicle_name


                  var fuel_id = this.state.result.result[i].Fuel.id
                  var fuel_name = this.state.result.result[i].Fuel.fuel_name


                  var sub_fuel_id = this.state.result.result[i].Subfuel.id
                  var sub_fuel_name = this.state.result.result[i].Subfuel.sub_fuel_type
                  var sub_fuel_price = this.state.result.result[i].Subfuel.price
                    var sub_fuel_price_africa = this.state.result.result[i].Subfuel.price_africa


                  var reg_num = this.state.result.result[i].MyVehicle.registration_num

                  var make_id = this.state.result.result[i].Make.id
                  var make_name =this.state.result.result[i].Make.year_name

                  var model_id = this.state.result.result[i].Model.id
                  var model_name = this.state.result.result[i].Model.model_name

                  var color_id = this.state.result.result[i].Color.id
                  var color_name = this.state.result.result[i].Color.color_name



                  let array = [...temp_arr];
                 // let array = [...arr];
                  array[i] = { ...array[i], key:id };
                  array[i] = { ...array[i], vid: vehicle_id };
                  array[i] = { ...array[i], vname: vehicle_name };
                  array[i] = { ...array[i], fid: fuel_id };
                  array[i] = { ...array[i], fname: fuel_name };
                  array[i] = { ...array[i], subfuelid: sub_fuel_id };
                  array[i] = { ...array[i], subfuelname: sub_fuel_name };
                  array[i] = { ...array[i], subfuelprice: sub_fuel_price };
                  array[i] = { ...array[i], reg: reg_num };
                  array[i] = { ...array[i], makeid: make_id };
                  array[i] = { ...array[i], makename: make_name };
                  array[i] = { ...array[i], modelid: model_id };
                  array[i] = { ...array[i], price_africa: sub_fuel_price_africa };
                  array[i] = { ...array[i], modelname: model_name };
                  array[i] = { ...array[i], colorid: color_id };
                  array[i] = { ...array[i], colorname: color_name };

                  temp_arr = array
                  this.setState({data: temp_arr})

                }
              //  this.setState({data: temp_arr})
                //ToastAndroid.show(JSON.stringify(this.state.data), ToastAndroid.LONG)
              //  ToastAndroid.show("responseJson.message", ToastAndroid.LONG);




               }
               else{
                //ToastAndroid.show("No vehicles find !!",ToastAndroid.SHORT)
                var result  = this.props.navigation.getParam('result')
                 this.props.navigation.navigate('AddVehicleOne',{result : result});
                this.setState({
                  vehicle_error : true,
                  loading_status:false
                })
               }


       }else{
                 //errror in insering data
                 Platform.OS === 'android'
                 ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                 : Alert.alert(JSON.stringify(responseJson.message))
                 this.setState({ loading_status:false});

         }


         }).catch((error) => {
          Platform.OS === 'android'
          ? ToastAndroid.show("Error", ToastAndroid.SHORT)
          : Alert.alert("Error")
         });
 }
 else {  }
});


}






  componentWillMount(){

    NetInfo.isConnected.fetch().then(isConnected => {
    if(isConnected)
    {
    this.fetch()
    }
    else{
      this.props.navigation.navigate("NoNetwork")
      return;
    }
   })

  }





  next(v_id){
    if(this.state.data.length > 0){
			//var jobs = this.state.jobs
			for(var i = 0 ; i < this.state.data.length ; i++){
				if(this.state.data[i].key == v_id){
          var result  = this.props.navigation.getParam('result')
					result["vehicle_reg_num"] = this.state.data[i].reg
          result["vehicle_id"] = this.state.data[i].vid
          result["make_id"] = this.state.data[i].makeid
          result["model_id"] = this.state.data[i].modelid
          result["color_id"] = this.state.data[i].colorid
          result["fuel_id"] = this.state.data[i].fid
          //result["sub_fuel_id"] = this.state.data[i].subfuelid
          result["fuel_subtype_id"] = this.state.data[i].subfuelid
          result["dummy_price"] = this.state.data[i].subfuelprice
          result["price_africa"] = this.state.data[i].price_africa
          result["dummy_name"] = this.state.data[i].subfuelname

					// ToastAndroid.show("Res///..."+);
				//	this.props.navigation.navigate('BookService',{result : result});
        this.props.navigation.navigate('SelectCurrency',{result : result});
				}
			}
		}
  }

  nextScreen(){
    var result  = this.props.navigation.getParam('result')
    this.props.navigation.navigate('AddVehicleOne',{result : result});
  }



  render() {



        if (this.state.loading_status) {
          return (
            <ActivityIndicator
              animating={true}
              style={styles.indicator}
              size="large"
            />
          );
        }


    return (

      <View style={styles.container}>



      <Text style ={{color:'black',fontSize :20,fontWeight :'bold',alignItems:'center',marginTop:13}}>Saved Vehicle</Text>

      <ScrollView style={{paddingBottom:15}} showsVerticalScrollIndicator={false}>
			 <View style={{padding:1,width:'100%'}}>

     {
       !this.state.vehicle_error
       ?
         <FlatList
							  style={{marginBottom:2,width:'100%'}}
							  data={this.state.data}
							  showsVerticalScrollIndicator={false}
                keyExtractor={item => item.vname}
							  renderItem={
                  ({item}) =>

							   <TouchableOpacity  onPress={() => this.next(item.key)}>
                 <View>
									  <Card containerStyle={{padding: 10,borderRadius:10}}>

                    <Text style ={{margin :2,color:'black'}}>Vehicle name : {item.vname}</Text>
										<Text style ={{margin :2}}>Vehicle Registeration Number : {item.reg}</Text>
                    <Text style ={{margin :2}}>Fuel name : {item.fname}</Text>
                    <Text style ={{margin :2}}>SubFuel name : {item.subfuelname}</Text>
                    <Text style ={{margin :2}}>Make name : {item.makename}</Text>
                    <Text style ={{margin :2}}>Model name : {item.modelname}</Text>
                    <Text style ={{margin :2}}>Color name : {item.colorname}</Text>
									  </Card>
                  </View>
							  </TouchableOpacity>
							  }

							/>


      :
        <Text>No Vehicles in your list</Text>
     }








       <TouchableOpacity
                 style={styles.locationScreenButton}
                 onPress={this.nextScreen.bind(this)}
                 underlayColor='#fff'>
                 <Text style={styles.locationText}>Add New Vehicle</Text>
        </TouchableOpacity>

        </View>
        </ScrollView>



      </View>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginLeft:15,
    marginRight:15,

    alignItems:'center',

  },
  image: {
      width: 120,
      height: 120,
      borderRadius: 120 / 2,
      overflow: "hidden",
      borderWidth: 3,
      borderColor: "#008000",
      marginBottom:15,
      alignItems:'center',
    },
    locationScreenButton:{
      width: "80%",
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      alignItems:'center',
      backgroundColor:'#FFC300',
      borderRadius:10,
      alignSelf:'center',
      borderWidth: 1,
      borderColor: '#fff'
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    text:{
      color:'black',
      fontSize :18,
      fontWeight : 'bold',
      marginBottom:20,
      width:'100%'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    },
    input: {
      width: "100%",
      height: 44,
      textAlign: 'center',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
    }
});
