import React from 'react';
import { StyleSheet, Text, View ,Button, Platform,TouchableOpacity,ToastAndroid,ActivityIndicator,StatusBar,NetInfo,WebView} from 'react-native';
import { colors,urls } from './Constants';


export default class HowWorks extends React.Component{

    constructor(){
        super()
        this.state = {
            text: '',
            message:'',
            loading_status:false
        }

    }

fetch(){

        this.setState({loading_status:true})

        let url = urls.base_url + 'api_howitworks'
                            fetch(url, {
                            method: 'GET',

                          }).then((response) => response.json())
                                .then((responseJson) => {
                                  this.setState({loading_status:false})
                                  if(!responseJson.error){

                                    let msg = responseJson.message
                                    let terms = responseJson.result[0].Page.description
                                    this.setState({text:terms,message:msg})

                                  }

                                else{
                                  //Alert.alert("Cant Connect to Server");
                                }

                              }
                                ).catch((error) => {
                                  Platform.OS === 'android'
                                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                        : Alert.alert("Error")
                                });


}
    componentDidMount(){
      StatusBar.setBackgroundColor('#32CD32')

            NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected)
            {
            // this.fetch()
            }
            else{
              this.props.navigation.navigate("NoNetwork")
              return;
            }
           })
    }

    render(){

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }


        return(
          <View style = {styles.container}>
         <WebView
              source = {{ uri:
                urls.base_url + 'api_howitworks' }}
            />
          </View>
        )
    }
}

let styles = StyleSheet.create({
  container: {
    height: '100%',
 },
    text: {

        fontSize: 14,
        width:'100%',

    },
    message:{
      fontSize: 20,
      fontWeight:'bold',
      width:'100%',
      color:'black',
      marginBottom:30
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    }
})


// {
//  this.state.isHidden ?  <View>
//  <Dropdown
//       label="Select Fuel Type"
//       data={this.state.data}
//       onChangeText={(name , price )=>{
//         //this.setState({subfuelid:value})
//         ToastAndroid.show("subfuelid" + name + "price "+ price  , ToastAndroid.LONG);
//
//       }}
//     />
//     </View> : null
// }
