import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text, Platform,FlatList, TouchableOpacity , TouchableHighlight,ToastAndroid,AsyncStorage,ActivityIndicator,NetInfo} from 'react-native';
import { colors,urls } from './Constants';

export default class OrderHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orders: [],
      loading_status:false,

    };
  }
  //this is used to remove toolbar in given screen
  static navigationOptions = {
    header: null ,
  };



fetch(){
  this.setState({loading_status:true})
  AsyncStorage.getItem('uname')
  .then((item) => {
     if (item) {
       id = item


       let url = urls.base_url + 'api_get_all_bookings_by_userid?user_id=' + item
       fetch(url, {
       method: 'GET',

     }).then((response) => response.json())
           .then((responseJson) => {
             this.setState({loading_status:false})
           if(!responseJson.error){
             var length = responseJson.result.length
             if(length === 0){
              Platform.OS === 'android'
              ? ToastAndroid.show("No bookings find !!", ToastAndroid.SHORT)
              : Alert.alert("No bookings find !!")
             }
             else{
               for(var i = 0 ; i < length ; i++){
                 const orders = [...this.state.orders];
                 const bookingData = responseJson.result;
                 bookingData.map((booking, index) => {
                      orders[index] = {}
                      orders[index]["bookingid"] = booking.order_id
                      orders[index]["date"] = booking.booking_date
                     // orders[index]["price"] = booking.total_amount
                     // orders[index]["price"] = booking.discount_amt
                     orders[index]["price"] = booking.total_amount
                     orders[index]["delivery_fee"] = booking.delivery_fee

                   });
                 this.setState({orders});

               }
             }

           }
           else{
             this.setState({loading_status:false})
             Alert.alert("Cant Connect to server");
           }


           }).catch((error) => {
            this.setState({loading_status:false});
           });

   }
   else {  }
  });
}
  componentDidMount(){

    NetInfo.isConnected.fetch().then(isConnected => {
    if(isConnected)
    {
    this.fetch()
    }
    else{
      this.props.navigation.navigate("NoNetwork")
      return;
    }
   })


  }


seeMore(booking_id){


  var length = this.state.orders.length
  var obj={}

  obj["bid"] = booking_id

          var length = this.state.orders.length
          
           for(var i = 0 ; i < length ; i++){
                 if(this.state.orders[i].bookingid === booking_id){

                   obj["price"] = this.state.orders[i].price

                 }

           }


  this.props.navigation.navigate("HistoryDetails",{result : obj})
}



track(booking_id){

  var length = this.state.orders.length
  var obj={}
  for(var i = 0 ; i < length ; i++){
    if(this.state.orders[i].bid === booking_id){
      obj["quantity"] = this.state.orders[i].quantity
      obj["price"] = this.state.orders[i].price
      obj["time"] = this.state.orders[i].time.toString()
      obj["orderstatus"] = this.state.orders[i].order_status
      obj["delivery_fee"] = this.state.orders[i].delivery_fee

    }

  }


  this.props.navigation.navigate("TrackOrder",{result : obj})


}

  render() {



            if (this.state.loading_status) {
              return (
                <ActivityIndicator
                  animating={true}
                  style={styles.indicator}
                  size="large"
                />
              );
            }



    return (
		<View style={styles.container}>

    <FlatList
      data={this.state.orders}
      showsVerticalScrollIndicator={false}
      renderItem={({item}) =>


      <View style={{ marginBottom:10,borderWidth: 1,borderColor: 'black', borderRadius:2,marginLeft:5,marginRight:5,marginTop:10}}>

      <View style={{backgroundColor:'green'}}>
          <Text style={{fontFamily: 'Verdana',fontSize: 20,fontWeight: 'bold', color: "white",alignItems:'center',marginLeft:'3%'}}>Date          : {item.date}</Text>
          </View>


          <View style={{ flexDirection : 'row',justifyContent: 'space-between'}}>
          <Text style={{fontFamily: 'Verdana',fontSize: 20,fontWeight: 'bold',marginLeft:'3%'}}>Booking Id   : {item.bookingid}</Text>
          <View/>
          </View>

            <View style={{ flexDirection : 'row',justifyContent: 'space-between'}}>
            <Text style={{fontFamily: 'Verdana',fontSize: 20,fontWeight: 'bold',marginLeft:'3%'}}>Price         : {item.price}</Text>
            <TouchableHighlight onPress={() => {
              this.seeMore(item.bookingid)}}>
            <Text style={{color : 'green',fontFamily: 'Verdana',fontSize: 15,fontWeight: 'bold',marginLeft:25,textDecorationLine: 'underline'}}>See More..</Text>
            </TouchableHighlight>
            </View>

      </View>

      }
      keyExtractor={item => item.bid}
    />


      </View>
	  )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
      backgroundColor: "white"
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  noorders: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'
  }

});
