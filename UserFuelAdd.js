import React from 'react';
import { StyleSheet, Text, View ,Button,AsyncStorage,Platform, Alert,TouchableOpacity,ToastAndroid,ActivityIndicator,StatusBar,NetInfo} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { colors,urls } from './Constants';
import FormData from 'FormData';
import {  Container,  Header,  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";


export default class UserFuelAdd extends React.Component{

    constructor(){
        super()
        this.state = {
            text: '',
            fuel:[],
            error:false,
            dropdowndata:[],
            isHidden:false,
            data:[],
            fuel_id:'',
            dropdownlabel:'',
            subfuelid:'key0',
            subfuelname:'',
            subfuelprice:0,
            subarray:[],
            loading_status:false
        }
        this.onSelect = this.onSelect.bind(this)
    }

    onSelect(index, value){
      this.setState({fuel_id:value})
          this.getFuelSubtype(value)
          //ToastAndroid.show("ID is " + value, ToastAndroid.LONG);

        // this.setState({
        // text: `Selected index: ${index} , value: ${value}`
        // })
    }

    getFuelSubtype(fuelid){
      NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected)
      {
        this.props.navigation.navigate("NoNetwork")
        return;
      }
      else{
        var formData = new FormData();
        // if(!this.state.loading_status){
        //   this.setState({loading_status:true,fuel_id:fuelid})
        // }

        formData.append('fuel_id', fuelid);
        let url = urls.base_url + 'api_get_all_subfueltype'
                                  fetch(url, {
                                  method: 'POST',
                                  headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'multipart/form-data',
                                  },
                                  body: formData

                                }).then((response) => response.json())
                                      .then((responseJson) => {
                                        // if(this.state.loading_status){
                                        //   this.setState({loading_status:false,fuel_id:fuelid})
                                        // }
                                        if(!responseJson.error){
                                          var length = responseJson.result.length
                                          if(length == 0){
                                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                                              this.setState({isHidden:false})
                                          }
                                          else{
                                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                                            for(var i = 0 ; i < length ; i++){
                                            //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                                            var name = responseJson.result[i].Subfuel.sub_fuel_type
                                            var subfuelid= responseJson.result[i].Subfuel.id
                                            var price = responseJson.result[i].Subfuel.price

                                                  const array = [...this.state.data];

                                                  array[i] = { ...array[i], name: name };
                                                  array[i] = { ...array[i], id : subfuelid };
                                                  array[i] = { ...array[i], price: price };
                                                  this.setState({data: array , isHidden:true,subarray:array});

                                            }
                                          }



                                        }


                                      else{
                                        Alert.alert("Cant Connect to Server");
                                      }

                                    }
                                      ).catch((error) => {
                                        Platform.OS === 'android'
                                        ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                        : Alert.alert("Error")
                                      });

      }

     })



    }

    continue(){

      if(this.state.subfuelid.length === 0 || this.state.subfuelid ==="key0"){
        
        //ToastAndroid.show("Please select fuel type !", ToastAndroid.SHORT);

        Platform.OS === 'android' 
        ? ToastAndroid.show("Please select fuel type !", ToastAndroid.SHORT)
        : Alert.alert("Please select fuel type !")

        
        return
      }


//starrt
AsyncStorage.getItem('uname')
.then((item) => {
 if (item) {

  // ToastAndroid.show("Please wait !", ToastAndroid.SHORT);

   this.setState({loading_status:true})

   var result  = this.props.navigation.getParam('result')


 var formData = new FormData();
 formData.append('user_id',item);
 formData.append('vehicle_id',result["vehicle_id"]);
 formData.append('make_id', result["make_id"]);
 formData.append('model_id',result["model_id"]);
 formData.append('color_id',result["color_id"]);
 formData.append('reg_num', result["reg_num"]);
 formData.append('fuel_id',this.state.fuel_id);
 formData.append('sub_fuel_id', this.state.subfuelid);
 formData.append('status',1);




 let url = urls.base_url + 'api_add_vehicles'
fetch(url, {
method: 'POST',
headers: {
  'Accept': 'application/json',
  'Content-Type':  'multipart/form-data',
},
body: formData

}).then((response) => response.json())
    .then((responseJson) => {
      this.setState({loading_status:false})
      Platform.OS === 'android' 
                               ? ToastAndroid.show(JSON.stringify(responseJson.message), ToastAndroid.SHORT)
                               : Alert.alert(JSON.stringify(responseJson.message))
      this.props.navigation.navigate("HomeScreen");


    }).catch((error) => {
      console.error(error);
    });
}
else { 
//ToastAndroid.show("User ID not found", ToastAndroid.LONG);

Platform.OS === 'android' 
? ToastAndroid.show("User ID not found", ToastAndroid.LONG)
: Alert.alert("User ID not found")

}
});


   //end

    }




    abc(){
      var result  = this.props.navigation.getParam('result')
      //ToastAndroid.show(result["user_id"] +'....'+ result["service_id"]+'--------'+result["location_name"]+'...'+result["make_id"]+'...'+result["vehicle_reg_num"], ToastAndroid.LONG);
    }

    fetch(){

            this.setState({loading_status:true})
            let url = urls.base_url + 'api_get_all_fueltype'
                                fetch(url, {
                                method: 'GET',

                              }).then((response) => response.json())
                                    .then((responseJson) => {
                                      this.setState({loading_status:false})
                                      if(!responseJson.error){
                                        var length = responseJson.result.length.toString();
                                        //
                                        //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                                        let newarray=[];
                                        for(var i = 0 ; i < length ; i++){
                                        //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                                        var name = responseJson.result[i].Fuel.fuel_name.toString()
                                        var fuelid= responseJson.result[i].Fuel.id
                                              if(i === 0){
                                                this.setState({fuel_id:fuelid})
                                                this.getFuelSubtype(fuelid)
                                              }
                                              const array = [...this.state.fuel];
                                              array[i] = { ...array[i], id: fuelid };
                                              array[i] = { ...array[i], name: name };
                                              this.setState({fuel: array });
                                              //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                        }

                                      }


                                    else{
                                      Alert.alert("Cant Connect to Server");
                                    }

                                  }
                                    ).catch((error) => {
                                      Platform.OS === 'android'
                                      ? ToastAndroid.show("Error", ToastAndroid.SHORT)
                                      : Alert.alert("Error")
                                    });


    }

     pickerSelect(value) {
        var arr = this.state.subarray
          for(var i = 0 ; i < arr.length ; i++){
                                            //  newarray.push(responseJson.result[i].Makes.year_name.toString());
                                            var id = arr[i].id
              if (id == value) {
                 // this.setState({data: array , isHidden:true,subarray:array});
                   this.setState({subfuelid:arr[i].id,subfuelprice:arr[i].price , subfuelname:arr[i].name})
              }
                                                 

                                            }
    }



    componentDidMount(){
      StatusBar.setBackgroundColor('#32CD32')
      NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
      {
      this.fetch()
      }
      else{
        this.props.navigation.navigate("NoNetwork")
        return;
      }
     })

    }

    render(){

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }


                      let makeRadio =this.state.fuel.map((fuels) => {
                                    return (
                                      <RadioButton value={fuels.id} key ={fuels.id} >
                                          <Text style = {{ fontSize: 20 ,fontWeight: 'bold'}}>{fuels.name}</Text>
                                      </RadioButton>
                                    )
                                })

                      let makeSubfuel =this.state.data.map((subfuel) => {
                                          return <Picker.Item label={subfuel.name} value={subfuel.id} key={subfuel.price}/>
                            })


        return(
            <View style={{ flexGrow: 1 ,marginLeft:15,marginRight :15}}>

            <Text style = {{ color: 'green', fontSize: 25 ,fontWeight: 'bold' ,marginBottom:20,marginTop:20,marginLeft:10}}>Which fuel you required ?</Text>

                <RadioGroup
                      size={24}
                      thickness={2}
                      color='#0A0A0A'
                      selectedIndex={0}
                      onSelect = {(index, value) => this.onSelect(index, value)}>


                                      {makeRadio}



                </RadioGroup>


              <Form>
              <Picker style={{ width :'90%' ,borderRadius: 10,borderWidth: 3,borderColor: 'black'}}

                mode="dropdown"
                selectedValue={this.state.subfuelid}
                onValueChange={(itemValue, itemIndex) =>
                  {
                   this.setState({subfuelid: itemValue})
                //  Alert.alert(itemValue)
                      this.pickerSelect(itemValue)





                  }
                  }
                >
                <Picker.Item label="Select Fuel Type" value="key0" />
                {makeSubfuel}

              </Picker>
              </Form>


                  {/* <Text style={{ color: 'red' }}>{this.state.text }</Text> */}


                <TouchableOpacity
                          style={styles.locationScreenButton}
                          onPress={this.continue.bind(this)}
                          underlayColor='#fff'>
                          <Text style={styles.locationText}>Add vehicle</Text>
                 </TouchableOpacity>


            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        marginTop: 20,
        padding: 20
    },
    text: {
        padding: 10,
        fontSize: 14,
    },

    input: {
      width: "100%",
      height: 40,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
    locationScreenButton:{
      width: "100%",
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#FFC300',
      borderRadius:2,
      borderWidth: 1,
      borderColor: '#fff'
    },
    locationText:{
        color:'black',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize :20,
        fontWeight : 'bold'
    },
    indicator: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      height: 80
    }
})


// {
//  this.state.isHidden ?  <View>
//  <Dropdown
//       label="Select Fuel Type"
//       data={this.state.data}
//       onChangeText={(name , price )=>{
//         //this.setState({subfuelid:value})
//         ToastAndroid.show("subfuelid" + name + "price "+ price  , ToastAndroid.LONG);
//
//       }}
//     />
//     </View> : null
// }
